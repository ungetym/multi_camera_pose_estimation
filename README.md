# Multi_Camera_Pose_Estimation

This tool can be used to calculate the poses of RGB(D) devices after successful intrinsic calibration via the Guided Camera Calibration Tool available [here](https://gitlab.com/ungetym/guided_camera_calibration) or the Multi-Camera Calibration Tool available [here](https://gitlab.com/ungetym/Multi_Camera_Calibration).

## Dependencies:

- Qt 6.4 (untested with older versions, but should work with version >5.10)
- OpenCV 4.2.0 (should also work with version 4.0 and above)
- Ceres 2.2.0 (untested with older versions)

## Build instructions:

There are two versions of this tool. The version for RGB-D devices such as the Azure Kinect is located in the folder [src/RGBD/cpp](https://gitlab.com/ungetym/multi_camera_pose_estimation/-/tree/main/src/RGBD/Cpp) and the other version for simple RGB devices is located in [src/RGB/cpp](https://gitlab.com/ungetym/multi_camera_pose_estimation/-/tree/main/src/RGB/Cpp).

A CMakeLists.txt is provided in the respective source folder. Use the usual 'mkdir build', 'cd build', 'cmake ..', 'make' pipeline or open the CMakeLists.txt file via an IDE which supports cmake (e.g. QtCreator).

## Usage:

After starting the tool it should look similar to this (the version for RGB cameras contains fewer input fields).

![GUI Overview](https://gitlab.com/ungetym/multi_camera_pose_estimation/-/raw/main/doc/tool.jpg "GUI Overview")

First select the directory your RGB (and depth images) are stored in. In my case the content of that folder looks as follows

![Image Folder](https://gitlab.com/ungetym/multi_camera_pose_estimation/-/raw/main/doc/filelist.jpg "Image Folder")

As you can see, the image are named using the camera name, an image ID and a suffix for the type (e.g. 'rgb' or 'depth'). Accordingly, the file name schemes have to be set. The rules for this are:

- The $CAM and the $ID tag must be present in each scheme exactly one time
- \- and _ are allowed separators
- Every part that is not the camera name, image ID or a known separator must be a string that is constant for all images of one type.

After setting the schemes, the .json or .MCC_Cams file resulting from the intrinsic or per-device calibration with the above mentioned tools has to be set. Since this file also contains the camera names, which can differ from the ones used for the image naming, you also have to set camera naming schemes. Here only the tag $CAM and constant strings are allowed.

IMPORTANT: The camera name specified by $CAM must be the same as in the file names. My .MCC_Cams calibration file e.g. looks like this

![Calibration File](https://gitlab.com/ungetym/multi_camera_pose_estimation/-/raw/main/doc/calibration_file.jpg "Calibration File")

Note, that the camera name 'kinect0' from the image files is also present in this calibration file. If this is not the case, please consider renaming either the images or the cameras in the calibration file.
Finally, the pose calculation can be started by clicking 'Start Pose Calculation' and after the process is finished the results can be stored as a .json or .MCC_Cams file with the same camera intrinsics that were loaded before.

Optional: By activating the checkbox 'Advanced Options' you can also fine tune the parameters for the different steps and inspect the detections.
