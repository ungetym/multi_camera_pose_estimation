%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%   This example shows the estimation of poses in a multi camera setup    %
%    based on a single moving target light as simulated by the branch     %
%                   "gt_for_extrinsics_calibration" of                    %
%          https://gitlab.com/ungetym/camera_dome_simulation/             %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;

%% Setup

% Enable to show light target detections
debug = false;

% Enable to load previously detected ball positions if available
load_detections = true;

% Path to image folder also containing the ground truth camera data
data_folder = "/home/anonym/Renderings/";

% Create list of rgb images
num_cams = 5;
rgb_image_paths = dir(fullfile(data_folder, '*.jpg'));
num_imgs_per_cam = size(rgb_image_paths, 1) / num_cams;

% Max size of noise in px added to the detections
detection_noise = 0;

% Intrinsics noise level (0 - none, 10 - maximum) is used to perturb K
intrinsics_noise = 0;

% K matrix - TODO: Read from the camera config file
K = [1423.25 0 960; 0 1423.25 600; 0 0 1];

%% Detect correspondences

% Find corresponding pixels for the different cameras
detections = {};

detections_filename = data_folder+"detections.mat";
if load_detections
    try
        load(detections_filename);
    catch 
        %fprintf("File %s not available. File will be created after " + ...
        %    "detection procedure.\n", detections_filename);
    end
end

if size(detections,2) < 1
    detections = cell(num_imgs_per_cam,1);
    for i = 1:num_imgs_per_cam
        detections_per_target = [];
        for j = 1:num_cams
            img_idx = (j-1)*num_imgs_per_cam + i;
            img = imread(strcat(data_folder, rgb_image_paths(img_idx).name));
            [x,y,r] = detectBallHSV(img);
            if r > 0
                detections_per_target = [detections_per_target; j, [x,y,r]];
            end
            % Debug output: draw detection onto image or show image without
            % detections if unable to detect
            if debug
                if r > 0
                    figure(1);
                    debug_img = insertMarker(img, [x,y], ...
                        "circle","Size", round(r));
                    debug_img = insertMarker(debug_img, [x,y], ...
                        "circle","Size", round(r)+1);
                    imshow(debug_img);
                else
                    figure(2);
                    imshow(img);
                end
                waitforkey();
            end
        end
        detections{i,1} = detections_per_target;
    end
    % Store detections, so they can be loaded next time
    save(detections_filename, 'detections', '-mat');
end
clearvars detections_filename detections_per_target i img img_idx j ...
    load_detections r x y;

%% Add noise to detections
if detection_noise > 0
    noise = @(x) detection_noise * x.^2;
    for i = 1:size(detections,2)
        for j = 1:size(detections{1,i},1)
            detections{1,i}(j,2:3) = detections{1,i}(j,2:3) + noise(rand(2,1))';
        end
    end
end

%% Add noise to K matrix

if intrinsics_noise > 0
    noise_vec = 1 + (intrinsics_noise*2*(rand(4,1) - 0.5))/100;
    K(1,1) = noise_vec(1) * K(1,1);
    K(2,2) = noise_vec(2) * K(2,2);
    K(1,3) = noise_vec(3) * K(1,3);
    K(2,3) = noise_vec(4) * K(2,3);
end

%% Calculate pairwise relative poses
R_rel = cell(num_cams-1, 1);
t_rel = cell(num_cams-1, 1);
correspondences = cell(num_cams-1, 1);
for cam_idx = 2:num_cams

    % Find set of correspondences between device cam_idx to device 
    % cam_idx-1

    pair_correspondences = [];
    for detection_idx = 1:num_imgs_per_cam
        if size(detections{1,detection_idx}, 2) > 0
            detected_in_cam = detections{1,detection_idx}(:,1);
            if ismember(cam_idx-1, detected_in_cam) && ismember(cam_idx, detected_in_cam)
                cam_succ_detect_idx = find(detections{1,detection_idx}(:,1)==cam_idx-1);
                cam_detect_idx = find(detections{1,detection_idx}(:,1)==cam_idx);
    
                pair_correspondences = [pair_correspondences; ...
                    detections{1,detection_idx}(cam_succ_detect_idx,2:3), ...
                    detections{1,detection_idx}(cam_detect_idx,2:3), ...
                    detection_idx];
            end
        end
    end

    num_correspondences = size(pair_correspondences, 1);
    if num_correspondences < 8
        fprintf("ERROR: Not sufficient correspondences between device %s and %s", ...
            cam_idx-1, cam_idx);
    end

    % Create default camera parameters
    params = cameraParameters("K", K, "ImageSize", [1920, 1080]);

    % Estimate essential matrix
    [E, inliersidx] = estimateEssentialMatrix(pair_correspondences(:,1:2), ...
        pair_correspondences(:,3:4), params, ...
        "MaxNumTrials", 100000, "Confidence", 99.999);

    % Calculate relative pose
    rel_pose = estrelpose(E, params.Intrinsics,...
        pair_correspondences(:,1:2), pair_correspondences(:,3:4));

    % Transformation is given TO reference camera
    R_rel{cam_idx-1, 1} = rel_pose.R';
    t_rel{cam_idx-1, 1} = rel_pose.R'*rel_pose.Translation';
    correspondences{cam_idx-1, 1} = pair_correspondences;
end

clearvars cam_detect_idx cam_idx cam_succ_detect_idx ...
    detected_in_cam detection_idx E num_correspondences ... 
    pair_correspondences params R t;

%% Calculate global relative poses (translation scaling)

% Triangulate pairwise correspondences
triangulated_points = cell(num_cams-1, 1);
for cam_idx = 2:num_cams
    triangulated_points{cam_idx-1} = triangulate(correspondences{cam_idx-1}(:,1:2), ...
        correspondences{cam_idx-1}(:,3:4), [K [0; 0; 0]], ...
        K*[R_rel{cam_idx-1} t_rel{cam_idx-1}]);
end

% Rescale each translation to the same scale as neighbor stereo setup
scaling_factors_rel = ones(num_cams,1);
current_factor = 1;
for cam_idx = 3:num_cams
    % Find common target indices
    common_targets = [];
    for i = 1:size(correspondences{cam_idx-1},1)
        correspondence_idx = correspondences{cam_idx-1}(i,5);
        if ismember(correspondence_idx, correspondences{cam_idx-2}(:,5))
            corr_idx_prev = find(correspondences{cam_idx-2}(:,5) == correspondence_idx);
            corr_idx_curr = find(correspondences{cam_idx-1}(:,5) == correspondence_idx);

            common_targets = [common_targets; ...
                triangulated_points{cam_idx-2}(corr_idx_prev,:), ...
                triangulated_points{cam_idx-1}(corr_idx_curr,:)];
        end
    end

    % Calculate rescaling factors for common detections
    num_pairs = size(common_targets, 1) * (size(common_targets, 1) + 1) / 2;
    scaling = zeros(num_pairs,1);
    counter = 1;
    for common_idx = 1:size(common_targets, 1)
        for common_compare_idx = 1:common_idx-1
            dist_1 = norm(common_targets(common_idx,1:3) - common_targets(common_compare_idx,1:3));
            dist_2 = norm(common_targets(common_idx,4:6) - common_targets(common_compare_idx,4:6));
            scaling(counter) = dist_1/dist_2;
            counter = counter + 1;
        end
    end
    
    scaling_factors_rel(cam_idx) = median(scaling);
    current_factor = current_factor * scaling_factors_rel(cam_idx);

    % Rescale translation vectors
    t_rel{cam_idx-1} = current_factor * t_rel{cam_idx-1};

end

%% Calculate absolute transforms
R_abs = R_rel;
t_abs = t_rel;
for cam_idx = 3:num_cams
    R_abs{cam_idx-1} = R_abs{cam_idx-2} * R_abs{cam_idx-1};
    t_abs{cam_idx-1} = R_abs{cam_idx-2} * t_rel{cam_idx-1} + t_abs{cam_idx-2};
end

%% Load and plot ground truth data for comparison
gt_filename = data_folder+"cameras.json";
gt_data_string = fileread(gt_filename);
gt_data = struct2cell(jsondecode(gt_data_string));

P_gt = cell(size(gt_data,1),1);
names_gt = string(zeros(size(gt_data,1),1));
for gt_cam_idx = 1:size(gt_data,1)
    t = gt_data{gt_cam_idx}.Position_m_;
    R_euler = gt_data{gt_cam_idx}.Rotation_Euler_Radians_;

    R_initial = [1 0 0; 0 0 1; 0 -1 0];

    % XYZ Euler angles to rotation matrix
    c = cos(R_euler);
    s = sin(R_euler);
    R_X = [1 0 0; 0 c(1) -s(1); 0 s(1) c(1)];
    R_Y = [c(2) 0 -s(2); 0 1 0; s(2) 0 c(2)];
    R_Z = [c(3) -s(3) 0; s(3) c(3) 0; 0 0 1];

    R = R_Z*R_Y*R_X;

    P_gt{gt_cam_idx} = [R_initial*R R_initial*t; 0 0 0 1];
    names_gt(gt_cam_idx) = "GT Cam_"+string(gt_cam_idx);

end
plotCamerasAndDistances(P_gt, names_gt, 0.5, false);

%% Plot reconstructed results
P = cell(num_cams,1);
P{1} = P_gt{1};
names = string(zeros(num_cams,1));
names(1) = "Cam_1";
scale = norm(P_gt{1}(1:3,4)-P_gt{2}(1:3,4)) / norm(t_rel{1});
for cam_idx = 2:num_cams
    %norm(t_abs{1,cam_idx-1})
    P{cam_idx} = P_gt{1} * [R_abs{cam_idx-1} scale*t_abs{cam_idx-1}; 0 0 0 1];
    names(cam_idx) = "Cam_"+string(cam_idx);
end
plotCamerasAndDistances(P, names, 0.5, true);

