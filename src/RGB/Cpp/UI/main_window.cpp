#include "main_window.h"
#include "./ui_main_window.h"

#include <QDesktopServices>
#include <QDir>
#include <QFileDialog>

Main_Window::Main_Window(std::shared_ptr<Data> data, QWidget *parent)
    : QMainWindow(parent)
    , ui_(new Ui::Main_Window)
{
    data_ = data;
    ui_->setupUi(this);

    // Add data properties to UI
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->box_image_dir->layout());
    layout->addWidget(data_->image_dir_.get(), 0, 0);

    layout = static_cast<QGridLayout*>(ui_->box_naming_schemes->layout());
    layout->addWidget(data_->image_name_scheme_.get(), 0, 0);

    layout = static_cast<QGridLayout*>(ui_->box_intrinsics->layout());
    layout->addWidget(data_->intrinsics_within_single_file_.get(), 0, 0);
    layout->addWidget(data_->intrinsics_file_path_.get(), 1, 0);
    layout->addWidget(data_->intrinsics_folder_path_.get(), 1, 0);
    layout->addWidget(data_->camera_name_scheme_.get(), 2, 0);
    if(data_->intrinsics_within_single_file_->value()){
        data_->intrinsics_folder_path_->setHidden(true);
    }
    else{
        data_->intrinsics_file_path_->setHidden(true);
    }
    connect(data_->intrinsics_within_single_file_.get(), &Property_Check::valueChanged,
            [this](const bool& checked){
        if(checked){
            data_->intrinsics_file_path_->setHidden(false);
            data_->intrinsics_folder_path_->setHidden(true);
        }
        else{
            data_->intrinsics_file_path_->setHidden(true);
            data_->intrinsics_folder_path_->setHidden(false);
        }
    });

    // Add detector properties
    std::vector<std::shared_ptr<Property>> detector_properties = data_->detector_->getProperties();
    layout = static_cast<QGridLayout*>(ui_->box_correspondence_detection->layout());
    int counter = 0;
    for(std::shared_ptr<Property>& prop : detector_properties){
        layout->addWidget(prop.get(), counter, 0, 1, layout->columnCount());
        counter++;
    }

    // Add optimizer properties
    std::vector<std::shared_ptr<Property>> opt_initial_properties = data_->optimizer_->getPropertiesInitial();
    layout = static_cast<QGridLayout*>(ui_->box_initial_poses->layout());
    counter = 0;
    for(std::shared_ptr<Property>& prop : opt_initial_properties){
        layout->addWidget(prop.get(), counter, 0, 1, layout->columnCount());
        counter++;
    }
    std::vector<std::shared_ptr<Property>> opt_LM_properties = data_->optimizer_->getPropertiesLM();
    layout = static_cast<QGridLayout*>(ui_->box_bundle_adjustment->layout());
    counter = 0;
    for(std::shared_ptr<Property>& prop : opt_LM_properties){
        layout->addWidget(prop.get(), counter, 0, 1, layout->columnCount());
        counter++;
    }
    std::vector<std::shared_ptr<Property>> opt_eval_properties = data_->optimizer_->getPropertiesEval();
    for(std::shared_ptr<Property>& prop : opt_eval_properties){
        layout->addWidget(prop.get(), counter, 0, 1, layout->columnCount());
        counter++;
    }

    // Set initial visibility regarding the advanced options
    toggleAdvanced();

    // Add and connect advanced options checkbox
    layout = static_cast<QGridLayout*>(ui_->box_pose_calc_scroll->layout());
    layout->addWidget(data_->advanced_options_.get(), 0, 0);
    connect(data_->advanced_options_.get(), &Property_Check::valueChanged, this, &Main_Window::toggleAdvanced);

    // Connect simplified process buttons
    connect(ui_->button_calc_poses, &QPushButton::clicked, this, &Main_Window::detectionStartRequested);

    // Connect correspondence detection buttons
    connect(ui_->button_detect_correspondences, &QPushButton::clicked, this, &Main_Window::detectionStartRequested);
    connect(ui_->button_save_correspondences, &QPushButton::clicked, this, &Main_Window::saveDetectionsRequested);
    connect(ui_->button_load_correspondences, &QPushButton::clicked, this, &Main_Window::loadDetectionsRequested);

    // Connect further correspondence detection related signals
    connect(this, &Main_Window::correspondenceDetectionRequested, data_->detector_.get(), &Detector::detectCorrespondences);
    connect(data_->detector_.get(), &Detector::progress, this, &Main_Window::progress);
    connect(data_->detector_.get(), &Detector::ballsDetected, this, &Main_Window::receiveDetections);

    // Connect optimizer
    connect(ui_->button_calc_initial_poses, &QPushButton::clicked, this, &Main_Window::initialPoseEstimationRequested);
    connect(this, &Main_Window::initialPoseEstimation, data_->optimizer_.get(), &Optimizer::initialPoseEstimation);
    connect(data_->optimizer_.get(), &Optimizer::progress, this, &Main_Window::progress);
    connect(ui_->button_start_ba, &QPushButton::clicked, this, &Main_Window::bundleAdjustmentRequested);
    connect(this, &Main_Window::bundleAdjustment, data_->optimizer_.get(), &Optimizer::bundleAdjustment);

    // Connect logger
    connect(this, &Main_Window::log, ui_->logger, &Logger::log);
    connect(ui_->camera_visualization, &Camera_Vis::log, ui_->logger, &Logger::log);
    connect(data_.get(), &Data::log, ui_->logger, &Logger::log);
    connect(data_.get(), &Data::logMat, ui_->logger, &Logger::logMat);
    connect(data_->imagelist_gen_.get(), &Imagelist_Generator::log, ui_->logger, &Logger::log);
    connect(data_->detector_.get(), &Detector::log, ui_->logger, &Logger::log);
    connect(data_->optimizer_.get(), &Optimizer::log, ui_->logger, &Logger::log);
    connect(data_->optimizer_.get(), &Optimizer::logMat, ui_->logger, &Logger::logMat);
    connect(data_->optimizer_.get(), &Optimizer::logMatEig, ui_->logger, &Logger::logMatEig);
    connect(data_->optimizer_.get(), &Optimizer::logVecVar, ui_->logger, &Logger::logVecVar);

    // Connect IO
    connect(ui_->button_save_poses, &QPushButton::clicked, this, &Main_Window::savePosesRequested);
    connect(ui_->button_save_poses_simple, &QPushButton::clicked, this, &Main_Window::savePosesRequested);

    // Connect visualization
    connect(data_->optimizer_.get(), &Optimizer::initialPosesCalculated,
            [this](){
        ui_->camera_visualization->updateDevices(data_->devices_);
    });
    connect(data_->optimizer_.get(), &Optimizer::done,
            [this](const std::vector<std::vector<double>>& scene_points){
        ui_->camera_visualization->updateDevices(data_->devices_);
        ui_->camera_visualization->updatePointCloud(scene_points);
    });

}

Main_Window::~Main_Window(){
    // Unparent properties to prevent double deletion
    for(std::shared_ptr<Property>& prop : data_->property_list_){
        prop->setParent(nullptr);
    }

    delete ui_;
}

void Main_Window::progress(const float &value){
    ui_->progressbar->setValue(int(value*100.0f));
}

void Main_Window::toggleAdvanced(){
    // get current state of checkbox
    bool advanced = data_->advanced_options_->value();
    // set visibility of simple..
    ui_->box_simple_options->setHidden(advanced);
    // ..and advanced options
    ui_->box_correspondence_detection->setHidden(!advanced);
    ui_->box_initial_poses->setHidden(!advanced);
    ui_->box_bundle_adjustment->setHidden(!advanced);

    // automatically start bundle adjustment after intial pose estimation if
    // simple mode is active
    if(!advanced){
        connect(data_->optimizer_.get(), &Optimizer::initialPosesCalculated,
                this, &Main_Window::bundleAdjustmentRequested);
    }
    else{
        disconnect(data_->optimizer_.get(), &Optimizer::initialPosesCalculated,
                   this, &Main_Window::bundleAdjustmentRequested);
    }
}

void Main_Window::detectionStartRequested(){
    // checks if all relevant data has been set
    if(data_->image_dir_->value().size() == 0){
        ERROR("Please set the image dir!");
        return;
    }
    if(data_->intrinsics_within_single_file_->value()){
        if(data_->intrinsics_file_path_->value().size() == 0){
            ERROR("Intrinsic parameter file not set.");
            return;
        }
    }
    else{
        if(data_->intrinsics_folder_path_->value().size() == 0){
            ERROR("Intrinsic parameter directory not set.");
            return;
        }
    }
    if(data_->image_name_scheme_->value().size() == 0){
        ERROR("File name scheme not set!");
        return;
    }

    // Load camera parameters
    if(!data_->loadCalibration()){
        ERROR("Unable to load camera calibration!");
        return;
    }

    // emit signal to start detection with all relevant parameters
    emit correspondenceDetectionRequested(data_->image_dir_->value().toStdString(),
                                          data_->devices_,
                                          data_->image_name_scheme_->value(),
                                          false);
}

void Main_Window::initialPoseEstimationRequested(){
    if(!data_->detections_){
        ERROR("Detections not available");
        return;
    }
    emit initialPoseEstimation(data_->devices_, data_->detections_, false);
}

void Main_Window::bundleAdjustmentRequested(){
    if(!data_->detections_){
        ERROR("Detections not available");
        return;
    }
    emit bundleAdjustment(data_->devices_, data_->detections_);
}

void Main_Window::savePosesRequested(){
    // Open file dialog
    QString file_name_q = QFileDialog::getSaveFileName(this, "Choose or create a calibration file to store the poses",
                                                       data_->image_dir_->value());

    // Check resulting string
    std::string file_name = file_name_q.toStdString();
    if(file_name.size()==0){
        STATUS("No file chosen.");
        return;
    }
    // If valid, save image dir path
    data_->saveCalibration(file_name);
}

void Main_Window::receiveDetections(const std::shared_ptr<Detections>& detections){
    data_->detections_ = detections;
    // in the simplified process, directly start the initial pose estimation
    if(! data_->advanced_options_->value()){
        initialPoseEstimationRequested();
    }
}

void Main_Window::saveDetectionsRequested(){
    // Open file dialog
    QString file_name_q = QFileDialog::getSaveFileName(this, "Choose or create a file to store the detections",
                                                       data_->image_dir_->value());

    // Check resulting string
    std::string file_name = file_name_q.toStdString();
    if(file_name.size()==0){
        STATUS("No file chosen.");
        return;
    }

    if(file_name.substr(file_name.length() - 4).compare("json") != 0){
        file_name += ".json";
    }

    // If valid, save image dir path
    data_->saveDetections(file_name);
}

void Main_Window::loadDetectionsRequested(){
    // Open file dialog
    QString file_name_q = QFileDialog::getOpenFileName(this, "Choose or a file containint detections",
                                                       data_->image_dir_->value());

    // Check resulting string
    std::string file_name = file_name_q.toStdString();
    if(file_name.size()==0){
        STATUS("No file chosen.");
        return;
    }

    // If valid, save image dir path
    data_->loadDetections(file_name);

    // Also prepare further data

    if(!data_->loadCalibration()){
        ERROR("Unable to load camera calibration!");
        return;
    }
    emit correspondenceDetectionRequested(data_->image_dir_->value().toStdString(),
                                          data_->devices_,
                                          data_->image_name_scheme_->value(),
                                          true);
}
