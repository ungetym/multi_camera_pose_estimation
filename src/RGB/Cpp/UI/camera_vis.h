#pragma once

#include "Backend/data_types.h"
#include "gl_control.h"

class Camera_Vis : public GL_Control
{
    Q_OBJECT

public:
    Camera_Vis(QWidget* parent);

public slots:

    ///
    /// \brief updateDevices
    /// \param devices
    ///
    void updateDevices(const std::vector<Device>& devices){
        devices_ = devices;
        updateParameters();
    }

    void updatePointCloud(const std::vector<std::vector<double>>& scene_points);

private:

    ///GL functions
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    ///
    /// \brief updateParameters
    ///
    void updateParameters();

private:

    int num_scene_points_ = 0;

    std::vector<Device> devices_;

    GLuint shader_program_pcl_;
    GLuint vao_pcl_, vbo_pcl_;

    GLuint shader_program_cam_;
    GLuint vao_, vbo_cam_, vbo_elements_, vbo_positions_,
    vbo_rotations_, vbo_params_, vbo_colors_;

    std::vector<GLfloat> cam_modell_ = {
        0.0f,0.0f,0.0f,
        1.0f,1.0f,1.0f,
        1.0f,-1.0f,1.0f,
        -1.0f,-1.0f,1.0f,
        -1.0f,1.0f,1.0f
    };
    std::vector<GLuint> cam_elements_ = {
        0, 1,
        0, 2,
        0, 3,
        0, 4,
        1, 2,
        2, 3,
        3, 4,
        4, 1
    };

    GLuint shader_program_axes_;
    GLuint vao_axes_, vbo_elements_axes_, vbo_axes_;

    std::vector<GLfloat> axes_modell_ = {
        0.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        0.0f,-1.0f,0.0f,//flip in order to show cam frame instead of opengl lefthanded frame
        0.0f,0.0f,-1.0f//
    };

    std::vector<GLuint> axes_elements_ = {
        0, 1,
        0, 2,
        0, 3
    };

    const char* vertex_shader_pcl_ =
            "#version 400\n"
            "layout (location = 0) in vec3 vertex_pos;"
            "uniform mat4 mvp;"
            "void main () {"
            "   gl_Position = mvp * vec4(vertex_pos,1.0);"
            "}";

    const char* fragment_shader_pcl_ =
            "#version 400\n"
            "out vec4 frag_color;"
            "void main () {"
            "   frag_color = vec4(1,0,1,0);"
            "}";

    const char* vertex_shader_cam_ =
            "#version 400\n"
            "layout (location = 0) in vec3 vertex_pos;"
            "layout (location = 1) in vec3 cam_color;"
            "layout (location = 2) in vec3 cam_pos;"
            "layout (location = 3) in vec3 cam_params;"
            "layout (location = 4) in mat3 cam_rot;"
            "uniform mat4 mvp;"
            "out vec4 color_coord;"
            "out vec3 color;"
            "void main () {"
            "   vec3 temp = vertex_pos;"
            "   temp.x *= cam_params.x;"
            "   temp.y *= cam_params.y;"
            "   temp.z *= cam_params.z;"
            "   temp = cam_rot*temp;"
            "   temp += cam_pos;"
            "   gl_Position = mvp*vec4(temp,1.0);"
            "   color_coord=gl_Position;"
            "   color = cam_color;"
            "}";

    const char* fragment_shader_cam_ =
            "#version 400\n"
            "in vec4 color_coord;"
            "in vec3 color;"
            "out vec4 frag_color;"
            "void main () {"
            "   frag_color = vec4(color,0);"
            "}";

    const char* vertex_shader_axes_ =
            "#version 400\n"
            "layout (location = 0) in vec3 vertex_pos;"
            "uniform mat4 mvp;"
            "uniform float scale;"
            "out vec3 color_coord;"
            "void main () {"
            "   gl_Position=mvp*vec4(scale*vertex_pos,1.0);"
            "   color_coord=vertex_pos;"
            "}";

    const char* fragment_shader_axes_ =
            "#version 400\n"
            "in vec3 color_coord;"
            "out vec4 frag_color;"
            "void main () {"
            "   if(color_coord.x!=0){"
            "       frag_color = vec4(1,0,0,0);"
            "   }"
            "   else if(color_coord.y!=0){"
            "       frag_color = vec4(0,1,0,0);"
            "   }"
            "   else if(color_coord.z!=0){"
            "       frag_color = vec4(0,0,1,0);"
            "   }"
            "}";

signals:
    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);

};
