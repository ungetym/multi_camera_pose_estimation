#include "gl_control.h"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <QDir>

GL_Control::GL_Control(QWidget* parent) :
    QOpenGLWidget(parent)
{
    this->setObjectName("GL_Control");
    this->setFocusPolicy(Qt::StrongFocus);
}

///////////////////////////    Matrix calculations    //////////////////////////

void GL_Control::calculateViewMatrix(){
    Eigen::Vector3f forward, side, up;
    forward=cam_center-cam_position;
    forward=forward.normalized();
    side={-forward[2],0.0,forward[0]};
    side=side.normalized();
    up=side.cross(forward);
    //Rotation
    view_mat.block(0,0,1,3)=side.transpose();
    view_mat.block(1,0,1,3)=up.transpose();
    view_mat.block(2,0,1,3)=-forward.transpose();
    //Translation
    Eigen::Vector3f cam_transformed = view_mat.block(0,0,3,3)*cam_position;
    view_mat.block(0,3,3,1)=-cam_transformed;
}

void GL_Control::calculateProjectionMatrix(float fovy, float aspect_ratio, float near, float far){
    if(orthographic_view){
        far*=10;
        float x_max, y_max;
        y_max = orthographic_zoom* near * tanf(fovy * M_PI / 360.0);
        x_max = y_max * aspect_ratio;
        projection_mat<<1.0/x_max, 0.0,       0.0,            0.0,
                0.0,       1.0/y_max, 0.0,            0.0,
                0.0,       0.0,       2.0/(near-far), (far+near)/(near-far),
                0.0,       0.0,       0.0,            1.0;
    }
    else{
        float x_max, y_max;
        y_max = near * tanf(fovy * M_PI / 360.0);
        x_max = y_max * aspect_ratio;
        projection_mat<<near/x_max, 0.0, 0.0, 0.0,
                0.0, near/y_max, 0.0, 0.0,
                0.0, 0.0, (far+near)/(near-far), 2.0*far*near/(near-far),
                0.0, 0.0, -1.0, 0.0;
    }
}


////////////////////    Camera control via mouse and keyboard   ////////////////

void GL_Control::mouseMoveEvent(QMouseEvent* e){
    if(e->buttons()==Qt::LeftButton){
        if(last_cursor_position.first!=-1000){
            //rotate cam_position around center
            float phi = fmod(cam_sphere_coord[0]+0.01*(e->localPos().x()-last_cursor_position.first),2.0*M_PI);
            float psi = fmod(cam_sphere_coord[1]+0.01*(e->localPos().y()-last_cursor_position.second),2.0*M_PI);
            rad = cam_sphere_coord[2];
            cam_position={rad*cosf(phi)*cosf(psi),rad*sinf(psi),rad*sinf(phi)*cosf(psi)};
            cam_position+=cam_center;
            cam_sphere_coord={phi,psi,rad};
            calculateViewMatrix();

            this->update();
        }
        last_cursor_position={e->localPos().x(),e->localPos().y()};
    }
    else if(e->buttons()==Qt::MiddleButton){
        if(last_cursor_position.first!=-1000){
            //move cam_position and center orthogonal to view direction
            Eigen::Vector3f forward, side, up, translate;
            forward=cam_center-cam_position;
            forward=forward.normalized();
            side={-forward[2],0.0,forward[0]};
            side=side.normalized();
            up=side.cross(forward);
            translate=side*(last_cursor_position.first-e->localPos().x())+up*(-last_cursor_position.second+e->localPos().y());
            translate*=mouse_movement_scale;
            cam_position+=translate;
            cam_center+=translate;
            calculateViewMatrix();

            this->update();
        }
        last_cursor_position={e->localPos().x(),e->localPos().y()};
    }
}

void GL_Control::mousePressEvent(QMouseEvent* e){
    if(e->buttons()==Qt::LeftButton || e->buttons()==Qt::MiddleButton){
        last_cursor_position={e->localPos().x(),e->localPos().y()};
    }

}

void GL_Control::wheelEvent(QWheelEvent* e){
    if(orthographic_view){
        orthographic_zoom-=(float)(orthographic_zoom_scale*e->angleDelta().ry());
        calculateProjectionMatrix(90.0, (float)this->width()/(float)this->height(), 0.01,600.0);
    }

    rad = (cam_sphere_coord[2]-=(float)(mouse_wheel_scale*e->angleDelta().ry()));
    float phi = cam_sphere_coord[0];
    float psi = cam_sphere_coord[1];
    cam_position={rad*cosf(phi)*cosf(psi),rad*sinf(psi),rad*sinf(phi)*cosf(psi)};
    cam_position+=cam_center;
    calculateViewMatrix();

    this->update();
}

void GL_Control::keyPressEvent(QKeyEvent* e){
    if(e->key()==Qt::Key_Left){
        Eigen::Vector3f move={cam_position[2]-cam_center[2],0.0,cam_center[0]-cam_position[0]};
        move=0.1*move.normalized();
        cam_center-=move;
        cam_position-=move;
    }
    else if(e->key()==Qt::Key_Right){
        Eigen::Vector3f move={cam_position[2]-cam_center[2],0.0,cam_center[0]-cam_position[0]};
        move=0.1*move.normalized();
        cam_center+=move;
        cam_position+=move;
    }
    else if(e->key()==Qt::Key_Up){
        Eigen::Vector3f move={0.0,0.1,0.0};
        cam_center+=move;
        cam_position+=move;
    }
    else if(e->key()==Qt::Key_Down){
        Eigen::Vector3f move={0.0,0.1,0.0};
        cam_center-=move;
        cam_position-=move;
    }
    else if(e->key()==Qt::Key_R){//reset
        cam_center={0.0,0.0,0.0};
        cam_position={0.0,0.0,3.0};
        cam_sphere_coord={0.5*M_PI,0.0,3.0};
    }
    calculateViewMatrix();

    this->update();
}
