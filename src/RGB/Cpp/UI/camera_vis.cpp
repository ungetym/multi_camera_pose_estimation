#include "camera_vis.h"
#include "logger.h"

Camera_Vis::Camera_Vis(QWidget *parent) :
    GL_Control(parent)
{
    mouse_wheel_scale*=10.0;
    mouse_movement_scale*=10.0;
    orthographic_zoom_scale = 0.5;
    orthographic_zoom = 1000.0;
}

void Camera_Vis::updatePointCloud(const std::vector<std::vector<double>>& scene_points){

    // Cast double to float
    std::vector<GLfloat> scene_points_f;
    for (const auto& p : scene_points) {
        scene_points_f.push_back(GLfloat(p[0]));
        scene_points_f.push_back(-GLfloat(p[1]));
        scene_points_f.push_back(-GLfloat(p[2])); //switch for OpenGL
    }
    num_scene_points_ = scene_points_f.size()/3;

    // Update buffer
    glBindBuffer (GL_ARRAY_BUFFER, vbo_pcl_);
    glBufferData (GL_ARRAY_BUFFER, scene_points_f.size()*sizeof(GLfloat), scene_points_f.data(), GL_STATIC_DRAW);
    glBindBuffer (GL_ARRAY_BUFFER, 0);

    this->update();
}

void Camera_Vis::updateParameters(){

    std::vector<GLfloat> positions, rotations, scalings, colors;

    // Create colors
    auto sampleColorMap = [](const size_t& num_samples, const cv::ColormapTypes& map){
        // Create a gradient image with num_samples levels
        cv::Mat gradient(num_samples, 1, CV_8UC1);
        for (int i = 0; i < num_samples; ++i) {
            gradient.at<uchar>(i, 0) = static_cast<uchar>(i * 255 / (num_samples - 1));
        }

        // Apply the colormap to the gradient image
        cv::Mat colored;
        cv::applyColorMap(gradient, colored, map);

        // Convert the colored image to a vector of floats
        std::vector<float> colors;
        for (int i = 0; i < num_samples; ++i) {
            cv::Vec3b color = colored.at<cv::Vec3b>(i, 0);
            colors.push_back(color[0] / 255.0f);
            colors.push_back(color[1] / 255.0f);
            colors.push_back(color[2] / 255.0f);
        }

        return colors;
    };

    colors = sampleColorMap(devices_.size(), cv::COLORMAP_AUTUMN);

    // Store camera positions and rotations
    Eigen::Matrix3d cv2gl_frame_change;
    cv2gl_frame_change << 1.0, 0.0, 0.0,
            0.0, -1.0, 0.0,
            0.0, 0.0, -1.0;


    float min_cam_dist = 9999999;
    std::vector<Eigen::Vector3f> positions_temp;

    for(const Device& dev : devices_){

        Eigen::Vector3d pos = - cv2gl_frame_change * dev.world_pose_.R_.transpose() * dev.world_pose_.t_;
        Eigen::Vector3f pos_f = pos.cast<float>();

        for(const Eigen::Vector3f& neighbor_pos_f : positions_temp){
            float dist = (neighbor_pos_f-pos_f).norm();
            if(dist > 0.001){
                min_cam_dist = std::min(min_cam_dist, (neighbor_pos_f-pos_f).norm());
            }
        }
        positions_temp.push_back(pos_f);

        positions.insert(positions.end(), pos_f.data(),pos_f.data()+3);

        Eigen::Matrix3d rotation = cv2gl_frame_change*dev.world_pose_.R_.transpose();
        Eigen::Matrix3f rotation_f = rotation.cast<float>();
        rotations.insert(rotations.end(), rotation_f.data(),rotation_f.data()+9);


    }

    // Calculate model scalings
    float model_scale = std::max(0.3 * min_cam_dist, 0.05);
    for(const Device& dev : devices_){

        if( dev.rgb_.K_.at<double>(0,0) > dev.rgb_.res_x_){
            scalings.push_back(model_scale / float(dev.rgb_.K_.at<double>(0,0)) *dev.rgb_.res_x_);
            scalings.push_back(model_scale / float(dev.rgb_.K_.at<double>(0,0)) *dev.rgb_.res_y_);
            scalings.push_back(model_scale);
        }
        else{
            scalings.push_back(model_scale);
            scalings.push_back(model_scale * dev.rgb_.res_y_ / dev.rgb_.res_x_);
            scalings.push_back(model_scale * float(dev.rgb_.K_.at<double>(0,0)) / dev.rgb_.res_x_);
        }
    }

    // Update vbos
    glBindBuffer (GL_ARRAY_BUFFER, vbo_colors_);
    glBufferData (GL_ARRAY_BUFFER, colors.size()*sizeof(GLfloat), colors.data(), GL_STATIC_DRAW);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_positions_);
    glBufferData (GL_ARRAY_BUFFER, positions.size()*sizeof(GLfloat), positions.data(), GL_STATIC_DRAW);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_rotations_);
    glBufferData (GL_ARRAY_BUFFER, rotations.size()*sizeof(GLfloat), rotations.data(), GL_STATIC_DRAW);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_params_);
    glBufferData (GL_ARRAY_BUFFER, scalings.size()*sizeof(GLfloat), scalings.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    this->update();
}

//////////////////////////////    GL functions    //////////////////////////////

void Camera_Vis::initializeGL(){
    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Compile shaders
    GLuint vs_pcl = glCreateShader (GL_VERTEX_SHADER);
    glShaderSource (vs_pcl, 1, &vertex_shader_pcl_, NULL);
    glCompileShader (vs_pcl);
    GLuint fs_pcl = glCreateShader (GL_FRAGMENT_SHADER);
    glShaderSource (fs_pcl, 1, &fragment_shader_pcl_, NULL);
    glCompileShader (fs_pcl);
    GLuint vs = glCreateShader (GL_VERTEX_SHADER);
    glShaderSource (vs, 1, &vertex_shader_cam_, NULL);
    glCompileShader (vs);
    GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
    glShaderSource (fs, 1, &fragment_shader_cam_, NULL);
    glCompileShader (fs);
    GLuint vs_axes = glCreateShader (GL_VERTEX_SHADER);
    glShaderSource (vs_axes, 1, &vertex_shader_axes_, NULL);
    glCompileShader (vs_axes);
    GLuint fs_axes = glCreateShader (GL_FRAGMENT_SHADER);
    glShaderSource (fs_axes, 1, &fragment_shader_axes_, NULL);
    glCompileShader (fs_axes);

    // Check for compile errors
    int status;
    glGetShaderiv(vs_pcl, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("PCL vertex shader compile error.");
    }
    glGetShaderiv(fs_pcl, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("PCL fragment shader compile error.");
    }
    glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("Cam vertex shader compile error.");
    }
    glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("Cam fragment shader compile error.");
    }
    glGetShaderiv(vs_axes, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("Axes vertex shader compile error.");
    }
    glGetShaderiv(fs_axes, GL_COMPILE_STATUS, &status);
    if(!status){
        ERROR("Axes fragment shader compile error.");
    }

    // Attach and link shaders
    shader_program_pcl_ = glCreateProgram();
    glAttachShader (shader_program_pcl_, vs_pcl);
    glAttachShader (shader_program_pcl_, fs_pcl);
    glLinkProgram (shader_program_pcl_);

    shader_program_cam_ = glCreateProgram();
    glAttachShader (shader_program_cam_, vs);
    glAttachShader (shader_program_cam_, fs);
    glLinkProgram (shader_program_cam_);

    shader_program_axes_ = glCreateProgram();
    glAttachShader (shader_program_axes_, vs_axes);
    glAttachShader (shader_program_axes_, fs_axes);
    glLinkProgram (shader_program_axes_);

    // Init elements
    vbo_elements_ = 0;
    glGenBuffers (1, &vbo_elements_);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, vbo_elements_);
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, cam_elements_.size()*sizeof(GLuint), cam_elements_.data(), GL_DYNAMIC_DRAW);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

    vbo_elements_axes_ = 0;
    glGenBuffers (1, &vbo_elements_axes_);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, vbo_elements_axes_);
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, axes_elements_.size()*sizeof(GLuint), axes_elements_.data(), GL_DYNAMIC_DRAW);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

    // Init vaos and vbos

    glGenVertexArrays(1, &vao_pcl_);
    glGenBuffers (1, &vbo_pcl_);

    vbo_cam_ = 0;
    glGenBuffers (1, &vbo_cam_);
    glBindBuffer (GL_ARRAY_BUFFER, vbo_cam_);
    glBufferData (GL_ARRAY_BUFFER, cam_modell_.size()*sizeof(GLfloat), cam_modell_.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glEnableVertexAttribArray(0);

    glGenVertexArrays(1, &vao_);
    glGenBuffers (1, &vbo_colors_);
    glGenBuffers (1, &vbo_positions_);
    glGenBuffers (1, &vbo_rotations_);
    glGenBuffers (1, &vbo_params_);

    glGenVertexArrays(1, &vao_axes_);
    glGenBuffers(1, &vbo_axes_);

    // Bind buffers

    glBindVertexArray (vao_pcl_);
    glBindBuffer (GL_ARRAY_BUFFER, vbo_pcl_);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);


    glBindVertexArray (vao_);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_cam_);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glEnableVertexAttribArray(0);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_colors_);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glVertexAttribDivisor(1,1);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_positions_);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glVertexAttribDivisor(2,1);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_params_);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glVertexAttribDivisor(3,1);

    glBindBuffer (GL_ARRAY_BUFFER, vbo_rotations_);
    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (void*)(0));
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (void*)(sizeof(float)*3));
    glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, 9*sizeof(GLfloat), (void*)(sizeof(float)*6));
    glVertexAttribDivisor(4,1);
    glVertexAttribDivisor(5,1);
    glVertexAttribDivisor(6,1);

    glBindVertexArray(0);


    glBindVertexArray(vao_axes_);
    glBindBuffer (GL_ARRAY_BUFFER, vbo_axes_);
    glBufferData (GL_ARRAY_BUFFER, axes_modell_.size()*sizeof(GLfloat), axes_modell_.data(), GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), NULL);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    //calculate initial mvp matrix
    last_cursor_position={-1000,-1000};
    model_mat = Eigen::Matrix4f::Identity(4,4);
    view_mat = Eigen::Matrix4f::Identity(4,4);
    projection_mat = Eigen::Matrix4f::Identity(4,4);
    cam_center={0.0,0.0,0.0};
    rad = 3.0;
    cam_position={0.0,0.0,rad};
    cam_sphere_coord={0.5*M_PI,0.0,rad};

    calculateViewMatrix();
    calculateProjectionMatrix(90.0,1.0,0.01,600.0);
}

void Camera_Vis::resizeGL(int w, int h){
    this->glViewport(0,0, w, h);
    calculateProjectionMatrix(90.0, (float)w/(float)h, 0.01,600.0);
}

void Camera_Vis::paintGL(){
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Calculate transformation matrix
    Eigen::Matrix4f trafo = projection_mat*view_mat*model_mat;

    // Draw scene points
    glDisable(GL_DEPTH_TEST);

    glUseProgram (shader_program_pcl_);
    GLuint location = glGetUniformLocation(shader_program_pcl_, "mvp");
    glUniformMatrix4fv(location, 1, GL_FALSE, trafo.data());

    glBindVertexArray(vao_pcl_);
    glPointSize(5.0f);
    glDrawArrays(GL_POINTS, 0, num_scene_points_);
    glBindVertexArray(0);

    glUseProgram(0);

    //glEnable(GL_DEPTH_TEST);

    // Draw cameras
    glUseProgram (shader_program_cam_);
    location = glGetUniformLocation(shader_program_cam_, "mvp");
    glUniformMatrix4fv(location, 1, GL_FALSE, trafo.data());

    glBindVertexArray(vao_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_);
    glDrawElementsInstanced(GL_LINES, cam_elements_.size(), GL_UNSIGNED_INT, 0, devices_.size());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    glUseProgram(0);

    // Draw axes
    glUseProgram (shader_program_axes_);
    location = glGetUniformLocation(shader_program_axes_, "mvp");
    glUniformMatrix4fv(location, 1, GL_FALSE, trafo.data());
    location = glGetUniformLocation(shader_program_axes_, "scale");
    glUniform1f(location, 0.3*rad);

    glBindVertexArray(vao_axes_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_axes_);
    glDrawElements(GL_LINES, axes_elements_.size(), GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
    glBindVertexArray(0);

}
