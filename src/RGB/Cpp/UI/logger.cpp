#include "logger.h"
#include <opencv2/core/eigen.hpp>

#include <QTime>

Logger::Logger(QWidget *parent) : QTextEdit(parent){
}

void Logger::log(const QString& msg, const int& type){
    QString new_log;
    if(type==LOG_ERROR){//set error text
        QObject* sender = QObject::sender();
        new_log+=QTime::currentTime().toString()+": "+sender->objectName()+" Error: ";
        setTextColor(QColor::fromRgb(255,0,0));
    }
    else if(type==LOG_NORMAL){//set status text
        new_log+=QTime::currentTime().toString()+": ";
        setTextColor(QColor::fromRgb(0,0,0));
    }
    else if(type==LOG_WARNING){//set warning text
        new_log+=QTime::currentTime().toString()+": Warning: ";
        setTextColor(QColor::fromRgb(255,128,0));
    }
    new_log+=msg;

    if(type==LOG_APPEND){//append msg to last logger entry
        insertPlainText(new_log);
    }
    else{
        append(new_log);
    }
    repaint();
}

void Logger::logMat(const QString& msg, const cv::Mat& mat){
    setTextColor(QColor::fromRgb(0,0,0));
    append(msg);
    // row-wise logging of the given matrix
    for(int row = 0; row<mat.rows; row++){
        QString mat_row = "  ";
        for(int col = 0; col<mat.cols; col++){
            if(mat.type()==CV_64F){
                mat_row += QString::number((float)mat.at<double>(row,col), 'G', 6)+"\t";
            }
            else if(mat.type()==CV_32F){
                mat_row += QString::number((float)mat.at<float>(row,col), 'G', 6)+"\t";
            }
        }
        append(mat_row);
    }
    append(" ");
    repaint();
}

void Logger::logMatEig(const QString& msg, const Eigen::MatrixXd& mat){
    cv::Mat mat_cv;
    cv::eigen2cv(mat, mat_cv);
    logMat(msg, mat_cv);
}

void Logger::logVecVar(const QString& msg, const Eigen::VectorXd& vec, const Eigen::VectorXd& var){
    setTextColor(QColor::fromRgb(0,0,0));
    append(msg);
    // row-wise logging of the given matrix
    for(int row = 0; row<vec.size(); row++){
        QString mat_row = "  ";
        mat_row += QString::number((float)vec[row], 'G', 6) + " +- "
                + QString::number((float)var[row], 'G', 6)+"\t";
        append(mat_row);
    }
    append(" ");
    repaint();
}
