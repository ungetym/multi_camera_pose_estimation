#pragma once

#include "Backend/data_types.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_5_Core>
#include <QMouseEvent>
#include <QKeyEvent>


class GL_Control : public QOpenGLWidget, protected QOpenGLFunctions_4_5_Core
{
    Q_OBJECT

public:
    GL_Control(QWidget* parent);

protected slots:
    void toggleOrthographic(){
        orthographic_view = !orthographic_view;
        calculateProjectionMatrix(90.0, (float)this->width()/(float)this->height(), 0.01,600.0);
    }

protected:
    // For mouse action scaling
    float mouse_wheel_scale = 0.0001;
    float mouse_movement_scale = 0.001;

    float orthographic_zoom_scale = 0.1;
    float orthographic_zoom = 100.0;

    float rad = 1.0;

    // Matrix calculations
    bool orthographic_view = false;
    void calculateViewMatrix();
    void calculateProjectionMatrix(float fovy, float aspect_ratio, float near, float far);

    // Camera control via mouse and keyboard
    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void wheelEvent(QWheelEvent* e);
    void keyPressEvent(QKeyEvent* e);

    Eigen::Matrix4f projection_mat, view_mat, model_mat;
    Eigen::Vector3f cam_center, cam_position, cam_sphere_coord;
    std::pair<int,int> last_cursor_position;
};
