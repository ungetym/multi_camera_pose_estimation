////////////////////////////////////////////////////////////////////////////////
///                              Main Window GUI                             ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Backend/data.h>
#include <QMainWindow>
#include <QThread>

QT_BEGIN_NAMESPACE
namespace Ui { class Main_Window; }
QT_END_NAMESPACE

///
/// \brief The Main_Window class creates the GUI and is the interface to the
///                              different program parts, such as detector and
///                              optimizer
///
class Main_Window : public QMainWindow{

    Q_OBJECT

public:
    ///
    /// \brief Main_Window is the default constructor
    /// \param data        pointer to the instance of the data class
    /// \param parent
    ///
    Main_Window(std::shared_ptr<Data> data, QWidget *parent = nullptr);

    /// default destructor
    ~Main_Window();

private slots:

    ///
    /// \brief progress sets the progress bar status
    /// \param value    between 0 and 1 for showing 0% to 100%
    ///
    void progress(const float& value);

    ///
    /// \brief toggleAdvanced de-/activates the advanced pose estimation options
    ///
    void toggleAdvanced();

    ///
    /// \brief detectionStartRequested initiates the table tennis ball detection
    ///
    void detectionStartRequested();

    ///
    /// \brief startInitialPoseEstimation emits the necessary signal for
    ///                                   starting the pose estimation via SVD
    ///                                   and RANSAC
    ///
    ///
    void initialPoseEstimationRequested();

    ///
    /// \brief bundleAdjustmentRequested emits the necessary signal for starting
    ///                                  the bundle adjustment procedure
    ///
    void bundleAdjustmentRequested();

    ///
    /// \brief savePosesRequested opens a file dialog for the user to select the
    ///                           file to which the poses should be saved
    ///
    void savePosesRequested();

    ///
    /// \brief receiveDetections saves new detections and in the case of the
    ///                          simplified process it initiates the
    ///                          optimization
    /// \param detections        as signaled by the ball detector
    ///
    void receiveDetections(const std::shared_ptr<Detections>& detections);

    ///
    /// \brief saveDetectionsRequested
    ///
    void saveDetectionsRequested();

    ///
    /// \brief loadDetectionsRequested
    ///
    void loadDetectionsRequested();

private:
    /// UI class containing the window elements
    Ui::Main_Window* ui_;

    /// Pointer to the data class holding all relevant information
    std::shared_ptr<Data> data_;

signals:

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief correspondencDetectionRequested is the signal to start the
    ///                              ball detection procedure. Check the equally
    ///                              named function in the detector class.
    /// \param image_dir
    /// \param devices
    /// \param rgb_scheme
    /// \param only_prepare_data
    ///
    void correspondenceDetectionRequested(const std::string& image_dir,
                                          std::vector<Device>& devices,
                                          const QString& rgb_scheme,
                                          const bool& only_prepare_data);

    ///
    /// \brief initialPoseEstimation is the signal to start the equally named
    ///                              function in the optimizer class, which
    ///                              calculates the initial poses by SVD and
    ///                              RANSAC.
    /// \param devices
    /// \param detections
    /// \param max_iterations
    /// \param inlier_threshold
    ///
    void initialPoseEstimation(std::vector<Device>& devices,
                               const std::shared_ptr<Detections>& detections,
                               const bool& reinitialization);

    ///
    /// \brief bundleAdjustment is the signal to start the equally named
    ///                         function in the optimizer class, which
    ///                         calculates the final poses based on non-linear
    ///                         optimization.
    /// \param devices
    /// \param detections
    /// \param max_iterations
    /// \param change_threshold
    ///
    void bundleAdjustment(std::vector<Device>& devices,
                          const std::shared_ptr<Detections>& detections);


};
