////////////////////////////////////////////////////////////////////////////////
///         Logger widget showing current status updates and errors          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <eigen3/Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <QTextEdit>

// define constants used for communicating the message type and therefore
// specify its representation in the text window
const int LOG_NORMAL = 0;
const int LOG_WARNING = 1;
const int LOG_ERROR = 2;
const int LOG_APPEND = 3;

// define logger macros for different types of messages
#define ERROR(msg) emit log(msg, LOG_ERROR)
#define WARN(msg) emit log(msg, LOG_WARNING)
#define STATUS(msg) emit log(msg, LOG_NORMAL)
#define LOGMATRIX(msg, mat) emit logMat(msg, mat)
#define LOGVECVAR(msg, vec, var) emit logVecVar(msg, vec, var)

///
/// \brief The Logger class creates a QTextEdit widget to show different types
///                         of status messages
///
class Logger : public QTextEdit{

    Q_OBJECT

public:
    ///
    /// \brief Logger is the standard constructor
    /// \param parent
    ///
    explicit Logger(QWidget *parent = nullptr);

public slots:

    ///
    /// \brief log  creates a time-stamped message in the logging window
    /// \param msg  the message to display
    /// \param type defines the color depending on the LOG_* type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief logMat   creates a time-stamped message showing an OpenCV matrix
    /// \param msg      message to display in addition to the matrix
    /// \param mat      the matrix to show
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

    ///
    /// \brief logMatEig  creates a time-stamped message showing an Eigen matrix
    /// \param msg      message to display in addition to the matrix
    /// \param mat      the matrix to show
    ///
    void logMatEig(const QString& msg, const Eigen::MatrixXd& mat);

    ///
    /// \brief logVecVar
    /// \param msg
    /// \param vec
    /// \param var
    ///
    void logVecVar(const QString& msg, const Eigen::VectorXd& vec, const Eigen::VectorXd& var);
};
