#pragma once

#include <QDialog>
#include <QKeyEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QPainter>
#include <QPixmap>
#include <QScrollArea>
#include <QVBoxLayout>

class Image_Annotation : public QDialog
{
    Q_OBJECT
public:

    ///
    /// \brief Image_Annotation
    /// \param path
    /// \param parent
    ///
    explicit Image_Annotation(const QString& path, QWidget* parent = nullptr)
        : QDialog(parent)
    {
        setModal(true);
        label_ = new QLabel(this);
        image_ = QImage(path);
        label_->setPixmap(QPixmap::fromImage(image_));
        label_->setMouseTracking(true);
        label_->installEventFilter(this);

        scroll_area_ = new QScrollArea(this);
        scroll_area_->setWidget(label_);
        scroll_area_->setWidgetResizable(true);
        scroll_area_->setAlignment(Qt::AlignCenter);

        QVBoxLayout* layout = new QVBoxLayout(this);
        layout->addWidget(scroll_area_);
        layout->setContentsMargins(0, 0, 0, 0);

        setLayout(layout);

        setWindowTitle("Please click on the target or press ESC!");
        resize(image_.width(), image_.height());
    }

    ///
    /// \brief getClicked
    /// \return
    ///
    QPoint getCenter() const{
        return center_;
    }

    ///
    /// \brief getRadius
    /// \return
    ///
    int getRadius() const{
        return radius_;
    }

protected:

    ///
    /// \brief eventFilter
    /// \param obj
    /// \param event
    /// \return
    ///
    bool eventFilter(QObject* obj, QEvent* event) override{
        if (event->type() == QEvent::KeyPress){
            QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_Escape){
                center_ = QPoint(-1, -1);
                close();
            }
        }

        if (obj == label_) {
            QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
            if (event->type() == QEvent::MouseButtonPress) {
                if (mouse_event->button() == Qt::LeftButton){
                    if (!center_set_) {
                        // Transform clicked point into image coordinates
                        center_viewport_ = scroll_area_->viewport()->mapFromGlobal(mouse_event->globalPos());
                        center_ = static_cast<QLabel*>(scroll_area_->widget())->mapFrom(scroll_area_->viewport(), center_viewport_);
                        center_set_ = true;
                        // Draw center onto image
                        QPainter painter(&image_);
                        painter.setPen(QPen(Qt::red, 1));
                        painter.drawEllipse(QPointF(center_viewport_), 1, 1);
                    } else {
                        QPoint viewport_point = scroll_area_->viewport()->mapFromGlobal(mouse_event->globalPos());
                        QPoint circle_point = static_cast<QLabel*>(scroll_area_->widget())->mapFrom(scroll_area_->viewport(), viewport_point);
                        radius_ = QLineF(center_, circle_point).length();
                        accept();
                    }
                }
            }
            else if (center_set_ && event->type() == QEvent::MouseMove) {
                QImage temp_image = image_.copy();
                QPainter painter(&temp_image);
                painter.setPen(QPen(Qt::red, 2));

                QPoint viewport_point = scroll_area_->viewport()->mapFromGlobal(mouse_event->globalPos());

                float radius = QLineF(center_viewport_, viewport_point).length();
                painter.drawEllipse(QPointF(center_viewport_), radius, radius);
                painter.end();
                label_->setPixmap(QPixmap::fromImage(temp_image));
            }
        }

        return QObject::eventFilter(obj, event);
    }

private:
    QImage image_;
    QLabel* label_;
    QScrollArea* scroll_area_;

    QPoint center_;
    QPoint center_viewport_;
    int radius_ = 0;
    bool center_set_ = false;
};
