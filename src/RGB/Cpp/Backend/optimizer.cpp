#include "helper.h"
#include "optimizer.h"

#include <random>

Optimizer::Optimizer(){

    /// RANSAC properties
    ransac_probability_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "RANSACProb", "RANSAC Probability", 0.01, 0.9999999, 0.999, 5));
    ransac_threshold_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "RANSACThresh", "RANSAC Threshold", 0.01, 9999.9, 2.0));
    property_list_initial_ = {ransac_probability_, ransac_threshold_};
    /// LM properties
    LM_max_iter_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "LMMaxIter", "Max Iterations", 1, 9999999, 1000));
    LM_change_threshold_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "LMChangeThresh", "Change Threshold", 0.0, 9999.9, 0.0001, 5));
    property_list_LM_ = {LM_max_iter_, LM_change_threshold_};

    /// Evaluation properties
    eval_calc_variances_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "EvalCalcVar", "Calculate Variances", true));
    property_list_eval_ = {eval_calc_variances_};
}

void Optimizer::initialPoseEstimation(std::vector<Device>& devices,
                                      const std::shared_ptr<Detections>& detections,
                                      const bool& reinitialize){

    STATUS("Starting initial pose estimation...");
    std::vector<std::vector<Pose>> poses;

    // Determine pairwise poses
    for(size_t dev_idx = 0; dev_idx < devices.size() - 1; dev_idx++){
        poses.push_back({});
        for(size_t dev_idx_2 = dev_idx+1; dev_idx_2 < devices.size(); dev_idx_2++){

            // Check list of correspondences for the ones between the devices with
            // index dev_idx and dev_idx+1
            std::vector<cv::Point2d> points_1;
            std::vector<cv::Point2d> points_2;
            std::vector<size_t> correspondence_idx;
            for(size_t detection_idx = 0; detection_idx < detections->size(); detection_idx++){
                if((*detections)[detection_idx][dev_idx].valid_ && (*detections)[detection_idx][dev_idx_2].valid_){
                    points_1.push_back((*detections)[detection_idx][dev_idx].pixelCV());
                    points_2.push_back((*detections)[detection_idx][dev_idx_2].pixelCV());
                    correspondence_idx.push_back(detection_idx);
                }
            }

            if(points_1.size() < 5){
                // At least 5 correspondences are required by
                // Nister's algorithm to estimate E

                WARN("Not enough correspondences available to estimate the initial pose of device "
                     + QString::fromStdString(devices[dev_idx].device_name_)
                     + " relative to device "
                     + QString::fromStdString(devices[dev_idx_2].device_name_));
                continue;
            }

            // Estimate relative pose via essential matrix
            cv::Mat E, R, t, mask;
            std::vector<double> zero_dist = {0.0,0.0,0.0,0.0};
            cv::recoverPose(points_1, points_2, devices[dev_idx].rgb_.K_,
                            zero_dist, devices[dev_idx_2].rgb_.K_,
                            zero_dist, E, R, t, cv::RANSAC,
                            ransac_probability_->value(),
                            ransac_threshold_->value(), mask);

            // Mark inliers
            for(size_t i = 0; i < correspondence_idx.size(); i++){
                const size_t& idx = correspondence_idx[i];
                if(mask.at<uchar>(i,0) == 1){
                    (*detections)[idx][dev_idx].inlier_for_.push_back(dev_idx_2);
                    (*detections)[idx][dev_idx_2].inlier_for_.push_back(dev_idx);
                }
            }

            poses.back().push_back(Pose(R, t));
        }
    }

    // Triangulate points and scale relative translations
    Eigen::MatrixXd P_1, P_2, K_1, K_2;
    devices[1].world_pose_ = Pose(poses[0][0].R_, poses[0][0].t_ / poses[0][0].t_.norm());

    // Calculate projection matrices for devices
    cv::cv2eigen(devices[0].rgb_.K_, K_1);
    P_1 = K_1 * devices[0].world_pose_.m_.block(0,0,3,4);
    cv::cv2eigen(devices[1].rgb_.K_, K_2);
    P_2 = K_2 * devices[1].world_pose_.m_.block(0,0,3,4);

    for(size_t detection_idx = 0; detection_idx < detections->size(); detection_idx++){

        if((*detections)[detection_idx][0].valid_ && (*detections)[detection_idx][1].valid_){

            Eigen::Vector2d pixel_1 = (*detections)[detection_idx][0].pixelEig();
            Eigen::Vector2d pixel_2 = (*detections)[detection_idx][1].pixelEig();

            Eigen::Vector3d x = triangulate(pixel_1, pixel_2, P_1, P_2);
            (*detections)[detection_idx][0].p_ = x;
            (*detections)[detection_idx][1].p_ = x;
        }
    }

    STATUS("...done. Initial poses are: ");
    STATUS(QString::fromStdString(devices[1].device_name_));
    LOGMATRIX("R", devices[1].world_pose_.R_cv_);
    LOGMATRIX("t", devices[1].world_pose_.t_cv_);

    // Use these initial triangulations to scale translation vectors
    Eigen::MatrixXd K, KR;
    Eigen::Vector3d Kt, KRx;
    double pix_x, pix_y;
    std::vector<double> scales;
    for(size_t dev_idx = 2; dev_idx < devices.size(); dev_idx++){

        Pose rel_to_ref = poses[0][dev_idx-1];
        cv::cv2eigen(devices[dev_idx].rgb_.K_, K);
        KR = K * rel_to_ref.R_;
        Kt = K * rel_to_ref.t_;

        for(size_t ref_dev_idx = 0; ref_dev_idx < dev_idx; ref_dev_idx++){
            scales.clear();

            cv::cv2eigen(devices[ref_dev_idx].rgb_.K_, K_1);
            P_1 = K_1 * devices[ref_dev_idx].world_pose_.m_.block(0,0,3,4);

            for(size_t detection_idx = 0; detection_idx < detections->size(); detection_idx++){
                if((*detections)[detection_idx][ref_dev_idx].valid_ && (*detections)[detection_idx][dev_idx].valid_){
                    if(!((*detections)[detection_idx][ref_dev_idx].p_.z() < 0.0)){
                        Eigen::Vector3d x = (*detections)[detection_idx][ref_dev_idx].p_;
                        pix_x = (*detections)[detection_idx][dev_idx].x_;
                        pix_y = (*detections)[detection_idx][dev_idx].y_;
                        KRx = KR*x;

                        scales.push_back((pix_x * KRx.z() - KRx.x())/(Kt.x() - pix_x * Kt.z()));
                        scales.push_back((pix_y * KRx.z() - KRx.y())/(Kt.y() - pix_y * Kt.z()));
                    }
                }
            }

            if(scales.size() > 20){
                // Get Median
                size_t n = scales.size()/2;
                nth_element(scales.begin(), scales.begin()+n, scales.end());
                double scale = scales[n];

                devices[dev_idx].world_pose_ = devices[ref_dev_idx].world_pose_ * Pose(rel_to_ref.R_, scale * rel_to_ref.t_);

                STATUS(QString::fromStdString(devices[dev_idx].device_name_));
                LOGMATRIX("R", devices[dev_idx].world_pose_.R_cv_);
                LOGMATRIX("t", devices[dev_idx].world_pose_.t_cv_);

                // Triangulate further points for dev_idx
                cv::cv2eigen(devices[dev_idx].rgb_.K_, K_2);
                P_2 = K_2 * devices[dev_idx].world_pose_.m_.block(0,0,3,4);
                for(size_t dev_idx_2 = ref_dev_idx; dev_idx_2 < dev_idx; dev_idx_2++){

                    cv::cv2eigen(devices[dev_idx_2].rgb_.K_, K_1);
                    P_1 = K_1 * devices[dev_idx_2].world_pose_.m_.block(0,0,3,4);

                    for(size_t detection_idx = 0; detection_idx < detections->size(); detection_idx++){
                        if((*detections)[detection_idx][dev_idx_2].valid_ && (*detections)[detection_idx][dev_idx].valid_){
                            if((*detections)[detection_idx][dev_idx_2].p_.z() < 0.0){

                                Eigen::Vector2d pixel_1 = (*detections)[detection_idx][dev_idx_2].pixelEig();
                                Eigen::Vector2d pixel_2 = (*detections)[detection_idx][dev_idx].pixelEig();

                                Eigen::Vector3d x = triangulate(pixel_1, pixel_2, P_1, P_2);
                                (*detections)[detection_idx][dev_idx_2].p_ = x;
                                (*detections)[detection_idx][dev_idx].p_ = x;
                            }
                        }
                    }
                }
                break;
            }
        }

    }

    emit initialPosesCalculated();
}

void Optimizer::bundleAdjustment(std::vector<Device>& devices,
                                 const std::shared_ptr<Detections>& detections){
    STATUS("Starting bundle adjustment...");

    /// Prepare the data for the ceres solver

    // Create poses as double arrays
    std::vector<std::vector<double>> poses
            = std::vector<std::vector<double>>(devices.size(),
                                               std::vector<double>(6,0.0));

    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        Device& device = devices[dev_idx];
        ceres::RotationMatrixToAngleAxis<double>(
                    static_cast<double*> (device.world_pose_.R_.data()),
                    poses[dev_idx].data());
        poses[dev_idx][3] = device.world_pose_.t_[0];
        poses[dev_idx][4] = device.world_pose_.t_[1];
        poses[dev_idx][5] = device.world_pose_.t_[2];
    }

    // Create 3D world points as double arrays
    std::vector<std::vector<double>> scene_points
            = std::vector<std::vector<double>>(detections->size(),
                                               std::vector<double>(3,0.0));

    for(size_t point_idx = 0; point_idx < detections->size(); point_idx++){
        Eigen::Vector3d world_point(0.0,0.0,0.0);
        double num_valid = 0.0;
        for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
            if( (*detections)[point_idx][dev_idx].inlier_for_.size() > 0 ){
                world_point += devices[dev_idx].world_pose_.R_.transpose() *
                        ((*detections)[point_idx][dev_idx].p_
                         - devices[dev_idx].world_pose_.t_);
                num_valid++;
            }
        }
        if(num_valid > 0.0){
            world_point /= num_valid;
        }

        scene_points[point_idx] = {world_point[0], world_point[1], world_point[2]};
    }

    /// Create the problem :)

    ceres::Problem problem;
    // Configure solver
    ceres::Solver::Options options;
    options.trust_region_strategy_type = ceres::TrustRegionStrategyType::DOGLEG;
    options.use_nonmonotonic_steps = false;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = true;
    options.max_num_iterations = LM_max_iter_->value();
    options.num_threads = 16;
    options.function_tolerance = LM_change_threshold_->value();
    options.gradient_tolerance = 0.0;

    // Add residual blocks
    std::vector<int> residuals_per_device;
    std::vector<std::vector<int>> residuals_for_detection = std::vector<std::vector<int>>(detections->size(),std::vector<int>());
    std::vector<int> pose_block_idc(devices.size(), -1);
    std::vector<bool> pose_block_exists(devices.size(), false);
    std::vector<bool> point_block_exists(detections->size(), false);
    int block_idx_counter = 0;
    int residual_counter = 0;
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        residuals_per_device.push_back(0);
        for(size_t point_idx = 0; point_idx < detections->size(); point_idx++){
            if( (*detections)[point_idx][dev_idx].inlier_for_.size() > 0 ){
                ceres::CostFunction* cost_function;
                residuals_per_device.back()+=2;
                residuals_for_detection[point_idx].push_back(residual_counter);
                residuals_for_detection[point_idx].push_back(residual_counter+1);
                residual_counter+=2;
                if(dev_idx > 1){
                    cost_function = new ceres::AutoDiffCostFunction<Reprojection_Error, 2, 6, 3>(
                                new Reprojection_Error(devices[dev_idx],
                                                       Eigen::Vector2d((*detections)[point_idx][dev_idx].x_,
                                                                       (*detections)[point_idx][dev_idx].y_)));
                    problem.AddResidualBlock(cost_function, new ceres::HuberLoss(2.0),
                                             poses[dev_idx].data(),
                                             scene_points[point_idx].data());

                    if(!pose_block_exists[dev_idx]){
                        pose_block_exists[dev_idx] = true;
                        pose_block_idc[dev_idx] = block_idx_counter;
                        block_idx_counter += 6;
                    }
                    if(!point_block_exists[point_idx]){
                        point_block_exists[point_idx] = true;
                        block_idx_counter += 3;
                    }

                }
                else{
                    cost_function = new ceres::AutoDiffCostFunction<Reprojection_Error_Fixed_Pose, 2, 3>(
                                new Reprojection_Error_Fixed_Pose(devices[dev_idx],
                                                                  Eigen::Vector2d((*detections)[point_idx][dev_idx].x_,
                                                                                  (*detections)[point_idx][dev_idx].y_)
                                                                  ));
                    problem.AddResidualBlock(cost_function, new ceres::HuberLoss(2.0),
                                             scene_points[point_idx].data());

                    if(!point_block_exists[point_idx]){
                        point_block_exists[point_idx] = true;
                        block_idx_counter += 3;
                    }
                }

            }
        }
    }

    /// Optimization

    // Run the solver
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    STATUS(QString::fromStdString(summary.FullReport()));

    double error_per_residual = summary.final_cost / double(summary.num_residuals_reduced);
    STATUS("Error per residual: " + QString::number(error_per_residual));

    // Get the residuals
    std::vector<double> residuals = std::vector<double>(summary.num_residuals, 0.0);
    ceres::Problem::EvaluateOptions options_eval;
    options_eval.num_threads = 1;
    options_eval.apply_loss_function = true;
    problem.Evaluate(options_eval, nullptr, &residuals, nullptr, nullptr);

    // Check error per device and per detection
    std::vector<double> error_per_devs;
    size_t counter = 0;
    size_t last_end = 0;
    for(const int& num_residuals : residuals_per_device){
        double error_per_dev = 0.0;
        for(size_t i = last_end; i < last_end+num_residuals; i++){
            error_per_dev += residuals[i]*residuals[i] / double(num_residuals);
            counter++;
        }
        last_end = counter;
        error_per_devs.push_back(error_per_dev);
    }

    std::vector<double> error_per_detection;
    for(const std::vector<int>& res_for_detect : residuals_for_detection){
        double error_per_detect = 0.0;

        for(const int& idx : res_for_detect){
            error_per_detect += residuals[idx]*residuals[idx] / double(res_for_detect.size());
        }
        error_per_detection.push_back(error_per_detect);
    }

    // Calculate covariance matrices for poses if required
    ceres::Covariance::Options cov_options;
    ceres::Covariance covariance(cov_options);
    if(eval_calc_variances_->value()){
        STATUS("Calculating variances...");
        std::vector<std::pair<const double*, const double*>> covariance_blocks;
        for(int dev_idx = 2; dev_idx < devices.size(); dev_idx++){
            covariance_blocks.push_back(std::make_pair(poses[dev_idx].data(),
                                                       poses[dev_idx].data()));
        }
        CHECK(covariance.Compute(covariance_blocks, &problem));
    }
    std::vector<Eigen::Vector3d> rotations_euler(devices.size());
    std::vector<Eigen::Vector3d> var_euler_rotations(devices.size());
    std::vector<Eigen::Vector3d> translations(devices.size());
    std::vector<Eigen::Vector3d> var_translations(devices.size());

    for(int dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        Eigen::Vector3d r_rod(poses[dev_idx][0],poses[dev_idx][1],
                poses[dev_idx][2]);
        translations[dev_idx] = Eigen::Vector3d(poses[dev_idx][3],
                poses[dev_idx][4],poses[dev_idx][5]);
        rotations_euler[dev_idx] = Helper::rotationMatrixToEulerAnglesDegree(
                    Helper::rodriguesToRotationMatrix(r_rod));

        if(eval_calc_variances_->value()){
            if(dev_idx > 1){
//                ceres::Covariance::Options cov_options;
//                ceres::Covariance covariance(cov_options);
//                std::vector<std::pair<const double*, const double*>> covariance_blocks;
//                covariance_blocks.push_back(std::make_pair(poses[dev_idx].data(),
//                                                           poses[dev_idx].data()));
//                CHECK(covariance.Compute(covariance_blocks, &problem));
                Eigen::MatrixXd cov(6, 6);
                covariance.GetCovarianceBlock(poses[dev_idx].data(),
                                              poses[dev_idx].data(), cov.data());
                var_translations[dev_idx] = Eigen::Vector3d(std::sqrt(cov(3,3)),
                                                            std::sqrt(cov(4,4)),
                                                            std::sqrt(cov(5,5)));

                // Transfer Rodrigues vector and variances to euler angles and variances
                Eigen::Matrix3d cov_r = cov.block(0,0,3,3);
                Eigen::Matrix3d cov_euler = Helper::transformCovariance(cov_r, r_rod);
                var_euler_rotations[dev_idx] = Eigen::Vector3d(
                            std::sqrt(cov_euler(0,0)),
                            std::sqrt(cov_euler(1,1)),
                            std::sqrt(cov_euler(2,2)));
            }
            else{
                var_translations[dev_idx] = Eigen::Vector3d(0,0,0);
                var_euler_rotations[dev_idx] = Eigen::Vector3d(0,0,0);
            }
        }
    }

    if(eval_calc_variances_->value()){
        STATUS("...done.");
    }


    // Update poses
    for(size_t dev_idx = 2; dev_idx < devices.size(); dev_idx++){
        Device& device = devices[dev_idx];
        Eigen::Matrix3d rotation;
        ceres::AngleAxisToRotationMatrix<double>(poses[dev_idx].data(),
                                                 static_cast<double*>(rotation.data()));
        Eigen::Vector3d translation;
        translation[0] = poses[dev_idx][3];
        translation[1] = poses[dev_idx][4];
        translation[2] = poses[dev_idx][5];
        device.world_pose_ = Pose(rotation, translation);
    }

    // Update 3D points
    for(size_t point_idx = 0; point_idx < detections->size(); point_idx++){
        for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
            if( (*detections)[point_idx][dev_idx].valid_){
                (*detections)[point_idx][dev_idx].p_ = Eigen::Vector3d(
                            scene_points[point_idx][0],
                        scene_points[point_idx][1],
                        scene_points[point_idx][2]);
            }
        }
    }

    // Tell the logger about the results
    STATUS("...done. The final poses are");
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        STATUS(QString::fromStdString(devices[dev_idx].device_name_));

        if(eval_calc_variances_->value()){
            LOGVECVAR("R_euler", rotations_euler[dev_idx], var_euler_rotations[dev_idx]);
            LOGVECVAR("t", translations[dev_idx], var_translations[dev_idx]);
        }
        else{
            LOGMATRIX("R", devices[dev_idx].world_pose_.R_cv_);
            LOGMATRIX("t", devices[dev_idx].world_pose_.t_cv_);
        }
    }

    emit done(scene_points);
}

Eigen::Vector3d Optimizer::triangulate(const Eigen::Vector2d& pixel_1,
                                       const Eigen::Vector2d& pixel_2,
                                       const Eigen::MatrixXd& P_1,
                                       const Eigen::MatrixXd& P_2){
    // Build matrix A for DLT
    Eigen::Matrix4d A;
    A.block(0,0,1,4) = pixel_1(0) * P_1.block(2,0,1,4) - P_1.block(0,0,1,4);
    A.block(1,0,1,4) = pixel_1(1) * P_1.block(2,0,1,4) - P_1.block(1,0,1,4);
    A.block(2,0,1,4) = pixel_2(0) * P_2.block(2,0,1,4) - P_2.block(0,0,1,4);
    A.block(3,0,1,4) = pixel_2(1) * P_2.block(2,0,1,4) - P_2.block(1,0,1,4);

    // Calculate SVD
    Eigen::JacobiSVD<Eigen::Matrix4d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Eigen::MatrixXd V = svd.matrixV();

    // Extract triangulated point from V
    Eigen::Vector3d x = Eigen::Vector3d(V.block(0, 3, 3, 1)) / V(3,3);
    return x;
}
