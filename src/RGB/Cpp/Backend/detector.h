////////////////////////////////////////////////////////////////////////////////
///            Detector for the table tennis ball in RGB-D images            ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Backend/data_types.h"
#include "Backend/imagelist_generator.h"
#include <QObject>

///
/// \brief The Detector class
///
class Detector : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Detector         is the default constructor
    /// \param imagelist_gen    pointer to an instance of the imagelist generator
    ///
    Detector(std::shared_ptr<Imagelist_Generator> imagelist_gen);

    ///
    /// \brief getProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties(){return property_list_;}

public slots:

    ///
    /// \brief detectCorrespondences    coordinates the whole correspondence
    ///                                 detection process, i.e. creation of
    ///                                 image file lists and RGB ball detection
    /// \param image_dir                directory containing the RGB images
    /// \param devices                  list of devices
    /// \param rgb_scheme               rgb image naming scheme
    ///
    void detectCorrespondences(const std::string& image_dir,
                               std::vector<Device>& devices,
                               const QString& rgb_scheme,
                               const bool &only_prepare_data);

private:

    ///
    /// \brief detectBallRGB   tries to find the pixel position and radius of an
    ///                        illuminated table tennis ball in an RGB image
    /// \param detection       resulting pixel coordinates and radius
    /// \param visualize_detection set to true to enable debug visualization
    /// \return                true if detected, false otherwise
    ///
    bool detectBallRGB(Detection* detection, const bool &visualize_detection);

private:

    std::shared_ptr<Property_Check> auto_thresholds_ = nullptr;
    std::shared_ptr<Property_Selector> visualization_ = nullptr;

    /// Thresholds for the hue, saturation and value channels
    std::shared_ptr<Property_Range> H_ = nullptr;
    std::shared_ptr<Property_Range> S_ = nullptr;
    std::shared_ptr<Property_Range> V_ = nullptr;

    std::vector<std::shared_ptr<Property>> property_list_;

    /// pointer to an instance of the imagelist generator
    std::shared_ptr<Imagelist_Generator> imagelist_gen_;

signals:

    ///
    /// \brief ballsDetected    is emitted after successfully finishing the ball
    ///                         detection
    /// \param detectedBalls    list of detections
    ///
    void ballsDetected(const std::shared_ptr<Detections>& detectedBalls);

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief progress
    /// \param val
    ///
    void progress(const float& val);

};
