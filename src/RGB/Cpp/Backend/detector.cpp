#include "detector.h"
#include "UI/image_annotation.h"
#include <math.h>
#include <QEventLoop>
#include <random>

Detector::Detector(std::shared_ptr<Imagelist_Generator> imagelist_gen){
    imagelist_gen_ = imagelist_gen;

    auto_thresholds_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "AutoThresholds", "Automatically Determine Thresholds", true));
    visualization_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "VisualizationMode", "Visualization:",{"None", "Initial Detection", "Undistorted Detection", "All"}, 0));

    /// Thresholds for the hue, saturation and value channels
    H_ = std::shared_ptr<Property_Range>(new Property_Range(nullptr, "H_Thresholds", "", "", " < H < ", -1, 180, 20, -1, 180, 60));
    S_ = std::shared_ptr<Property_Range>(new Property_Range(nullptr, "S_Thresholds", "", "", " < S < ", -1, 255, 10, -1, 255, 130));
    V_ = std::shared_ptr<Property_Range>(new Property_Range(nullptr, "V_Thresholds", "", "", " < V < ", -1, 255, 128, -1, 255, -1));

    property_list_ = {auto_thresholds_, H_, S_, V_, visualization_};
}

void Detector::detectCorrespondences(const std::string& image_dir,
                                     std::vector<Device>& devices,
                                     const QString& rgb_scheme,
                                     const bool& only_prepare_data = false){

    // Create undistortion maps
    STATUS("Calculating undistortion maps...");
    for(Device& dev : devices){
        cv::initUndistortRectifyMap(dev.rgb_.K_, dev.rgb_.dist_, cv::noArray(),
                                    dev.rgb_.K_,
                                    cv::Size(dev.rgb_.res_x_,dev.rgb_.res_y_),
                                    CV_32FC1, dev.rgb_.map_x_, dev.rgb_.map_y_);
    }
    STATUS("...done.");

    // Create a list of the available images
    Imagelist img_list = imagelist_gen_->createImagelist(image_dir,
                                                         rgb_scheme);

    // Scan the file descriptor in the image list to create lists of available
    // cameras and image IDs
    std::vector<std::string> IDs;

    for(std::vector<RGBImageDescription>& descriptors : img_list){
        IDs.push_back(descriptors[0].ID_);
        for(RGBImageDescription& descriptor : descriptors){
            std::string camera = descriptor.camera_;

            // search for camera name in devices
            for(size_t cam_idx = 0; cam_idx < devices.size(); cam_idx++){
                if(camera.compare(devices[cam_idx].device_name_) == 0){
                    descriptor.cam_idx_ = cam_idx;
                }
            }
            if(descriptor.cam_idx_ == -1){
                ERROR("Camera name " + QString::fromStdString(camera)
                      + " has no match in device list from intrinsics file.");
                return;
            }
        }
    }

    if(only_prepare_data){
        return;
    }

    // Ask user to click a few balls to determine the HSV thresholds
    if(auto_thresholds_->value()){
        H_->setValues(255, 0);
        S_->setValues(255, 0);
        V_->setValues(255, 0);

        // Show random images to the user until 5 balls are marked
        int num_annotated = 0;

        // Create a random number generator engine
        std::random_device rd;
        std::mt19937 gen(rd());

        // Create a uniform distribution over the range
        std::uniform_int_distribution<int> distribution_img(0, img_list.size()-1);
        std::uniform_int_distribution<int> distribution_cam(0, devices.size()-1);

        while(num_annotated < 5){

            // Randomly sample idx and camera
            size_t img_idx = distribution_img(gen);
            size_t cam_idx = std::min(distribution_cam(gen), int(img_list[img_idx].size()-1));

            Image_Annotation annotator(QString::fromStdString(img_list[img_idx][cam_idx].path_rgb_));
            annotator.show();

            QEventLoop loop;
            connect(&annotator, &Image_Annotation::finished, &loop, &QEventLoop::quit);
            loop.exec();

            QPoint center = annotator.getCenter();
            int radius = annotator.getRadius();
            if (center.x() != -1 && center.y() != -1 && radius > 0){
                num_annotated++;

                // Calculate HSV histograms in order to find suitable thresholds
                cv::Mat image = cv::imread(img_list[img_idx][cam_idx].path_rgb_);
                cv::Mat hsv_image;
                cv::cvtColor(image, hsv_image, cv::COLOR_BGR2HSV);

                // Get the ball region
                cv::Rect roi(center.x() - radius, center.y() - radius, radius * 2, radius * 2);
                cv::Mat region = hsv_image(roi);

                // Split the region into individual channels: H, S, V
                cv::Mat channels[3];
                cv::split(region, channels);

                // Create a binary mask based on the condition (V > 180)
                cv::Mat mask;
                cv::inRange(channels[2], 0, 128, mask);
                region.setTo(cv::Scalar(0, 0, 0), mask);
                cv::split(region, channels);

                // Calculate histograms for each channel
                int hist_size = 32;  // Number of bins
                int hist_size_V = 8;  // Number of bins
                float rangeH[] = {0, 180};  // Range of pixel values
                float range[] = {0, 255};  // Range of pixel values
                const float* hist_range_H = {rangeH};
                const float* hist_range = {range};
                bool uniform = true, accumulate = false;
                cv::Mat hist[3];
                cv::calcHist(&channels[0], 1, 0, cv::Mat(), hist[0], 1, &hist_size, &hist_range_H, uniform, accumulate);
                cv::calcHist(&channels[1], 1, 0, cv::Mat(), hist[1], 1, &hist_size, &hist_range, uniform, accumulate);
                cv::calcHist(&channels[2], 1, 0, cv::Mat(), hist[2], 1, &hist_size_V, &hist_range, uniform, accumulate);

                hist[0].at<float>(0,0) = std::min(hist[0].at<float>(1,0),
                        hist[0].at<float>(hist[0].rows-1,0));
                hist[1].at<float>(0,0) = 0.0f;
                hist[2].at<float>(0,0) = 0.0f;

                float threshold = 2;
                for (int i = 0; i < hist_size; i++) {
                    if(hist[0].at<float>(i,0) < threshold){
                        hist[0].at<float>(i,0) = 0.0f;
                    }
                    if(hist[1].at<float>(i,0) < threshold){
                        hist[1].at<float>(i,0) = 0.0f;
                    }
                }
                for (int i = 0; i < hist_size_V; i++) {
                    if(hist[2].at<float>(i,0) < threshold){
                        hist[2].at<float>(i,0) = 0.0f;
                    }
                }

                // Extract intervals
                auto firstNonZero = [](const cv::Mat& hist){
                    for(int i = 0; i < hist.rows; i++){
                        if(hist.at<float>(i,0) > 0.0){
                            return i;
                        }
                    }
                    return -1;
                };

                auto lastNonZero = [](const cv::Mat& hist){
                    for(int i = hist.rows-1; i > 1; i--){
                        if(hist.at<float>(i,0) > 0.0){
                            return i;
                        }
                    }
                    return -1;
                };

                auto firstZero = [](const cv::Mat& hist){
                    for(int i = 0; i < hist.rows; i++){
                        if(hist.at<float>(i,0) < 0.01){
                            return i;
                        }
                    }
                    return -1;
                };

                auto lastZero = [](const cv::Mat& hist){
                    for(int i = hist.rows-1; i > 1; i--){
                        if(hist.at<float>(i,0) < 0.01){
                            return i;
                        }
                    }
                    return -1;
                };

                for(int i = 0; i < 3; i++){
                    int low = firstNonZero(hist[i]);
                    int high = lastNonZero(hist[i]) + 1;

                    if(i == 0){
                        if(high == 0){
                            low = lastZero(hist[i]) + 1;
                            high = firstZero(hist[i]);
                        }
                        H_->setLower(std::min(H_->lower(), int(double(low)*255.0/double(hist_size))));
                        H_->setUpper(std::max(H_->upper(), int(double(high)*255.0/double(hist_size))));
                    }
                    else if(i==1){
                        S_->setLower(std::min(S_->lower(), int(double(low)*255.0/double(hist_size))));
                        S_->setUpper(std::max(S_->upper(), int(double(high)*255.0/double(hist_size))));
                    }
                    else{
                        V_->setLower(std::min(V_->lower(), int(double(low)*255.0/double(hist_size_V))));
                        V_->setUpper(std::max(V_->upper(), int(double(high)*255.0/double(hist_size_V))));
                    }
                }
            }
        }

        if(H_->lower() < 1){
            H_->setLower(-1);
        }
        if(H_->upper() > 179){
            H_->setUpper(-1);
        }
        if(S_->lower() < 1){
            S_->setLower(-1);
        }
        if(S_->upper() > 254){
            S_->setUpper(-1);
        }
        if(V_->lower() < 1){
            V_->setLower(-1);
        }
        if(V_->upper() > 254){
            V_->setUpper(-1);
        }

        STATUS("HSV thresholds calculated.");
    }

    // detect light ball in rgb images
    STATUS("Detecting ball in RGB images...");
    std::shared_ptr<Detections> detections = std::shared_ptr<Detections>(
                new Detections(IDs.size(),
                               std::vector<Detection>(devices.size(),
                                                      Detection())));
    int found = 0;
    int imgs = 0;
    for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){

        if(visualization_->index() == VIS_NONE || visualization_->index() == VIS_UNDISTORTED_DETECTIONS){

            cv::parallel_for_(cv::Range(0, img_list[ID_idx].size()), [&](const cv::Range& range){
                for (int i = range.start; i < range.end; ++i) {
                    const RGBImageDescription& descriptor = img_list[ID_idx][i];
                    (*detections)[ID_idx][descriptor.cam_idx_].rgb_image_file_ =
                            descriptor.path_rgb_;
                    if(detectBallRGB(&((*detections)[ID_idx][descriptor.cam_idx_]),
                            (visualization_->index() == VIS_ALL
                             || visualization_->index() == VIS_RGB_DETECTIONS))){
                        found++;
                    }
                    imgs++;
                }
            });
        }
        else{
            // Slower sequential detection in order to prevent multithreading
            // interfering with the visualization
            for (const RGBImageDescription& descriptor : img_list[ID_idx]) {
                (*detections)[ID_idx][descriptor.cam_idx_].rgb_image_file_ =
                        descriptor.path_rgb_;
                if(detectBallRGB(&((*detections)[ID_idx][descriptor.cam_idx_]),
                        (visualization_->index() == VIS_ALL
                         || visualization_->index() == VIS_RGB_DETECTIONS))){
                    found++;
                }
                imgs++;
            }

        }
        emit progress(float(ID_idx+1)/float(IDs.size()));
    }
    emit progress(1.0);
    if(visualization_->index() > 0){
        cv::destroyAllWindows();
    }
    STATUS("...done. Found "+QString::number(found)+ " targets in "+QString::number(imgs)+" images.");

    // undistort detections
    STATUS("Undistort detections...");
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        std::vector<cv::Point2f> distorted_points;
        for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){
            distorted_points.push_back(
                        cv::Point2f((*detections)[ID_idx][dev_idx].x_,
                                    (*detections)[ID_idx][dev_idx].y_));
        }
        std::vector<cv::Point2f> undistorted_points;
        cv::undistortPoints(distorted_points, undistorted_points,
                            devices[dev_idx].rgb_.K_, devices[dev_idx].rgb_.dist_,
                            cv::noArray(), devices[dev_idx].rgb_.K_);

        for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){
            if((*detections)[ID_idx][dev_idx].valid_){
                (*detections)[ID_idx][dev_idx].x_ = undistorted_points[ID_idx].x;
                (*detections)[ID_idx][dev_idx].y_ = undistorted_points[ID_idx].y;

                if(visualization_->index() == VIS_ALL
                        || visualization_->index() == VIS_UNDISTORTED_DETECTIONS){

                    cv::Mat image = cv::imread(
                                (*detections)[ID_idx][dev_idx].rgb_image_file_);
                    cv::remap(image, image, devices[dev_idx].rgb_.map_x_,
                              devices[dev_idx].rgb_.map_y_, cv::INTER_LINEAR);
                    cv::circle(image,
                               cv::Point(int((*detections)[ID_idx][dev_idx].x_),
                                         int((*detections)[ID_idx][dev_idx].y_)),
                               int((*detections)[ID_idx][dev_idx].r_),
                               cv::Scalar(0,255,0), 2);
                    cv::imshow("Undistorted detection", image);
                    cv::waitKey(0);
                }

            }
        }
    }
    if(visualization_->index() > 0){
        cv::destroyAllWindows();
    }
    STATUS("...done.");


    emit ballsDetected(detections);
}

bool Detector::detectBallRGB(Detection* detection, const bool& visualize_detection){

    detection->valid_ = false;

    cv::Mat image = cv::imread(detection->rgb_image_file_);

    cv::Mat image_hsv;
    // Convert to HSV color space
    cv::cvtColor(image, image_hsv, cv::COLOR_BGR2HSV);
    // Separate channels
    cv::Mat channel_images[3];
    cv::split(image_hsv, channel_images);
    // Threshold channels
    std::vector<cv::Mat> masks;
    // Threshold H
    if(H_->lower() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[0], masks.back(), double(H_->lower()), 1.0, cv::THRESH_BINARY);
    }
    if(H_->upper() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[0], masks.back(), double(H_->upper()), 1.0, cv::THRESH_BINARY_INV);
    }
    // Threshold S
    if(S_->lower() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[1], masks.back(), double(S_->lower()), 1.0, cv::THRESH_BINARY);
    }
    if(S_->upper() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[1], masks.back(), double(S_->upper()), 1.0, cv::THRESH_BINARY_INV);
    }
    // Threshold V
    if(V_->lower() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[2], masks.back(), double(V_->lower()), 1.0, cv::THRESH_BINARY);
    }
    if(V_->upper() > -1){
        masks.push_back(cv::Mat());
        cv::threshold(channel_images[2], masks.back(), double(V_->upper()), 1.0, cv::THRESH_BINARY_INV);
    }

    // Combine masks
    cv::Mat mask_combined = masks[0];
    for(const cv::Mat& mask : masks){
        mask_combined &= mask;
    }

    // Apply dilation filter
    cv::Mat element = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(3,3),
                                                cv::Point(1,1));

    cv::dilate(mask_combined, mask_combined, element);
    cv::dilate(mask_combined, mask_combined, element);
    cv::erode(mask_combined, mask_combined, element);

    // Find connected components and their stats
    cv::Mat labels;
    cv::Mat stats;
    cv::Mat centroids;
    int num_components = cv::connectedComponentsWithStats(mask_combined, labels, stats, centroids);

    // Filter components according to radius and roundness
    float width, height, area, squareness, fill_factor, score;
    float target_fill_factor = M_PI/4.0f;
    float best_score = 0.3f;
    int best_component = -1;
    for(int comp_idx = 0; comp_idx < num_components; comp_idx++){

        // Get component stats
        width = float(stats.at<uint32_t>(comp_idx, 2));
        height = float(stats.at<uint32_t>(comp_idx, 3));
        area = float(stats.at<uint32_t>(comp_idx, 4));

        // Filter out components that are too large
        if((width > float(image.rows)/4.0f) || (height > float(image.rows)/4.0f)){
            continue;
        }

        // Calculate properties and score
        squareness = std::min(width,height)/std::max(width,height);
        fill_factor = area / (width * height);
        score = squareness * (1.0f - 2.0f * std::abs(target_fill_factor - fill_factor));

        // Check if best score
        if(score > best_score){
            best_component = comp_idx;
            best_score = score;
        }
    }

    // If a best component has been found, save its properties
    if(best_component != -1){
        detection->valid_ = true;
        detection->x_ = centroids.at<double>(best_component, 0);
        detection->y_ = centroids.at<double>(best_component, 1);
        detection->r_ = std::min(detection->x_ - double(stats.at<uint32_t>(best_component, 0)),
                                 detection->y_ - double(stats.at<uint32_t>(best_component, 1)));
        // Show the detection
        if(visualize_detection){
            cv::circle(image, cv::Point(int(detection->x_), int(detection->y_)),
                       int(detection->r_), cv::Scalar(0,255,0), 2);
            cv::imshow("RGB Image Detection", image);
            cv::waitKey(0);
        }

        return true;
    }
    else{
        // Otherwise reject the image
        if(visualize_detection){
            // For additional debug output, uncomment the following line and
            // check the masks e.g. with GIMP
            //  cv::imwrite("Rejected.png", image);
            //  cv::imwrite("Mask.png", mask);
            //  cv::imwrite("MaskH.png", (masks[0] | masks[1]));
            //  cv::imwrite("MaskS.png", (masks[2] & masks[3]));
            //  cv::imwrite("MaskV.png", masks[4]);

            // Show the rejected image
            cv::imshow("Rejected - No ball detected!", image);
            cv::waitKey(0);
        }
        // Remove image dir path from image_file for clearer logging
        //QString file_name = QString::fromStdString(detection->rgb_image_file_);
        //file_name = file_name.mid(file_name.lastIndexOf('/')+1);
        //WARN("Ball could not be detected in image "+file_name);
        return false;
    }
}
