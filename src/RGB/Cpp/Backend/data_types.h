////////////////////////////////////////////////////////////////////////////////
///     This header describes simple structs, constants, and type aliases    ///
///                      used throughout different classes                   ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <eigen3/Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <QCheckBox>
#include <QComboBox>
#include <QDesktopServices>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QWidget>

/// constants used to enable different debug visualizations
const int VIS_NONE = 0;
const int VIS_RGB_DETECTIONS = 1;
const int VIS_UNDISTORTED_DETECTIONS = 2;
const int VIS_ALL = 3;

/// Calibration model flags
const int CALIB_BROWN = 0;      /// Classical Brown distortion model
const int CALIB_RATIONAL = 1;   /// Brown + rational radial distortion


////////////////////////////// Property Widgets ////////////////////////////////

/// Property type flags
const int PROP_SLIDER_INT = 0;
const int PROP_SPIN_INT = 1;
const int PROP_SPIN_DOUBLE = 2;
const int PROP_SELECTOR = 3;
const int PROP_CHECK = 4;
const int PROP_BUTTON = 5;
const int PROP_LINEEDIT = 6;
const int PROP_FILESELECT = 7;
const int PROP_DIRSELECT = 8;
const int PROP_RANGE = 9;

class Property : public QWidget{

    Q_OBJECT

public:

    Property(QWidget* parent, const QString& name, const int& property_type) :
        QWidget(parent){

        name_ = name;
        property_type_ = property_type;
    }

    ///
    /// \brief name
    /// \return
    ///
    QString name(){return name_;}

    ///
    /// \brief type
    /// \return
    ///
    int type(){return property_type_;}

    ///
    /// \brief setValue
    /// \param val
    ///
    virtual void setValue(const QVariant& val) = 0;

private:
    QString name_;
    int property_type_;
};

class Property_Slider : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Slider
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Slider(QWidget *parent, const QString& property_name,
                    const QString& label, const int& min, const int& max,
                    const int& current) :
        Property(parent, property_name, PROP_SLIDER_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        slider_  = new QSlider(Qt::Orientation::Horizontal);
        slider_->setMinimum(min);
        slider_->setMaximum(max);
        slider_->setValue(current);
        layout_->addWidget(slider_);
        display_  = new QLabel(QString::number(slider_->value()));
        layout_->addWidget(display_);

        this->setLayout(layout_);

        connect(slider_, &QSlider::valueChanged, [this](int value){display_->setText(QString::number(value));});
        connect(slider_, &QSlider::valueChanged, this, &Property_Slider::valueChanged);
    }

    ///
    /// \brief Property_Slider
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Slider(QWidget *parent, const QString& property_name,
                    const QString& label, const int& min, const int& max,
                    const int& current, const QString& button_label,
                    const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SLIDER_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        slider_  = new QSlider(Qt::Orientation::Horizontal);
        slider_->setMinimum(min);
        slider_->setMaximum(max);
        slider_->setValue(current);
        layout_->addWidget(slider_);
        display_  = new QLabel(QString::number(slider_->value()));
        layout_->addWidget(display_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](int value){slider_->setValue(default_value_);});
        }
        connect(slider_, &QSlider::valueChanged, [this](int value){display_->setText(QString::number(value));});
        connect(slider_, &QSlider::valueChanged, this, &Property_Slider::valueChanged);
    }

    ///
    /// \brief value
    /// \return
    ///
    int value(){
        if(slider_){
            return slider_->value();
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        slider_->setValue(val.toInt());
    }

private:
    QSlider* slider_;
    QLabel* display_;
    QPushButton* button_;

    int default_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Spin : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Spin
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Spin(QWidget *parent, const QString& property_name,
                  const QString& label, const int& min, const int& max,
                  const int& current) :
        Property(parent, property_name, PROP_SPIN_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        layout_->addWidget(spin_);

        this->setLayout(layout_);

        connect(spin_, qOverload<int>(&QSpinBox::valueChanged), this, &Property_Spin::valueChanged);
    }

    ///
    /// \brief Property_Spin
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Spin(QWidget *parent, const QString& property_name,
                  const QString& label, const int& min, const int& max,
                  const int& current, const QString& button_label,
                  const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SPIN_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        layout_->addWidget(spin_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](){spin_->setValue(default_value_);});
        }

        connect(spin_, qOverload<int>(&QSpinBox::valueChanged), this, &Property_Spin::valueChanged);
    }

    ///
    /// \brief value
    /// \return
    ///
    int value(){
        if(spin_){
            try{
                return spin_->value();
            }
            catch(...){
            }
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        spin_->setValue(val.toInt());
    }

private:
    QSpinBox* spin_;
    QPushButton* button_;
    int default_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Spin_F : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Spin_F
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Spin_F(QWidget *parent, const QString& property_name,
                    const QString& label, const float& min, const float& max,
                    const float& current, const int& decimal_places = 2) :
        Property(parent, property_name, PROP_SPIN_DOUBLE){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QDoubleSpinBox();
        spin_->setDecimals(decimal_places);
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        spin_->setSingleStep(std::min(1.0, double(max-min)/100.0));
        layout_->addWidget(spin_);

        this->setLayout(layout_);

        connect(spin_, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Property_Spin_F::valueChanged);
        connect(spin_, &QDoubleSpinBox::editingFinished, [this](){emit valueEditFinished(spin_->value());});
    }

    ///
    /// \brief Property_Spin_F
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Spin_F(QWidget *parent, const QString& property_name,
                    const QString& label, const double& min, const double& max,
                    const double& current, const QString& button_label,
                    const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SPIN_DOUBLE){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QDoubleSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        spin_->setSingleStep(std::min(1.0, double(max-min)/100.0));
        layout_->addWidget(spin_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](){spin_->setValue(default_value_);});
        }

        connect(spin_, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Property_Spin_F::valueChanged);
        connect(spin_, &QDoubleSpinBox::editingFinished, [this](){
            double value = spin_->value();
            if(value != last_value_){
                last_value_ = value;
                emit valueEditFinished(value);
            }
        });
    }

    ///
    /// \brief value
    /// \return
    ///
    double value(){
        if(spin_){
            try{
                return spin_->value();
            }
            catch(...){
            }
        }
        return NAN;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        spin_->setValue(val.toDouble());
    }

private:
    QDoubleSpinBox* spin_;
    QPushButton* button_;
    double default_value_;
    double last_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const double& value);

    ///
    /// \brief valueEditFinished
    /// \param value
    ///
    void valueEditFinished(const double& value);

};

class Property_Selector : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Selector
    /// \param parent
    /// \param label
    /// \param list
    ///
    Property_Selector(QWidget *parent, const QString& property_name,
                      const QString& label, const QStringList& list,
                      const int& selected) :
        Property(parent, property_name, PROP_SELECTOR){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        selector_  = new QComboBox();
        selector_->addItems(list);
        selector_->setCurrentIndex(selected);
        layout_->addWidget(selector_);

        this->setLayout(layout_);

        connect(selector_, qOverload<int>(&QComboBox::currentIndexChanged), this, &Property_Selector::valueChanged);
    }

    ///
    /// \brief index
    /// \return
    ///
    int index(){
        if(selector_){
            return selector_->currentIndex();
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        selector_->setCurrentIndex(val.toInt());
    }

    ///
    /// \brief text
    /// \return
    ///
    QString text(){return selector_->currentText();}

private:
    QComboBox* selector_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Check : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Check
    /// \param parent
    /// \param label
    /// \param checked
    /// \param as_toggle_button
    /// \param alternative_label
    ///
    Property_Check(QWidget *parent, const QString& property_name,
                   const QString& label, const bool& checked,
                   const bool& as_toggle_button = false,
                   const QString& alternative_label = "",
                   const bool& label_left = true,
                   const bool& separate_label = true) :
        Property(parent, property_name, PROP_CHECK){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        checkbox_  = new QCheckBox();
        checkbox_->setChecked(checked);

        if(as_toggle_button){
            button_ = new QPushButton(label);
            layout_->addWidget(button_);
            alternative_label_text_ = (alternative_label.size() == 0) ? label : alternative_label;
            label_text_ = label;
        }
        else{

            if(separate_label){
                QLabel* label_  = new QLabel(label);
                if(label_left){
                    layout_->addWidget(label_);
                    layout_->addWidget(checkbox_);
                }
                else{
                    layout_->addWidget(checkbox_);
                    layout_->addWidget(label_);
                }
                layout_->addItem(new QSpacerItem(0,0,QSizePolicy::Expanding));
            }
            else{
                layout_->addWidget(checkbox_);
                checkbox_->setText(label);
                if(label_left){
                    checkbox_->setLayoutDirection(Qt::RightToLeft);
                }
            }
        }

        this->setLayout(layout_);

        default_value_ = checked;

        if(as_toggle_button){
            connect(button_, &QPushButton::clicked, [this](){
                bool checked = !checkbox_->isChecked();
                if(checked == default_value_){
                    button_->setText(label_text_);
                }
                else{
                    button_->setText(alternative_label_text_);
                }

                checkbox_->setChecked(checked);
                emit valueChanged(checked);
            });
        }
        else{
            connect(checkbox_, &QCheckBox::clicked, this, &Property_Check::valueChanged);
        }
    }

    ///
    /// \brief value
    /// \return
    ///
    bool value(){
        return checkbox_->isChecked();
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        checkbox_->setChecked(val.toBool());
    }

private:
    QCheckBox* checkbox_;
    QPushButton* button_;
    bool default_value_;
    QString label_text_;
    QString alternative_label_text_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const bool& value);

};

class Property_Button: public Property{
    Q_OBJECT

public:
    ///
    /// \brief Property_Check
    /// \param parent
    /// \param label
    /// \param checked
    /// \param as_toggle_button
    /// \param alternative_label
    ///
    Property_Button(QWidget *parent, const QString& property_name,
                    const QString& label) :
        Property(parent, property_name, PROP_BUTTON){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        button_ = new QPushButton(label);
        layout_->addWidget(button_);
        this->setLayout(layout_);

        connect(button_, &QPushButton::clicked, this, &Property_Button::valueChanged);
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        button_->setChecked(val.toBool());
    }


private:
    QPushButton* button_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const bool& checked);

};

class Property_LineEdit: public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_LineEdit
    /// \param parent
    /// \param property_name
    /// \param label
    /// \param label_left
    ///
    Property_LineEdit(QWidget *parent, const QString& property_name,
                      const QString& label, const bool& label_left) :
        Property(parent, property_name, PROP_LINEEDIT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        this->setLayout(layout_);
        lineedit_  = new QLineEdit();

        if(label.size() > 0){
            QLabel* label_  = new QLabel(label);
            if(label_left){
                layout_->addWidget(label_);
                layout_->addWidget(lineedit_);
            }
            else{
                layout_->addWidget(lineedit_);
                layout_->addWidget(label_);
            }
        }
        else{
            layout_->addWidget(lineedit_);
        }
    }

    ///
    /// \brief value
    /// \return
    ///
    QString value(){
        return lineedit_->text();
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        lineedit_->setText(val.toString());
    }

private:
    QLineEdit* lineedit_;
    QString label_text_;

signals:
    ///
    /// \brief textChanged
    /// \param text
    ///
    void textChanged(const QString& text);

};

class Property_FileSelector: public Property{
    Q_OBJECT

public:
    ///
    Property_FileSelector(QWidget *parent, const QString& property_name,
                          const QString& label, const bool& label_left,
                          const QString& button_label,
                          const QString& dialog_label,
                          const bool& readFile = true) :
        Property(parent, property_name, PROP_FILESELECT){

        dialog_label_ = dialog_label;

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        this->setLayout(layout_);

        lineedit_  = new QLineEdit();

        if(label.size() > 0){
            QLabel* label_  = new QLabel(label);
            if(label_left){
                layout_->addWidget(label_);
                layout_->addWidget(lineedit_);
            }
            else{
                layout_->addWidget(lineedit_);
                layout_->addWidget(label_);
            }
        }
        else{
            layout_->addWidget(lineedit_);
        }

        button_select_ = new QPushButton(button_label);
        layout_->addWidget(button_select_);
        if(readFile){
            connect(button_select_, &QPushButton::clicked, [this](){
                //Open file dialog
                QString file_name = QFileDialog::getOpenFileName(this, dialog_label_);
                QFile file(file_name);
                if(file.exists()){
                    lineedit_->setEnabled(false);
                    lineedit_->setText(file_name);
                    lineedit_->setEnabled(true);
                    emit textChanged(file_name);
                }
            });
        }
        else{
            connect(button_select_, &QPushButton::clicked, [this](){
                //Open file dialog
                QString file_name = QFileDialog::getSaveFileName(this, dialog_label_);
                lineedit_->setEnabled(false);
                lineedit_->setText(file_name);
                lineedit_->setEnabled(true);
                emit textChanged(file_name);

            });
        }
    }

    ///
    /// \brief value
    /// \return
    ///
    QString value(){
        return lineedit_->text();
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        lineedit_->setEnabled(false);
        lineedit_->setText(val.toString());
        lineedit_->setEnabled(true);
    }

private:
    QLineEdit* lineedit_;
    QString label_text_, dialog_label_;
    QPushButton* button_select_;

signals:
    ///
    /// \brief textChanged
    /// \param text
    ///
    void textChanged(const QString& text);

};

class Property_DirSelector: public Property{
    Q_OBJECT

public:
    ///
    Property_DirSelector(QWidget *parent, const QString& property_name,
                         const QString& label, const bool& label_left,
                         const QString& button_label,
                         const QString& dialog_label) :
        Property(parent, property_name, PROP_DIRSELECT){

        dialog_label_ = dialog_label;

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        this->setLayout(layout_);

        lineedit_  = new QLineEdit();

        if(label.size() > 0){
            QLabel* label_  = new QLabel(label);
            if(label_left){
                layout_->addWidget(label_);
                layout_->addWidget(lineedit_);
            }
            else{
                layout_->addWidget(lineedit_);
                layout_->addWidget(label_);
            }
        }
        else{
            layout_->addWidget(lineedit_);
        }

        button_select_ = new QPushButton(button_label);
        layout_->addWidget(button_select_);
        connect(button_select_, &QPushButton::clicked, [this](){
            //Open dir dialog
            QString dir_name = QFileDialog::getExistingDirectory(this, dialog_label_, "~");
            QDir dir(dir_name);
            if(dir.exists()){
                lineedit_->setEnabled(false);
                lineedit_->setText(dir_name);
                lineedit_->setEnabled(true);
                emit textChanged(dir_name);
            }
        });

        button_open_ = new QPushButton("Open");
        layout_->addWidget(button_open_);
        connect(button_open_, &QPushButton::clicked, [this](){
            QDir dir(lineedit_->text());
            if(dir.exists()){
                // Open dir in default file explorer
                QDesktopServices::openUrl(QUrl::fromLocalFile(lineedit_->text()));
            }
        });
    }

    ///
    /// \brief value
    /// \return
    ///
    QString value(){
        return lineedit_->text();
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        lineedit_->setEnabled(false);
        lineedit_->setText(val.toString());
        lineedit_->setEnabled(true);
    }

private:
    QLineEdit* lineedit_;
    QString label_text_, dialog_label_;
    QPushButton* button_select_;
    QPushButton* button_open_;

signals:
    ///
    /// \brief textChanged
    /// \param text
    ///
    void textChanged(const QString& text);

};

struct Range{

    Range(const int& a, const int& b){
        lower = a;
        upper = b;
    }

    int lower;
    int upper;
};

class Property_Range : public Property{

    Q_OBJECT

public:
    ///
    Property_Range(QWidget *parent, const QString& property_name,
                  const QString& label,
                  const QString& label_lower,
                  const QString& label_upper,
                  const int& min_lower, const int& max_lower,
                  const int& current_lower,
                  const int& min_upper, const int& max_upper,
                  const int& current_upper) :
        Property(parent, property_name, PROP_RANGE){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        this->setLayout(layout_);

        if(label.size() > 0){
            layout_->addWidget(new QLabel(label));
        }
        if(label_lower.size() > 0){
            layout_->addWidget(new QLabel(label_lower));
        }
        spin_lower_  = new QSpinBox();
        spin_lower_->setMinimum(min_lower);
        spin_lower_->setMaximum(max_lower);
        spin_lower_->setValue(current_lower);
        layout_->addWidget(spin_lower_);
        if(label_upper.size() > 0){
            layout_->addWidget(new QLabel(label_upper));
        }
        spin_upper_  = new QSpinBox();
        spin_upper_->setMinimum(min_upper);
        spin_upper_->setMaximum(max_upper);
        spin_upper_->setValue(current_upper);
        layout_->addWidget(spin_upper_);

        layout_->addItem(new QSpacerItem(0,0,QSizePolicy::Expanding));

        connect(spin_lower_, qOverload<int>(&QSpinBox::valueChanged),
                [this](const int& value){emit valueChanged(value, spin_upper_->value());});
        connect(spin_upper_, qOverload<int>(&QSpinBox::valueChanged),
                [this](const int& value){emit valueChanged(spin_lower_->value(), value);});
    }


    ///
    /// \brief values
    /// \return
    ///
    Range values(){
        if(spin_lower_ && spin_upper_){
            try{
                return Range(spin_lower_->value(), spin_upper_->value());
            }
            catch(...){
            }
        }
        return Range(INT_MAX, INT_MAX);
    }

    ///
    /// \brief value
    /// \return
    ///
    QPoint value(){
        Range range = values();
        return QPoint(range.lower, range.upper);
    }

    ///
    /// \brief lower
    /// \return
    ///
    int lower(){
        if(spin_lower_){
            try{
                return spin_lower_->value();
            }
            catch(...){
            }
        }
        return INT_MAX;
    }

    ///
    /// \brief upper
    /// \return
    ///
    int upper(){
        if(spin_upper_){
            try{
                return spin_upper_->value();
            }
            catch(...){
            }
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        setLower(val.toPoint().x());
        setUpper(val.toPoint().y());
    }

    ///
    /// \brief setValues
    /// \param lower
    /// \param upper
    ///
    void setValues(const QVariant& lower, const QVariant& upper){
        setLower(lower.toInt());
        setUpper(upper.toInt());
    }

    ///
    /// \brief setValues
    /// \param lower
    /// \param upper
    ///
    void setValues(const int& lower, const int& upper){
        setLower(lower);
        setUpper(upper);
    }

    ///
    /// \brief setLower
    /// \param lower
    ///
    void setLower(const QVariant& lower){
        setLower(lower.toInt());
    }

    ///
    /// \brief setLower
    /// \param lower
    ///
    void setLower(const int& lower){
        spin_lower_->setEnabled(false);
        spin_lower_->setValue(lower);
        spin_lower_->setEnabled(true);
    }

    ///
    /// \brief setUpper
    /// \param upper
    ///
    void setUpper(const QVariant& upper){
        setUpper(upper.toInt());
    }

    ///
    /// \brief setUpper
    /// \param upper
    ///
    void setUpper(const int& upper){
        spin_upper_->setEnabled(false);
        spin_upper_->setValue(upper);
        spin_upper_->setEnabled(true);
    }

private:
    QSpinBox* spin_lower_;
    QSpinBox* spin_upper_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& lower, const int& upper);

};

////////////////////////////////////////////////////////////////////////////////

///
/// \brief The Pose struct contains a transform between two 3D coordinate frames
///                        in different forms
///
struct Pose{

    ///
    /// \brief Pose is the default constructor
    ///
    Pose(){
        m_cv_ = cv::Mat::eye(4,4, CV_64F);
        R_cv_ = m_cv_(cv::Rect(0,0,3,3)).clone();
        t_cv_ = m_cv_(cv::Rect(3,0,1,3)).clone();
        cv::cv2eigen(m_cv_, m_);
        cv::cv2eigen(R_cv_, R_);
        cv::cv2eigen(t_cv_, t_);
    }

    ///
    /// \brief Pose is a constructor taking a 4x4 Eigen matrix
    /// \param P
    ///
    Pose(const Eigen::Matrix4d& P){
        m_ = P;
        R_ = m_.block(0,0,3,3);
        t_ = m_.block(0,3,3,1);
        cv::eigen2cv(m_,m_cv_);
        cv::eigen2cv(R_,R_cv_);
        cv::eigen2cv(t_,t_cv_);
    }

    ///
    /// \brief Pose is a constructor taking a 4x4 OpenCV matrix
    /// \param P
    ///
    Pose(const cv::Mat& P){
        m_cv_ = P.clone();
        R_cv_ = m_cv_(cv::Rect(0,0,3,3)).clone();
        t_cv_ = m_cv_(cv::Rect(3,0,1,3)).clone();
        cv::cv2eigen(m_cv_, m_);
        cv::cv2eigen(R_cv_, R_);
        cv::cv2eigen(t_cv_, t_);
    }

    ///
    /// \brief Pose is a constructor taking a 3x3 OpenCV rotation matrix and a
    ///             3x1 OpenCV translation vector
    /// \param R
    /// \param t
    ///
    Pose(const cv::Mat& R, const cv::Mat t){
        cv::Mat P = cv::Mat(3,4,CV_64F);
        R.copyTo(P(cv::Rect(0,0,3,3)));
        t.copyTo(P(cv::Rect(3,0,1,3)));
        m_cv_ = P.clone();
        R_cv_ = m_cv_(cv::Rect(0,0,3,3)).clone();
        t_cv_ = m_cv_(cv::Rect(3,0,1,3)).clone();
        cv::cv2eigen(m_cv_, m_);
        cv::cv2eigen(R_cv_, R_);
        cv::cv2eigen(t_cv_, t_);
    }

    ///
    /// \brief Pose is a constructor taking a 3x3 Eigen matrix and a 3d Eigen
    ///             vector
    /// \param R    rotation
    /// \param t    translation
    ///
    Pose(const Eigen::Matrix3d& R, const Eigen::Vector3d& t){
        m_ = Eigen::Matrix4d::Identity();
        m_.block(0,0,3,3) = R;
        m_.block(0,3,3,1) = t;
        R_ = R;
        t_ = t;
        cv::eigen2cv(m_,m_cv_);
        cv::eigen2cv(R_,R_cv_);
        cv::eigen2cv(t_,t_cv_);
    }

    ///
    /// \brief operator * multiplies this pose with a given other pose
    /// \param pose       the other pose
    /// \return           the product of these transformations
    ///
    Pose operator * (const Pose& pose) const{
        return Pose(R_ * pose.R_, R_ * pose.t_ + t_);
    }

    /// pose as 4x4 OpenCV mat
    cv::Mat m_cv_;
    /// pose as OpenCV mats in the form of a 3x3 rotation matrix...
    cv::Mat R_cv_;
    /// ...and a 1x3 translation vector
    cv::Mat t_cv_;

    /// pose as 4x4 Eigen matrix
    Eigen::Matrix4d m_;
    /// pose as 3x3 rotation Eigen matrix...
    Eigen::Matrix3d R_;
    /// ...and 3d translation Eigen vector
    Eigen::Vector3d t_;
};

///
/// \brief The Cam_Parameters struct contains the intrinsic parameters of a
///                                  single camera (i.e. the rgb or ir camera of
///                                  a device).
///
struct Cam_Parameters{
    /// camera name, e.g. 'irkinect0'
    std::string name_;

    /// resolution
    int res_x_;
    int res_y_;
    /// camera matrix
    cv::Mat K_;
    /// and its inverse
    cv::Mat K_inv_;
    /// distortion coefficients
    cv::Mat dist_;

    /// distortion model
    int dist_model_;


    /// camera pose relative to world coordinates as 4x4 matrix, i.e. transform
    /// world->camera
    cv::Mat pose_;

    /// precalculated undistortion maps for image rectification
    cv::Mat map_x_;
    cv::Mat map_y_;
};

///
/// \brief The Device struct describes an RGB camera
///
struct Device{

    ///
    /// \brief Device
    /// \param name
    ///
    Device(const std::string& name){
        device_name_ = name;
    }
    /// camera device name, e.g. 'kinect0'
    std::string device_name_;
    /// intrinsics
    Cam_Parameters rgb_;
    /// transformation from world coordinate frame to rgb camera
    Pose world_pose_;
};

///
/// \brief The Detection struct
///
struct Detection{

    /// associated image file paths
    std::string rgb_image_file_;

    /// is set to false if ball can not be detected
    bool valid_;

    /// is set to true if detection is an inlier for initial pose estimation
    std::vector<size_t> inlier_for_;

    /// rgb pixel coordinates
    double x_;
    double y_;
    /// size of the ball detected in the rgb image in pixels
    double r_;

    /// 3D scene point in rgb camera coordinates
    Eigen::Vector3d p_ = Eigen::Vector3d(0.0,0.0,-1.0);

    ///
    /// \brief pixelCV returns the rgb pixel coordinates as OpenCV Point
    /// \return
    ///
    cv::Point2d pixelCV(){return cv::Point2d(x_, y_);}

    ///
    /// \brief pixelEig returns the rgb pixel coordinates as Eigen Vector
    /// \return
    ///
    Eigen::Vector2d pixelEig(){return Eigen::Vector2d(x_, y_);}
};

/// alias for readability - Detections[i][j] contains the detection from image i
/// of camera j
using Detections = std::vector<std::vector<Detection>>;

