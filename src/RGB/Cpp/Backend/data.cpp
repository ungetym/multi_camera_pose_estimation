#include "data.h"
#include <fstream>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

Data::Data(){

    // Create shared properties
    image_dir_ = std::shared_ptr<Property_DirSelector>(new Property_DirSelector(nullptr, "ImageDir", "", true, "Select", "Choose a directory"));
    image_name_scheme_ = std::shared_ptr<Property_LineEdit>(new Property_LineEdit(nullptr, "RGBImageScheme", "", true));
    intrinsics_within_single_file_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "IntrinsicsSingleFile", "Single Calibration File", false));
    intrinsics_file_path_= std::shared_ptr<Property_FileSelector>(new Property_FileSelector(nullptr, "IntrinsicsFile", "", true, "Select", "Choose a file"));
    intrinsics_folder_path_ = std::shared_ptr<Property_DirSelector>(new Property_DirSelector(nullptr, "IntrinsicsDir", "", true, "Select", "Choose a directory"));
    camera_name_scheme_ = std::shared_ptr<Property_LineEdit>(new Property_LineEdit(nullptr, "RGBCamScheme", "", true));
    advanced_options_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "AdvancedOptions", "Advanced Options", false));

    property_list_ = {image_dir_, image_name_scheme_,
                      intrinsics_within_single_file_, intrinsics_file_path_,
                      intrinsics_folder_path_, camera_name_scheme_,
                      advanced_options_};

    // Create instances of the imagelist generator, detector and optimizer
    imagelist_gen_ = std::shared_ptr<Imagelist_Generator>(new Imagelist_Generator());
    detector_ = std::shared_ptr<Detector>(new Detector(imagelist_gen_));
    optimizer_ = std::shared_ptr<Optimizer>(new Optimizer());

    // Get properties and add to list
    std::vector<std::shared_ptr<Property>> detector_properties = detector_->getProperties();
    property_list_.insert(property_list_.end(), detector_properties.begin(), detector_properties.end());
    std::vector<std::shared_ptr<Property>> opt_initial_properties = optimizer_->getPropertiesInitial();
    property_list_.insert(property_list_.end(), opt_initial_properties.begin(), opt_initial_properties.end());
    std::vector<std::shared_ptr<Property>> opt_LM_properties = optimizer_->getPropertiesLM();
    property_list_.insert(property_list_.end(), opt_LM_properties.begin(), opt_LM_properties.end());
    std::vector<std::shared_ptr<Property>> opt_eval_properties = optimizer_->getPropertiesEval();
    property_list_.insert(property_list_.end(), opt_eval_properties.begin(), opt_eval_properties.end());


    detector_thread_ = new QThread();
    //detector_->moveToThread(detector_thread_);
    detector_thread_->start();

    optimizer_thread_ = new QThread();
    //optimizer_->moveToThread(optimizer_thread_);
    optimizer_thread_->start();

    // Load previous GUI settings if available
    loadUISettings(property_list_);
}

bool Data::saveUISettings(const std::vector<std::shared_ptr<Property>>& settings,
                          const bool& ask_user){

    if(settings.size() == 0){
        return false;
    }

    QString file_path = "default_settings.json";
    if(ask_user){
        // Ask the user where to store the file
        file_path = QFileDialog::getSaveFileName(nullptr,
                                                 "Create a new settings file",
                                                 QDir::homePath(),
                                                 "JSON Files (*.json)",
                                                 nullptr,
                                                 QFileDialog::DontConfirmOverwrite);

        if(file_path.size() == 0){
            ERROR("No file chosen.");
            return false;
        }

        if(file_path.right(4).compare("json") != 0){
            file_path += ".json";
        }
    }

    // Open file
    QFile file(file_path);
    if(!file.open(QIODevice::ReadWrite)) {
        ERROR("Unable to open the file");
        return false;
    }

    QJsonArray json_data;
    for(const std::shared_ptr<Property>& setting : settings) {
        QJsonObject setting_object;
        setting_object.insert("Name", setting->name());
        int type = setting->type();
        setting_object.insert("Type", type);
        if(type == PROP_SLIDER_INT){
            Property_Slider* slider = static_cast<Property_Slider*>(setting.get());
            setting_object.insert("Value", slider->value());
        }
        else if(type == PROP_SPIN_INT){
            Property_Spin* spin = static_cast<Property_Spin*>(setting.get());
            setting_object.insert("Value", spin->value());
        }
        else if(type == PROP_SPIN_DOUBLE){
            Property_Spin_F* spin = static_cast<Property_Spin_F*>(setting.get());
            setting_object.insert("Value", spin->value());
        }
        else if(type == PROP_SELECTOR){
            Property_Selector* select = static_cast<Property_Selector*>(setting.get());
            setting_object.insert("Value", select->index());
        }
        else if(type == PROP_CHECK){
            Property_Check* check = static_cast<Property_Check*>(setting.get());
            setting_object.insert("Value", check->value());
        }
        else if(type == PROP_LINEEDIT){
            Property_LineEdit* lineedit = static_cast<Property_LineEdit*>(setting.get());
            setting_object.insert("Value", lineedit->value());
        }
        else if(type == PROP_FILESELECT){
            Property_FileSelector* selector = static_cast<Property_FileSelector*>(setting.get());
            setting_object.insert("Value", selector->value());
        }
        else if(type == PROP_DIRSELECT){
            Property_DirSelector* selector = static_cast<Property_DirSelector*>(setting.get());
            setting_object.insert("Value", selector->value());
        }
        else if(type == PROP_RANGE){
            Property_Range* range = static_cast<Property_Range*>(setting.get());
            setting_object.insert("Value", range->lower());
            setting_object.insert("Value2", range->upper());
        }
        json_data.append(setting_object);
    }

    // Clear the original content in the file
    file.resize(0);
    //QJsonDocument doc;
    QJsonDocument doc;
    doc.setArray(json_data);
    file.write(doc.toJson());
    file.close();

    return true;
}

bool Data::loadUISettings(const std::vector<std::shared_ptr<Property>>& settings,
                          const bool& ask_user){

    if(settings.size() == 0){
        return false;
    }

    QString file_path = "default_settings.json";
    if(ask_user){
        // Ask the user which file to load
        file_path = QFileDialog::getOpenFileName(nullptr,
                                                 "Load settings file",
                                                 QDir::homePath(),
                                                 "JSON Files (*.json)",
                                                 nullptr);

        if(file_path.size() == 0){
            ERROR("No file chosen.");
            return false;
        }

        if(file_path.right(4).compare("json") != 0){
            file_path += ".json";
        }
    }

    // Open file and read all data
    QString val;
    QFile file;
    file.setFileName(file_path);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)){
        val = file.readAll();
        file.close();

        // Decode json structure
        QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
        QJsonArray root = doc.array();
        for(int property_idx = 0; property_idx < root.count(); property_idx++){
            QJsonValue property_val = root.at(property_idx);
            QJsonObject property_obj = property_val.toObject();
            QString name = property_obj.value(QString("Name")).toString();
            int type = property_obj.value(QString("Type")).toInt();
            QJsonValue value = property_obj.value("Value");
            QVariant value_var(value);
            //
            for(std::shared_ptr<Property> property : settings){
                if(property->name().compare(name) == 0){

                    if(property->type() == PROP_RANGE){
                        Property_Range* prop = static_cast<Property_Range*>(property.get());
                        prop->setLower(value_var);
                        value = property_obj.value("Value2");
                        QVariant value_var2(value);
                        prop->setUpper(value_var2);
                    }
                    else{
                        property->setValue(value_var);
                    }
                    break;
                }
            }

        }

        return true;
    }
    return false;
}

bool Data::parseCameraNameScheme(const std::string &input_string, std::vector<Tag> *scheme){

    // search for tag $CAM and label everything else as string tag
    int last = -1;
    for(size_t i = 0; i< input_string.size(); i++){
        if(input_string[i] == '$'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }

            if(input_string.substr(i,4).compare("$CAM") == 0){
                scheme->push_back(Tag(CAM,"$CAM"));
                i += 3;
                last = i;
            }
            else{
                ERROR("Unable to parse naming scheme: "
                      + QString::fromStdString(input_string)
                      + " Only the tag $CAM is allowed");
                scheme->clear();
                return false;
            }
        }
        else if(i == input_string.size() - 1){
            scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
        }
    }

    return true;
}

bool Data::loadCalibration(){
    if(intrinsics_within_single_file_->value()){
        std::string path = intrinsics_file_path_->value().toStdString();
        int size = path.size();
        if(size < 8){
            return false;
        }

        if(path.substr(size-4).compare("json") == 0){
            devices_.clear();
            return loadJSONCalibration(intrinsics_file_path_->value(), &devices_);
        }
        else if(path.substr(size-8).compare("MCC_Cams") == 0){
            return parseMCCCalibration();
        }
        else{
            ERROR("The calibration file must either be of type .json or .MCC_Cams");
            return false;
        }
    }
    else{
        // Collect .json files from dir and subdirs
        QDir dir(intrinsics_folder_path_->value());
        if(!dir.exists()){
            ERROR("Calibration dir does not exist.");
            return false;
        }
        devices_.clear();

        QStringList calibration_file_list = dir.entryList({"*.json"});
        QStringList sub_dirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        for(const QString &sub_dir_name : sub_dirs) {
            QDir sub_dir(dir.absoluteFilePath(sub_dir_name));
            QStringList sub_file_list = sub_dir.entryList({"*.json"});
            for(const QString& file_name : sub_file_list){
                calibration_file_list.push_back(sub_dir_name+"/"+file_name);
            }
        }
        for(const QString &calibration_file_name : calibration_file_list){
            QString calibration_file_path = intrinsics_folder_path_->value() + "/" + calibration_file_name;
            loadJSONCalibration(calibration_file_path, &devices_);
        }

    }

    return true;
}

bool Data::parseMCCCalibration(){

    devices_.clear();

    // parse camera name schemes
    std::vector<Tag> rgb_cam_scheme, ir_cam_scheme;
    if(!parseCameraNameScheme(camera_name_scheme_->value().toStdString(),
                              &rgb_cam_scheme)){
        return false;
    }

    // helper function to jump around ifstream
    auto goToLine = [](std::ifstream& file, const unsigned int& num){
        file.seekg(std::ios::beg);
        for(int i=0; i < num - 1; ++i){
            file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        return !file.eof();
    };

    // open file
    std::ifstream file(intrinsics_file_path_->value().toStdString().c_str());
    // check if file can be read
    if(!file.is_open ()){
        ERROR("Calibration file could not be opened");
        return false;
    }

    // starting line after initial description in MCC_Cams file
    int current_line = 6;
    // save index for every camera - this is helpful if the ir and rgb cameras
    // are not stored in a known order
    std::map<std::string,int> name_index;

    // start reading
    std::vector<std::string> camera_names;
    std::string camera_name;
    while(goToLine(file, current_line)){

        // extract camera name
        file >> camera_name >> camera_name;

        // if eof, end reading
        if(file.eof()){
            break;
        }

        // check if one of the naming schemes matches
        bool rgb_scheme_matches = true;
        bool rgb_has_str_tag = false;
        std::string camera_name_rgb = camera_name;
        for(const Tag& tag : rgb_cam_scheme){
            if(tag.first > 0){
                size_t pos = camera_name.find(tag.second);
                rgb_scheme_matches = (pos != std::string::npos);
                rgb_has_str_tag = true;
                if (rgb_scheme_matches){
                    camera_name_rgb.erase(pos, tag.first);
                }
            }
        }
        bool ir_scheme_matches = true;
        bool ir_has_str_tag = false;
        std::string camera_name_ir = camera_name;
        for(const Tag& tag : ir_cam_scheme){
            if(tag.first > 0){
                size_t pos = camera_name.find(tag.second);
                ir_scheme_matches = (pos != std::string::npos);
                ir_has_str_tag = true;
                if (ir_scheme_matches){
                    camera_name_ir.erase(pos, tag.first);
                }
            }
        }

        bool is_rgb = rgb_scheme_matches && (!ir_scheme_matches ||
                                             (ir_scheme_matches && rgb_has_str_tag && !ir_has_str_tag));
        camera_name = is_rgb ? camera_name_rgb : camera_name_ir;

        // search for camera name in existing cameras
        Cam_Parameters* params;
        auto pos = std::find(camera_names.begin(), camera_names.end(),
                             camera_name);
        if(pos != camera_names.end()){
            params = &(devices_[std::distance(
                        camera_names.begin(), pos)].rgb_);
        }
        else{
            camera_names.push_back(camera_name);
            devices_.push_back(Device(camera_name));
            params = &(devices_.back().rgb_);
        }

        params->name_ = camera_name_rgb;

        if(is_rgb){
            STATUS("Reading RGB camera parameters of device "
                   + QString::fromStdString(camera_name));
        }
        else{
            STATUS("Reading IR camera parameters of device "
                   + QString::fromStdString(camera_name));
        }

        // read resolution
        file >> params->res_x_ >> params->res_y_;

        // read camera distortion parameters
        goToLine(file, current_line+1);
        params->dist_ = cv::Mat(8,1, CV_64F);
        for(int i = 0; i < 8; i++){
            file >> params->dist_.at<double>(0,i);
        }
        LOGMATRIX("Dist =",params->dist_);


        // read K matrix
        goToLine(file, current_line+2);
        params->K_ = cv::Mat(3,3,CV_64F);
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                file >> params->K_.at<double>(row,col);
            }
        }
        params->K_inv_ = params->K_.inv();
        LOGMATRIX("K =",params->K_);

        // read the pose
        goToLine(file, current_line+3);
        params->pose_ = cv::Mat(4, 4,CV_64F);
        for(int row = 0; row < 4; row++){
            for(int col = 0; col < 4; col++){
                file >> params->pose_.at<double>(row,col);
            }
        }
        LOGMATRIX("pose =",params->pose_);

        // go to next camera
        current_line += 4;
    }

    return true;
}

bool Data::loadJSONCalibration(const QString &file_path, std::vector<Device> *devices){

    if(!devices || file_path.size() == 0){
        return false;
    }

    // Parse camera name schemes
    std::vector<Tag> rgb_cam_scheme;
    if(!parseCameraNameScheme(camera_name_scheme_->value().toStdString(),
                              &rgb_cam_scheme)){
        return false;
    }

    // Open file and read all data
    QString val;
    QFile file;
    file.setFileName(file_path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    // Decode json structure
    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonArray root = doc.array();
    for(int cam_idx = 0; cam_idx < root.count(); cam_idx++){
        QJsonValue cam_obj_val = root.at(cam_idx);
        QJsonObject cam_obj = cam_obj_val.toObject();
        QJsonObject params_obj = cam_obj.value("Parameters").toObject();

        // Camera name
        QJsonValue name = cam_obj.value(QString("Name"));
        std::string camera_name = name.toString().toStdString();

        // Check if the naming schemes matches
        bool rgb_scheme_matches = true;
        std::string camera_name_rgb = camera_name;
        for(const Tag& tag : rgb_cam_scheme){
            if(tag.first > 0){
                size_t pos = camera_name.find(tag.second);
                rgb_scheme_matches = (pos != std::string::npos);
                if (rgb_scheme_matches){
                    camera_name_rgb.erase(pos, tag.first);
                }
            }
        }
        if(!rgb_scheme_matches){
            ERROR("The camera name " + QString::fromStdString(camera_name)
                  + " does not match the specified naming scheme.");
            return false;
        }

        // Check whether device already exists
        bool cam_exists = false;
        for(const Device& dev : *devices){
            if(dev.device_name_.compare(camera_name) == 0){
                WARN("Multiple intrinsics for camera "+QString::fromStdString(camera_name)
                     +". Calibration from file "+ file_path +" ignored.");
                cam_exists = true;
                break;
            }
        }
        if(cam_exists){
            continue;
        }

        // Create new device
        devices->push_back(Device(camera_name));
        Cam_Parameters* params = &(devices->back().rgb_);

        // Resolution
        QJsonObject res_obj = params_obj.value(QString("Resolution")).toObject();
        params->res_x_ = res_obj["Width"].toInt();
        params->res_y_ = res_obj["Height"].toInt();

        // K matrix
        cv::Mat K = cv::Mat::zeros(3,3,CV_64FC1);
        QJsonObject K_obj = params_obj.value(QString("K matrix")).toObject();
        K.at<double>(0,0) = K_obj["fx"].toDouble();
        K.at<double>(1,1) = K_obj["fy"].toDouble();
        K.at<double>(0,2) = K_obj["cx"].toDouble();
        K.at<double>(1,2) = K_obj["cy"].toDouble();
        K.at<double>(2,2) = 1.0;
        params->K_ = K.clone();
        params->K_inv_ = params->K_.inv();

        // Distortion parameters
        QJsonObject dist_obj = params_obj.value(QString("Distortion parameters")).toObject();
        params->dist_model_ = dist_obj["Distortion model"].toInt();

        params->dist_ = params->dist_model_ == CALIB_BROWN ? cv::Mat::zeros(5,1,CV_64FC1) : cv::Mat::zeros(8,1,CV_64FC1);
        params->dist_.at<double>(2,0) = dist_obj["p1"].toDouble();
        params->dist_.at<double>(3,0) = dist_obj["p2"].toDouble();
        params->dist_.at<double>(0,0) = dist_obj["k1"].toDouble();
        params->dist_.at<double>(1,0) = dist_obj["k2"].toDouble();
        params->dist_.at<double>(4,0) = dist_obj["k3"].toDouble();
        if(params->dist_model_ == CALIB_RATIONAL){
            params->dist_.at<double>(5,0) = dist_obj["k4"].toDouble();
            params->dist_.at<double>(6,0) = dist_obj["k5"].toDouble();
            params->dist_.at<double>(7,0) = dist_obj["k6"].toDouble();
        }
    }

    return true;
}

void Data::saveCalibration(const std::string& file_path){
    int size = file_path.size();
    if(size < 1){
        ERROR("Output name too short.");
        return;
    }

    if(size > 5 && file_path.substr(size-4).compare("json") == 0){
        saveJSONCalibration(file_path);
    }
    else if(size > 9 && file_path.substr(size-8).compare("MCC_Cams") == 0){
        saveMCCCalibration(file_path);
    }
    else{
        saveJSONCalibration(file_path+".json");
    }
}

void Data::saveMCCCalibration(const std::string& file_path){

    // open target file
    std::ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Output file "+ QString::fromStdString(file_path)+" could not be opened!");
        return;
    }

    // write header
    file << "$CamID $Width $Height $CamType\n"
            "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6\n"
            "$K matrix rowwise\n"
            "$Pose matrix rowwise\n";

    // write devices
    for(const Device& device: devices_){
        for(int i = 0; i < 2; i++){

            const Cam_Parameters& cam = device.rgb_;

            // write camera name and resolution
            file << "\nCam " << cam.name_ << " " << cam.res_x_
                 << " " << cam.res_y_ << " -1\n";
            // write ir intrinsics
            for(size_t j = 0; j < 7; j++){
                file << cam.dist_.at<double>(0,j) << " ";
            }
            file << cam.dist_.at<double>(0,7) << "\n";
            // write K matrix
            for(size_t row = 0; row < 3; row++){
                for(size_t col = 0; col < 3; col++){
                    file << cam.K_.at<double>(row,col) ;
                    if(col == 2 && row == 2){
                        file << "\n";
                    }
                    else{
                        file << " ";
                    }
                }
            }
            // write pose
            const Pose pose = device.world_pose_;
            for(size_t row = 0; row < 4; row++){
                for(size_t col = 0; col < 4; col++){
                    file << pose.m_cv_.at<double>(row,col);
                    if(col < 3 || row < 3){
                        file << " ";
                    }
                }
            }
        }
    }

    file.close();

    STATUS("Poses successfully saved.");
}

void Data::saveJSONCalibration(const std::string& file_path){

    // Open file
    QFile file(QString::fromStdString(file_path));
    if(!file.open(QIODevice::ReadWrite)) {
        ERROR("Unable to open the file "+QString::fromStdString(file_path));
        return;
    }

    // Clear the original content in the file
    file.resize(0);

    QJsonArray json_data;

    for(const Device& dev : devices_) {

        const Cam_Parameters& cam = dev.rgb_;

        QJsonObject cam_object;
        cam_object.insert("Name", QString::fromStdString(dev.device_name_));
        QJsonObject cam_params_object;

        QJsonObject res_object;
        res_object.insert("Width",cam.res_x_);
        res_object.insert("Height",cam.res_y_);
        cam_params_object.insert("Resolution", res_object);

        QJsonObject K_object;
        K_object.insert("fx",cam.K_.at<double>(0,0));
        K_object.insert("fy",cam.K_.at<double>(1,1));
        K_object.insert("cx",cam.K_.at<double>(0,2));
        K_object.insert("cy",cam.K_.at<double>(1,2));
        cam_params_object.insert("K matrix", K_object);

        QJsonObject distortion_object;
        distortion_object.insert("Distortion model", cam.dist_model_);
        distortion_object.insert("k1",cam.dist_.at<double>(0,0));
        distortion_object.insert("k2",cam.dist_.at<double>(1,0));
        distortion_object.insert("k3",cam.dist_.at<double>(4,0));
        if(cam.dist_model_ == CALIB_RATIONAL){
            distortion_object.insert("k4",cam.dist_.at<double>(5,0));
            distortion_object.insert("k5",cam.dist_.at<double>(6,0));
            distortion_object.insert("k6",cam.dist_.at<double>(7,0));
        }
        distortion_object.insert("p1",cam.dist_.at<double>(2,0));
        distortion_object.insert("p2",cam.dist_.at<double>(3,0));
        cam_params_object.insert("Distortion parameters", distortion_object);

        QJsonObject pose_object;
        QJsonArray rotation;
        cv::Mat R_rod;
        cv::Rodrigues(dev.world_pose_.R_cv_, R_rod);
        rotation.append(R_rod.at<double>(0,0));
        rotation.append(R_rod.at<double>(1,0));
        rotation.append(R_rod.at<double>(2,0));
        pose_object.insert("Rotation (Rodrigues Vector)", rotation);
        QJsonArray translation;
        translation.append(dev.world_pose_.t_cv_.at<double>(0,0));
        translation.append(dev.world_pose_.t_cv_.at<double>(1,0));
        translation.append(dev.world_pose_.t_cv_.at<double>(2,0));
        pose_object.insert("Translation", translation);
        QJsonArray transform;
        for(int row = 0; row < 4; row++){
            for(int col = 0; col < 4; col++){
                transform.append(dev.world_pose_.m_cv_.at<double>(row,col));
            }
        }
        pose_object.insert("Transformation_Matrix_(rowwise)", transform);
        cam_params_object.insert("Pose (Transformation from reference camera)", pose_object);

        cam_object.insert("Parameters", cam_params_object);
        json_data.append(cam_object);
    }

    QJsonDocument doc;
    doc.setArray(json_data);
    file.write(doc.toJson());
    file.close();
}

void Data::saveDetections(const std::string& file_path){
    // Open file
    QFile file(QString::fromStdString(file_path));
    if(!file.open(QIODevice::ReadWrite)) {
        ERROR("Unable to open the file "+QString::fromStdString(file_path));
        return;
    }

    // Clear the original content in the file
    file.resize(0);

    QJsonArray json_data;
    int counter = 0;
    for(const std::vector<Detection>& detections : *detections_) {

        QJsonObject idx_object;
        idx_object.insert("IDX", counter);
        counter++;
        QJsonArray detections_array;

        for(const Detection& detection : detections){
            QJsonObject detection_object;
            detection_object.insert("x", detection.x_);
            detection_object.insert("y", detection.y_);
            detection_object.insert("valid", detection.valid_);
            detection_object.insert("r", detection.r_);
            detection_object.insert("file", QString::fromStdString(detection.rgb_image_file_));

            QJsonArray p_3d;
            p_3d.append(detection.p_.x());
            p_3d.append(detection.p_.y());
            p_3d.append(detection.p_.z());
            detection_object.insert("world_point", p_3d);

            QJsonArray inlier_list;
            for(const size_t& idx : detection.inlier_for_){
                inlier_list.append(int(idx));
            }
            detection_object.insert("inlier_list", inlier_list);
            detections_array.append(detection_object);
        }
        idx_object.insert("Detections", detections_array);
        json_data.append(idx_object);
    }

    QJsonDocument doc;
    doc.setArray(json_data);
    file.write(doc.toJson());
    file.close();
}

void Data::loadDetections(const std::string& file_path){
    // Open file and read all data
    QString val;
    QFile file;
    file.setFileName(QString::fromStdString(file_path));
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    // Decode json structure
    if(detections_){
        detections_->clear();
    }
    else{
        detections_ = std::shared_ptr<Detections>(new Detections());
    }
    QJsonDocument doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonArray root = doc.array();
    for(int detections_idx = 0; detections_idx < root.count(); detections_idx++){

        detections_->push_back(std::vector<Detection>());

        QJsonValue idx_val = root.at(detections_idx);
        QJsonObject idx_object = idx_val.toObject();

        QJsonArray detections_array = idx_object.value("Detections").toArray();

        for(const QJsonValue& detection_val : detections_array){
            QJsonObject detection_object = detection_val.toObject();
            Detection d;
            d.x_ = detection_object.value("x").toDouble();
            d.y_ = detection_object.value("y").toDouble();
            d.valid_ = detection_object.value("valid").toBool();
            d.r_ = detection_object.value("r").toDouble();
            d.rgb_image_file_ = detection_object.value("file").toString().toStdString();

            //QJsonArray p_3d = detection_object.value("world_point").toArray();
            //d.p_ = Eigen::Vector3d(p_3d.at(0).toDouble(),
            //                       p_3d.at(1).toDouble(),
            //                       p_3d.at(2).toDouble());


            //            QJsonArray inlier_list = detection_object.value("inlier_list").toArray();
            //            for(const QJsonValue& idx_val : inlier_list){
            //                d.inlier_for_.push_back(size_t(idx_val.toInt()));
            //            }

            detections_->back().push_back(d);
        }
    }

    STATUS("Detections successfully loaded");
}
