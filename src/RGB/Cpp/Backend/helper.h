#pragma once

#include <eigen3/Eigen/Eigen>

namespace Helper{

///
/// \brief rodriguesToRotationMatrix
/// \param r
/// \return
///
Eigen::Matrix3d rodriguesToRotationMatrix(const Eigen::Vector3d& r) {
    Eigen::Matrix3d R, r_skew;
    double theta = r.norm();

    if(theta == 0.0){
        return Eigen::Matrix3d::Identity();
    }

    r_skew << 0, -r(2), r(1),
              r(2), 0, -r(0),
              -r(1), r(0), 0;

    R = Eigen::Matrix3d::Identity() +
        (sin(theta) / theta) * r_skew +
        (1 - cos(theta)) / (theta * theta) * r_skew * r_skew;

    return R;
}

///
/// \brief rotationMatrixToEulerAngles
/// \param R
/// \return
///
Eigen::Vector3d rotationMatrixToEulerAngles(const Eigen::Matrix3d& R) {
    double sy = sqrt(R(0,0) * R(0,0) + R(1,0) * R(1,0));
    bool singular = sy < 1e-6;
    double x, y, z;

    if (!singular) {
        x = atan2(R(2,1), R(2,2));
        y = atan2(-R(2,0), sy);
        z = atan2(R(1,0), R(0,0));
    } else {
        x = atan2(-R(1,2), R(1,1));
        y = atan2(-R(2,0), sy);
        z = 0;
    }
    return Eigen::Vector3d(x, y, z);
}

///
/// \brief rotationMatrixToEulerAnglesDegree
/// \param R
/// \return
///
Eigen::Vector3d rotationMatrixToEulerAnglesDegree(const Eigen::Matrix3d& R) {
    Eigen::Vector3d r_euer_rad = rotationMatrixToEulerAngles(R);
    return r_euer_rad/M_PI*180.0;
}

///
/// \brief transformCovariance
/// \param cov_r
/// \param r
/// \return
///
Eigen::Matrix3d transformCovariance(const Eigen::Matrix3d& cov_r, const Eigen::Vector3d& r) {
    Eigen::Matrix3d R = rodriguesToRotationMatrix(r);
    Eigen::Vector3d euler = rotationMatrixToEulerAngles(R);

    // Compute the Jacobian numerically (using Ceres for finite differences)
    const double delta = 1e-6;
    Eigen::Matrix3d jacobian;
    for (int i = 0; i < 3; ++i) {
        Eigen::Vector3d r_perturbed = r;
        r_perturbed(i) += delta;

        Eigen::Matrix3d R_perturbed = rodriguesToRotationMatrix(r_perturbed);
        Eigen::Vector3d euler_perturbed = rotationMatrixToEulerAngles(R_perturbed);

        jacobian.col(i) = (euler_perturbed - euler) / delta;
    }
    return jacobian * cov_r * jacobian.transpose();
}


}
