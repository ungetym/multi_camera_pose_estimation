////////////////////////////////////////////////////////////////////////////////
///                 Generate list of available RGB-D images                  ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UI/logger.h"
#include <QObject>

/// constants used for the tag system
const int CAM = 0;              /// camera tag
const int ID = -1;              /// image ID tag
const int SEP_UNDERSCORE = -2;  /// separator underscore
const int SEP_MINUS = -3;       /// separator minus
const int STRING = -4;          /// user-specified string

///
/// \brief The FileDescription struct contains the camera name and image id
///                                   extracted from a file name
///
struct FileDescription{
    std::string camera_;
    std::string ID_;
};

///
/// \brief The RGBImageDescription struct contains the camera, image set ID and
///                                        corresponding RGB image path
///
struct RGBImageDescription{

    ///
    /// \brief RGBImageDescription is the default constructor
    /// \param camera               name of the camera
    /// \param ID                   image set ID
    /// \param path_rgb             path to RGB image
    ///
    RGBImageDescription(const std::string& camera, const std::string& ID,
                         const std::string& path_rgb){
        camera_ = camera;
        ID_ = ID;
        path_rgb_ = path_rgb;
    }

    /// camera name
    std::string camera_;
    /// index of this camera in the list of cameras
    int cam_idx_ = -1;

    /// image set ID
    std::string ID_;
    /// image path
    std::string path_rgb_;
};

/// aliases for brevity
using Imagelist = std::vector<std::vector<RGBImageDescription>>;
using Tag = std::pair<int, std::string>;


///
/// \brief The Imagelist_Generator class is used to create a list of available
///                                      RGB-D images corresponding to the
///                                      user-specified file naming scheme
///
class Imagelist_Generator: public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Imagelist_Generator is the default constructor
    ///
    Imagelist_Generator();

public slots:

    ///
    /// \brief createImagelist parses the naming schemes and files available in
    ///                        the specified directory to create a list of
    ///                        images sorted by the image IDs
    /// \param image_dir       directory containing the images
    /// \param rgb_scheme      RGB file name scheme
    ///
    Imagelist createImagelist(const std::string& image_dir,
                              const QString& rgb_scheme);

private:

    ///
    /// \brief parseScheme  parses the user defined naming scheme into a vector
    ///                     of ints encoding the different file name parts
    ///                     IMPORTANT: The file names are assumed to contain the
    ///                     tags $CAM and $ID exactly one time and separated by
    ///                     at least one separator, i.e. underscore or minus
    ///
    /// \param input_string user defined naming scheme string
    /// \param scheme       resulting encoded naming scheme
    ///
    ///
    bool parseScheme(const std::string& input_string, std::vector<Tag>* scheme);

    ///
    /// \brief parseFileName    tries to separate a file name according to the
    ///                         user-specified naming scheme
    /// \param file_path        file path
    /// \param scheme           naming scheme as extracted by parseScheme(...)
    /// \param description      output FileDescription containing the camera and
    ///                         image set ID
    /// \return                 true if extraction was successful, false
    ///                         otherwise
    ///
    bool parseFileName(const std::string& file_path,
                       const std::vector<Tag>& scheme,
                       FileDescription* description);

private:
    /// tag based file naming schemes which will be extracted from the GUI
    /// strings by parseScheme(..)
    std::vector<Tag> rgb_scheme_;

signals:

    ///
    /// \brief imagelistCreated is emitted after successful creation of the list
    /// \param list
    ///
    void imagelistCreated(const Imagelist& list);

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);

};
