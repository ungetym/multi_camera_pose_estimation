////////////////////////////////////////////////////////////////////////////////
///      Different optimization methods to calculate the RGB camera poses    ///
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Backend/data_types.h"
#include "UI/logger.h"
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <QObject>

///
/// \brief The Reprojection_Error class represents the reprojection error of a
///                                     given pose and world point and is used
///                                     by the optimization to create the
///                                     residuals
///
class Reprojection_Error{

public:
    ///
    /// \brief Reprojection_Error is the default constructor
    /// \param device             for which the 3D-to-2D correspondence was
    ///                           measured
    /// \param pixel              the measured pixel position
    ///
    Reprojection_Error(const Device& device,
                       const Eigen::Vector2d& pixel) :
        device_{ device },
        pixel_{ pixel}
    { }

    ///
    /// \brief operator ()  calculates the reprojection error
    /// \param pose         transform from world to current RGB camera
    /// \param scene_point  3D world point to optimize
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const pose, const T* const scene_point,
                    T* residuals) const {

        const T* rotation = pose;
        const T* translation = pose + 3;

        // rotate world point into camera coordinates
        T p[3];
        ceres::AngleAxisRotatePoint(rotation, scene_point, p);

        // translate point
        p[0] += translation[0];
        p[1] += translation[1];
        p[2] += translation[2];

        // get the rgb camera's K matrix values
        const T fx = T(device_.rgb_.K_.at<double>(0,0));
        const T fy = T(device_.rgb_.K_.at<double>(1,1));
        const T cx = T(device_.rgb_.K_.at<double>(0,2));
        const T cy = T(device_.rgb_.K_.at<double>(1,2));

        // transfer from camera to pixel coordinates
        T x_projected = (fx * p[0] / p[2]) + cx;
        T y_projected = (fy * p[1] / p[2]) + cy;

        // calculate residuals, note: ceres squares these automatically
        residuals[0] = x_projected - T(pixel_[0]);
        residuals[1] = y_projected - T(pixel_[1]);

        if (p[2] < 0.0){
            residuals[0] *= 100.0;
            residuals[1] *= 100.0;
        }

        return true;
    }

    ///
    /// \brief create   is used to create a ceres cost function
    /// \param device   current device
    /// \param pixel    detected pixel position
    /// \return
    ///
    static ceres::CostFunction* create(const Device& device,
                                       const Eigen::Vector2d& pixel) {
        return new ceres::AutoDiffCostFunction<Reprojection_Error, 2, 6, 3>(
                    new Reprojection_Error(device, pixel));
    }

protected:
    const Device device_;
    const Eigen::Vector2d pixel_;
};

///
/// \brief The Reprojection_Error_Fixed_Pose class is the pendant to the class
///                                                above, but for the first
///                                                camera which is assumed to be
///                                                located at the world
///                                                coordinate center with zero
///                                                rotation
///
class Reprojection_Error_Fixed_Pose{

public:
    ///
    /// \brief Reprojection_Error_Fixed_Pose is the default constructor
    /// \param device
    /// \param pixel
    ///
    Reprojection_Error_Fixed_Pose(const Device& device,
                                  const Eigen::Vector2d& pixel) :
        device_{ device },
        pixel_{ pixel}
    {

        R_ = {0.0,0.0,0.0};
        ceres::RotationMatrixToAngleAxis<double>(
                    static_cast<const double*> (device.world_pose_.R_.data()),
                    R_.data());
        t_ = {device.world_pose_.t_.x(),
              device.world_pose_.t_.y(),
              device.world_pose_.t_.z()};
    }

    ///
    /// \brief operator () calculates the reprojection error
    /// \param scene_point           3D world point to optimize
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const scene_point, T* residuals) const {

        T rotation[3];
        rotation[0]= T(R_[0]);
        rotation[1]= T(R_[1]);
        rotation[2]= T(R_[2]);

        T translation[3];
        translation[0] = T(t_[0]);
        translation[1] = T(t_[1]);
        translation[2] = T(t_[2]);

        // rotate world point into camera coordinates
        T p[3];
        ceres::AngleAxisRotatePoint(rotation, scene_point, p);

        // translate point
        p[0] += translation[0];
        p[1] += translation[1];
        p[2] += translation[2];

        // get the rgb camera's K matrix values
        const T fx = T(device_.rgb_.K_.at<double>(0,0));
        const T fy = T(device_.rgb_.K_.at<double>(1,1));
        const T cx = T(device_.rgb_.K_.at<double>(0,2));
        const T cy = T(device_.rgb_.K_.at<double>(1,2));

        // transfer from camera to pixel coordinates
        T x_projected = (fx * p[0] / p[2]) + cx;
        T y_projected = (fy * p[1] / p[2]) + cy;

        // calculate residuals, note: ceres squares these automatically
        residuals[0] = x_projected - T(pixel_[0]);
        residuals[1] = y_projected - T(pixel_[1]);

        return true;
    }

    ///
    /// \brief create       is used to create a ceres cost function
    /// \param device       current camera device
    /// \param pixel        detected pixel position
    /// \return
    ///
    static ceres::CostFunction* create(const Device& device,
                                       const Eigen::Vector2d& pixel) {
        return new ceres::AutoDiffCostFunction<Reprojection_Error_Fixed_Pose, 2, 3>(
                    new Reprojection_Error_Fixed_Pose(device, pixel));
    }

protected:
    const Device device_;
    const Eigen::Vector2d pixel_;

    std::vector<double> R_;
    std::vector<double> t_;
};

///
/// \brief The Reprojection_Error_Fixed_Scene class
///
class Reprojection_Error_Fixed_Scene{

public:
    ///
    /// \brief Reprojection_Error_Fixed_Scene is the default constructor
    /// \param device
    /// \param scene_point
    /// \param pixel
    ///
    Reprojection_Error_Fixed_Scene(const Device& device,
                                   const Eigen::Vector3d& scene_point,
                                   const Eigen::Vector2d& pixel) :
        device_{ device },
        scene_point_{ scene_point },
        pixel_{ pixel }
    {
    }

    ///
    /// \brief operator ()  calculates the reprojection error
    /// \param pose         transform from world to current RGB camera
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const pose,
                    T* residuals) const {

        const T* rotation = pose;
        const T* translation = pose + 3;

        // Rotate world point into camera coordinates
        T p[3];
        T scene_point[3];
        scene_point[0] = T(scene_point_.x());
        scene_point[1] = T(scene_point_.y());
        scene_point[2] = T(scene_point_.z());
        ceres::AngleAxisRotatePoint(rotation, scene_point, p);

        // Translate point
        p[0] += translation[0];
        p[1] += translation[1];
        p[2] += translation[2];

        // Get the rgb camera's K matrix values
        const T fx = T(device_.rgb_.K_.at<double>(0,0));
        const T fy = T(device_.rgb_.K_.at<double>(1,1));
        const T cx = T(device_.rgb_.K_.at<double>(0,2));
        const T cy = T(device_.rgb_.K_.at<double>(1,2));

        // Transfer from camera to pixel coordinates
        T x_projected = (fx * p[0] / p[2]) + cx;
        T y_projected = (fy * p[1] / p[2]) + cy;

        // Calculate residuals, note: ceres squares these automatically
        residuals[0] = x_projected - T(pixel_[0]);
        residuals[1] = y_projected - T(pixel_[1]);

        return true;
    }

    ///
    /// \brief create       is used to create a ceres cost function
    /// \param device       current device
    /// \param scene_point
    /// \param pixel        detected pixel position
    /// \return
    ///
    static ceres::CostFunction* create(const Device& device,
                                       const Eigen::Vector3d& scene_point,
                                       const Eigen::Vector2d& pixel) {
        return new ceres::AutoDiffCostFunction<Reprojection_Error_Fixed_Scene, 2, 6>(
                    new Reprojection_Error_Fixed_Scene(device, scene_point, pixel));
    }

protected:
    const Device device_;
    const Eigen::Vector3d scene_point_;
    const Eigen::Vector2d pixel_;
};

///
/// \brief The Optimizer class contains different optimization methods for
///                            finding the RGB camera poses
///
class Optimizer : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Optimizer is the default constructor
    ///
    Optimizer();

    ///
    /// \brief getPropertiesInitial
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getPropertiesInitial(){return property_list_initial_;}

    ///
    /// \brief getPropertiesLM
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getPropertiesLM(){return property_list_LM_;}

    ///
    /// \brief getPropertiesEval
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getPropertiesEval(){return property_list_eval_;}

public slots:

    ///
    /// \brief initialPoseEstimation via RANSAC and SVD
    /// \param devices          list of available RGB-D devices to which the
    ///                         poses will be saved
    /// \param detections       detected RGB-D and 3D positions of the table
    ///                         tennis ball
    /// \param max_iterations   for the RANSAC loop
    /// \param inlier_threshold in mm to accept a detection as RANSAC inlier
    ///
    void initialPoseEstimation(std::vector<Device>& devices,
                               const std::shared_ptr<Detections>& detections,
                               const bool &reinitialize = false);

    ///
    /// \brief bundleAdjustment starts a non-linear optimization
    ///                         (Levenberg-Marquardt) with the results of the
    ///                         initial pose estimation
    /// \param devices          list of available RGB-D devices to which the
    ///                         poses will be saved
    /// \param detections       detected RGB-D and 3D positions of the table
    ///                         tennis ball
    ///
    void bundleAdjustment(std::vector<Device>& devices,
                          const std::shared_ptr<Detections>& detections);

private:

    ///
    /// \brief triangulate
    /// \param pixel_1
    /// \param pixel_2
    /// \param P_1
    /// \param P_2
    /// \return
    ///
    Eigen::Vector3d triangulate(const Eigen::Vector2d& pixel_1,
                                const Eigen::Vector2d& pixel_2,
                                const Eigen::MatrixXd& P_1,
                                const Eigen::MatrixXd& P_2);

private:

    /// RANSAC properties
    std::shared_ptr<Property_Spin_F> ransac_probability_ = nullptr;
    std::shared_ptr<Property_Spin_F> ransac_threshold_ = nullptr;
    std::vector<std::shared_ptr<Property>> property_list_initial_;

    /// LM properties
    std::shared_ptr<Property_Spin> LM_max_iter_ = nullptr;
    std::shared_ptr<Property_Spin_F> LM_change_threshold_ = nullptr;
    std::vector<std::shared_ptr<Property>> property_list_LM_;

    /// Evaluation properties
    std::shared_ptr<Property_Check> eval_calc_variances_ = nullptr;
    std::vector<std::shared_ptr<Property>> property_list_eval_;

signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

    ///
    /// \brief logMatEig is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMatEig(const QString& msg, const Eigen::MatrixXd& mat);

    ///
    /// \brief logVecVar
    /// \param msg
    /// \param vec
    /// \param var
    ///
    void logVecVar(const QString& msg, const Eigen::VectorXd& vec, const Eigen::VectorXd& var);

    ///
    /// \brief progress
    /// \param val
    ///
    void progress(const float& val);

    ///
    /// \brief initialPosesCalculated is emitted upon finishing the initial pose
    ///                               calculation to notify the main window
    ///                               class, that the poses are available and
    ///                               can be used for the next optimization step
    ///
    void initialPosesCalculated();

    ///
    /// \brief done
    /// \param scene_points
    ///
    void done(const std::vector<std::vector<double>>& scene_points);
};
