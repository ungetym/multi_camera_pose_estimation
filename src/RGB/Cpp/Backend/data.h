////////////////////////////////////////////////////////////////////////////////
///     The data class holds all relevant data and subprogram instances      ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Backend/detector.h>
#include <Backend/imagelist_generator.h>
#include <Backend/optimizer.h>
#include <UI/logger.h>
#include <QObject>
#include <QThread>

///
/// \brief The Data class holds all relevant data such as detections and device
///                       parameters. Furthermore, it contains pointers to the
///                       instances of the optimizer and detector and handles
///                       IO, i.e. the parsing and writing of camera parameter
///                       files.
///
class Data : public QObject{

    Q_OBJECT

public:
    ///
    /// \brief Data is the default constructor
    ///
    Data();

    /// destructor
    ~Data(){saveUISettings(property_list_);}

    ///
    /// \brief saveUISettings is used to save the GUI settings to a json file
    ///
    /// \param settings
    /// \param ask_user
    /// \return
    ///
    bool saveUISettings(const std::vector<std::shared_ptr<Property>>& settings,
                        const bool& ask_user = false);

    ///
    /// \brief loadData is used to load the GUI data from a json file
    ///
    bool loadUISettings(const std::vector<std::shared_ptr<Property>>& settings,
                        const bool& ask_user = false);

    ///
    /// \brief parseCameraNameScheme reads the user-specified cam naming scheme
    ///                              given as a string and divides it into a
    ///                              sequence of tag types which can later be
    ///                              used to associate file names with cameras.
    ///                              IMPORTANT: The tag $CAM must be used
    ///                              exactly once in every camera name scheme!
    /// \param input_string          the user-specified scheme as string
    /// \param scheme                the resulting list of tags
    /// \return                      true if valid scheme, false otherwise
    ///
    bool parseCameraNameScheme(const std::string& input_string,
                               std::vector<Tag>* scheme);

    ///
    /// \brief loadCalibration examines the calibration file path(s) and decides
    ///                        which of the functions below should be used for
    ///                        for loading the cameras' intrinsics
    ///
    bool loadCalibration();

    ///
    /// \brief parseMCCCalibration reads the device/camera parameters from an M
    ///                            CC_Cams file as produced by the Multi-Camera
    ///                            Calibration tool available here:
    ///                      https://gitlab.com/ungetym/Multi_Camera_Calibration
    ///
    bool parseMCCCalibration();

    ///
    /// \brief loadJSONCalibration reads the device/camera parameters from a
    ///                            json file as produced by the Guided Camera
    ///                            Calibration Tool available here:
    ///                     https://gitlab.com/ungetym/guided_camera_calibration
    ///
    bool loadJSONCalibration(const QString& file_path, std::vector<Device>* devices);

    ///
    /// \brief saveCalibration
    /// \param file_path
    ///
    void saveCalibration(const std::string& file_path);

    ///
    /// \brief saveMCCCalibration is the output pendant to parseCalibration
    /// \param file_path
    ///
    void saveMCCCalibration(const std::string& file_path);

    ///
    /// \brief saveJSONCalibration
    /// \param file_path
    ///
    void saveJSONCalibration(const std::string& file_path);

    ///
    /// \brief saveDetections
    /// \param file_path
    ///
    void saveDetections(const std::string& file_path);

    ///
    /// \brief loadDetections
    /// \param file_path
    ///
    void loadDetections(const std::string& file_path);

public:

    std::vector<std::shared_ptr<Property>> property_list_;

    /// Directory containing the RGB images
    std::shared_ptr<Property_DirSelector> image_dir_ = nullptr;
    /// Image naming scheme - Tags: $CAM and $ID  Separators: - and _
    /// Everything else should be a constant string
    std::shared_ptr<Property_LineEdit> image_name_scheme_ = nullptr;

    /// Path(s) to the file/dir containing the intrinsic parameters for all
    /// devices
    std::shared_ptr<Property_Check> intrinsics_within_single_file_ = nullptr;
    std::shared_ptr<Property_FileSelector> intrinsics_file_path_ = nullptr;
    std::shared_ptr<Property_DirSelector> intrinsics_folder_path_ = nullptr;

    /// Naming schemes for the camera within the calibration files. Must contain
    /// the tag $CAM and can contain further constant strings
    std::shared_ptr<Property_LineEdit> camera_name_scheme_ = nullptr;

    /// Switch to turn on advanced options for the detection and calibration
    /// process
    std::shared_ptr<Property_Check> advanced_options_ = nullptr;

    /// List of available RGBD devices
    std::vector<Device> devices_;

    /// Pointer to an instance of the imagelist generator
    std::shared_ptr<Imagelist_Generator> imagelist_gen_ = nullptr;

    /// Pointer to an instance of the rgb table tennis ball detector
    std::shared_ptr<Detector> detector_ = nullptr;

    /// Pointer to the list of detections returned by the detector
    std::shared_ptr<Detections> detections_ = nullptr;

    /// Pointer to the optimizer instance used for the initial pose estimation
    /// and the final bundle adjustment
    std::shared_ptr<Optimizer> optimizer_ = nullptr;

    /// Threads for the different program components
    QThread* detector_thread_ = nullptr;
    QThread* optimizer_thread_ = nullptr;

signals:
    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

};
