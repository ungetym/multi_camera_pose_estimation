#include "imagelist_generator.h"
#include <QDir>

Imagelist_Generator::Imagelist_Generator(){}

Imagelist Imagelist_Generator::createImagelist(const std::string& image_dir,
                                               const QString& rgb_scheme){
    Imagelist result;

    // Parse the different naming schemes
    if(!parseScheme(rgb_scheme.toStdString(),&rgb_scheme_)){
        ERROR("RGB image file scheme could not be parsed.");
        return result;
    }

    // Get a list of image files in the specified directory or subdirectories
    QDir dir(QString::fromStdString(image_dir));
    if(!dir.exists()){
        ERROR("Image dir does not exist.");
        return result;
    }
    QStringList file_list = dir.entryList({"*.png","*.jpeg","*.jpg","*.bmp","*.tif","*.tiff"});

    QStringList sub_dirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for(const QString &sub_dir_name : sub_dirs) {
        QDir sub_dir(dir.absoluteFilePath(sub_dir_name));
        QStringList sub_file_list = sub_dir.entryList({"*.png","*.jpeg","*.jpg","*.bmp","*.tif","*.tiff"});
        for(const QString& file_name : sub_file_list){
            file_list.push_back(sub_dir_name+"/"+file_name);
        }
    }

    // Extract camera and ID from file name
    for(const QString& file : file_list){
        std::string full_path = image_dir+"/"+file.toStdString();
        FileDescription description;
        parseFileName(file.toStdString(), rgb_scheme_, &description);

        // search for image ID and camera
        size_t ID_idx_found = -1;
        size_t cam_idx_found = -1;
        for(size_t ID_idx = 0; ID_idx < result.size(); ID_idx++){
            for(size_t cam_idx = 0; cam_idx < result[ID_idx].size(); cam_idx++){
                if(result[ID_idx][cam_idx].ID_.compare(description.ID_) != 0){
                    break;
                }
                else{
                    ID_idx_found = ID_idx;
                    if(result[ID_idx][cam_idx].camera_.compare(description.camera_) == 0){
                        cam_idx_found = cam_idx;
                    }
                }
            }
        }

        // depending on if the camera and ID have been found, create a new entry
        // or modify an existing one
        if(ID_idx_found == -1){
            result.push_back(
                        std::vector<RGBImageDescription>(1,
                                                         RGBImageDescription (description.camera_,
                                                                              description.ID_,
                                                                              full_path)));

        }
        else{
            if(cam_idx_found == -1){
                result[ID_idx_found].push_back(RGBImageDescription (
                                                   description.camera_,
                                                   description.ID_,
                                                   full_path));

            }
            else{

                result[ID_idx_found][cam_idx_found].path_rgb_ =
                        full_path;

            }
        }
    }

    return result;
}

bool Imagelist_Generator::parseScheme(const std::string& input_string,
                                      std::vector<Tag> *scheme){
    scheme->clear();

    int last = -1;
    for(size_t i = 0; i< input_string.size(); i++){
        if(input_string[i] == '$'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }

            if(input_string.substr(i,4).compare("$CAM") == 0){
                scheme->push_back(Tag(CAM,"$CAM"));
                i += 3;
                last = i;
            }
            else if(input_string.substr(i,3).compare("$ID") == 0){
                scheme->push_back(Tag(ID,"$ID"));
                i += 2;
                last = i;
            }
            else{
                ERROR("Only the tags $CAM and $ID are allowed");
                scheme->clear();
                return false;
            }
        }
        else if(input_string[i] == '_'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }
            last = i;
            scheme->push_back(Tag(SEP_UNDERSCORE,"_"));
        }
        else if(input_string[i] == '-'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }
            last = i;
            scheme->push_back(Tag(SEP_MINUS,"-"));
        }
        else if(input_string[i] == '.'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }
            last = i;
        }
    }

    return true;
}

bool Imagelist_Generator::parseFileName(const std::string& file_path,
                                        const std::vector<Tag>& scheme,
                                        FileDescription* description){

    std::string file = file_path;
    if(file_path.find_last_of("/") != std::string::npos){
        file = file_path.substr(file_path.find_last_of("/")+1);
    }

    int last = -1;
    for(size_t i = 0; i < scheme.size(); i++){
        if(scheme[i].first == CAM){
            if(i == scheme.size() - 1){ // extract final part of the string
                description->camera_ = file.substr(last + 1,
                                                   file.find(".") - last - 1);
            }
            else{
                int next = file.find(scheme[i + 1].second, last+1);
                if(next == std::string::npos){
                    WARN("File name of " + QString::fromStdString(file)
                         + " could not be parsed");
                    return false;
                }
                else{
                    description->camera_ = file.substr(last + 1,
                                                       next - last - 1);
                    last = next-1;
                }
            }
        }
        else if(scheme[i].first == ID){
            if(i == scheme.size() - 1){ // extract final part of the string
                description->ID_ = file.substr(last + 1,
                                               file.find(".") - last - 1);
            }
            else{
                int next = file.find(scheme[i + 1].second, last+1);
                if(next == std::string::npos){
                    WARN("File name of " + QString::fromStdString(file)
                         + " could not be parsed");
                    return false;
                }
                else{
                    description->ID_ = file.substr(last + 1, next - last - 1);
                    last = next-1;
                }
            }
        }
        else if(scheme[i].first == SEP_MINUS ||
                scheme[i].first == SEP_UNDERSCORE){
            last++;
        }
        else{// check if string matches scheme string
            int str_size = scheme[i].second.size();
            if(file.substr(last+1, str_size).compare(scheme[i].second) != 0){
                return false;
            }
            last += str_size;
        }
    }

    return true;
}
