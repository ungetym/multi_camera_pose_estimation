clear all;

%% Setup

% enable to show detections
debug = false;
debug_epi = false;
debug_corresponding_depth = false;
debug_3D = false;

% radius of the table tennis ball in mm
ball_radius = 20;

% path to image folder
data_folder = '/home/anonym/Desktop/Ant_Media/tabletennisball2/';

% create list of rgb images
rgb_image_paths = dir(fullfile(data_folder, '*rgb.png'));
num_imgs_per_cam = size(rgb_image_paths, 1) / 3;

% create list of depth images
depth_image_paths = dir(fullfile(data_folder, '*depth.png'));

%% Detect correspondences

% find corresponding pixels for the different cameras
correspondences = [];

for i = 1:num_imgs_per_cam
    img_cam_1 = imread(strcat(data_folder, rgb_image_paths(i).name));
    [x_1,y_1,r_1] = detectBallHSV(img_cam_1);
    img_cam_2 = imread(strcat(data_folder, rgb_image_paths(num_imgs_per_cam + i).name));
    [x_2,y_2,r_2] = detectBallHSV(img_cam_2);
    img_cam_3 = imread(strcat(data_folder, rgb_image_paths(2 * num_imgs_per_cam + i).name));
    [x_3,y_3,r_3] = detectBallHSV(img_cam_3);

    
    if r_1 * r_2 * r_3 > 0

        correspondences = [correspondences; i, [x_1,y_1,r_1], ...
            [x_2,y_2,r_2], [x_3,y_3,r_3]];

        % for debugging: show detected ball
        if debug
            figure(1);
            subplot(2,2,1);
            debug_img_1 = insertMarker(img_cam_1, [x_1,y_1], ...
                "circle","Size", round(r_1));
            debug_img_1 = insertMarker(debug_img_1, [x_1,y_1], ...
                "circle","Size", round(r_1)+1);
            imshow(debug_img_1);
            subplot(2,2,2);
            debug_img_2 = insertMarker(img_cam_2, [x_2,y_2], ...
                "circle","Size", round(r_2));
            debug_img_2 = insertMarker(debug_img_2, [x_2,y_2], ...
                "circle","Size", round(r_2)+1);
            imshow(debug_img_2);
            subplot(2,2,3);
            debug_img_3 = insertMarker(img_cam_3, [x_3,y_3], ...
                "circle","Size", round(r_3));
            debug_img_3 = insertMarker(debug_img_3, [x_3,y_3], ...
                "circle","Size", round(r_3)+1);
            imshow(debug_img_3);
        end
    else
        if debug
            figure(2);
            if ~(r_1 > 0)
                fprintf('Unable to detect ball in %s \n', ...
                    rgb_image_paths(i).name);
                imshow(img_cam_1);
            elseif ~(r_2 > 0)
                fprintf('Unable to detect ball in %s \n', ...
                    rgb_image_paths(num_imgs_per_cam + i).name);
                imshow(img_cam_2);
            else
                fprintf('Unable to detect ball in %s \n', ...
                    rgb_image_paths(2 * num_imgs_per_cam + i).name);
                imshow(img_cam_3);
            end
        end
    end

    if debug
        waitforkey();
    end
end
clearvars img_cam_1 img_cam_2 img_cam_3 x_1 x_2 x_3 y_1 y_2 y_3 r_1 r_2 r_3;

%% Load device calibrations

dev_params_file = append(data_folder,'final_all_new.MCC_Cams');
dev_params = parseMCCfile(dev_params_file);

% get indices for rgb and ir cameras
rgb_camera_prefix = 'kinect';
ir_camera_prefix = 'irkinect';
rgb_camera_indices = [];
ir_camera_indices = [];
for i = 1:size(dev_params,1)
    name = dev_params{i,1};
    if strcmp(name(1:size(rgb_camera_prefix,2)), rgb_camera_prefix)
        rgb_camera_indices = [rgb_camera_indices, i];
        % search for corresponding ir camera
        for j = 1:size(dev_params,1)
            name_ir = dev_params{j,1};
            if size(name_ir,2) >= size(ir_camera_prefix,2)
                if strcmp(name_ir(1:size(ir_camera_prefix,2)), ir_camera_prefix) && strcmp(name_ir(end), name(end))
                    ir_camera_indices = [ir_camera_indices, j];
                end
            end
        end
    end
end

% calculate undistortion maps
rgb_maps = {};
ir_maps = {};

for dev_idx = 1:size(rgb_camera_indices,2)

    rgb_idx = rgb_camera_indices(dev_idx);
    ir_idx = ir_camera_indices(dev_idx);

    [map_x, map_y] = calcBackwardUndistortMap(dev_params{ir_idx,2}, ...
        dev_params{ir_idx,3}, dev_params{ir_idx,5}, ...
        dev_params{ir_idx,4}, dev_params{ir_idx,5});
    ir_maps{dev_idx} = {map_x, map_y};
    [map_x, map_y] = calcBackwardUndistortMap(dev_params{rgb_idx,2}, ...
        dev_params{rgb_idx,3}, dev_params{rgb_idx,5}, ...
        dev_params{rgb_idx,4}, dev_params{rgb_idx,5});
    rgb_maps{dev_idx} = {map_x, map_y};
end
clearvars map_x map_y;

%% Undistort detections
for i = 1:size(correspondences,1)
    for j = 1:3
        pixel = correspondences(i,2 + 3*(j-1) : 3 + 3*(j-1));
        % search backward undistortion map for pixel coordinates
        pos_x = round(pixel(1));
        pos_y = round(pixel(2));

        while (sign(rgb_maps{j}{1}(pos_y,pos_x) - pixel(1)) == ...
                sign(rgb_maps{j}{1}(pos_y,pos_x+1) - pixel(1))) || ...
                (sign(rgb_maps{j}{2}(pos_y,pos_x) - pixel(2)) == ...
                sign(rgb_maps{j}{2}(pos_y+1,pos_x) - pixel(2)))
            % x search
            while (sign(rgb_maps{j}{1}(pos_y,pos_x) - pixel(1)) == ...
                    sign(rgb_maps{j}{1}(pos_y,pos_x+1) - pixel(1)))
                pos_x = pos_x - sign(rgb_maps{j}{1}(pos_y,pos_x) ...
                    - pixel(1)) * sign(rgb_maps{j}{1}(pos_y,pos_x+1) ...
                    - rgb_maps{j}{1}(pos_y,pos_x));
            end

            while (sign(rgb_maps{j}{2}(pos_y,pos_x) - pixel(2)) == ...
                    sign(rgb_maps{j}{2}(pos_y+1,pos_x) - pixel(2)))
                pos_y = pos_y - sign(rgb_maps{j}{2}(pos_y,pos_x) ...
                    - pixel(2)) * sign(rgb_maps{j}{2}(pos_y+1,pos_x) ...
                    - rgb_maps{j}{2}(pos_y,pos_x));
            end
        end
        
        % bilinear interpolation
        ul = [rgb_maps{j}{1}(pos_y,pos_x), rgb_maps{j}{2}(pos_y,pos_x)];
        ur = [rgb_maps{j}{1}(pos_y,pos_x+1), rgb_maps{j}{2}(pos_y,pos_x+1)];
        bl = [rgb_maps{j}{1}(pos_y+1,pos_x), rgb_maps{j}{2}(pos_y+1,pos_x)];
        br = [rgb_maps{j}{1}(pos_y+1,pos_x+1), rgb_maps{j}{2}(pos_y+1,pos_x+1)];

        pos_x_inter = pos_x + (pixel(1) - (ul(1)+bl(1))/2) / ...
            ((ur(1)+br(1))/2 - (ul(1)+bl(1))/2);
        pos_y_inter = pos_y + (pixel(2) - (ul(2)+ur(2))/2) / ...
            ((bl(2)+br(2))/2 - (ul(2)+ur(2))/2);
        
        correspondences(i,2 + 3*(j-1) : 3 + 3*(j-1)) = ...
            [pos_x_inter,pos_y_inter];

    end
end
clearvars pos_x pos_y pos_x_inter pos_y_inter ul ur bl br;

%% Find matching depth values

correspondences = [correspondences, ...
    zeros(size(correspondences,1),9)];

% calculate fundamental matrices
F = {};
for dev_idx = 1:3

    rgb_idx = rgb_camera_indices(dev_idx);
    ir_idx = ir_camera_indices(dev_idx);

    pose_rgb_to_depth = dev_params{ir_idx,6} * inv(dev_params{rgb_idx,6});
    R = pose_rgb_to_depth(1:3,1:3);
    t = pose_rgb_to_depth(1:3,4);
    E = R * [0 -t(3) t(2); t(3) 0 -t(1); -t(2) t(1) 0];

    K_rgb = dev_params{rgb_idx,5};
    K_ir = dev_params{ir_idx,5};
    F{dev_idx} = inv(K_ir.') * E * inv(K_rgb);
end
clearvars dev_idx pose_rgb_to_depth R t E K_rgb K_ir;

% search along epipolar lines for match
for i = 1:size(correspondences,1)
    img_idx = correspondences(i,1);

    for j = 1:3

        rgb_idx = rgb_camera_indices(j);
        ir_idx = ir_camera_indices(j);

        % undistort depth image
        depth_img = imread(strcat(data_folder, ...
            depth_image_paths(img_idx + (j-1) * num_imgs_per_cam).name));
        depth_img_rectified = double(undistortImage(depth_img, ir_maps{j}{1}, ...
            ir_maps{j}{2}, "nearest"));
        
        % map from depth to rgb
        pose_depth_to_rgb = dev_params{rgb_idx,6} * inv(dev_params{ir_idx,6});
        mapped_coordinates = mapDepthToRGB(depth_img_rectified, ...
            dev_params{ir_idx,5}, dev_params{rgb_idx,5}, pose_depth_to_rgb);

        % calculate epipolar line
        pixel = correspondences(i,2 + 3*(j-1) : 3 + 3*(j-1));
        epi = F{j} * [pixel(1); pixel(2); 1];


        if debug_epi
            % undistort rgb image
            rgb_img = imread(strcat(data_folder, ...
                rgb_image_paths(img_idx + (j-1) * 50).name));
            rgb_img_rectified = undistortImage(rgb_img, rgb_maps{j}{1}, ...
                rgb_maps{j}{2}, "nearest");
            % show undistorted pixel on undistorted image
            figure(3);
            debug_rgb_img_rectified = insertMarker(rgb_img_rectified, [pixel(1),pixel(2)], ...
                    "circle","Size", 3);
            imshow(debug_rgb_img_rectified);
            % show corresponding epipolar line on rectified depth image
            figure(4);
            imshow(depth_img_rectified);
            line([0 size(depth_img_rectified,2)], [-epi(3)/epi(2)...
                (-epi(3)-epi(1)*size(depth_img_rectified,2))/epi(2)]);
        
            waitforkey();
        end

        % check projected coordinates along epipolar line
        best_dist = 3;
        best_depth = -1;
        best_pixel = [-1,-1];
        for col = 1:size(mapped_coordinates,2)
            row = round((-epi(3)-epi(1)*col)/epi(2));

            dist = norm(squeeze(mapped_coordinates(row,col,:)) - pixel.');
            if dist < best_dist
                best_dist = dist;
                best_depth = depth_img_rectified(row,col) + ball_radius;
                best_pixel = [col, row];
            end
        end
        clearvars col row dist;

        % calculate 3D point in rgb camera coordinates
        p = [0,0,best_depth];
        if best_depth ~= -1
            K_ir = dev_params{ir_idx,5};
            p = inv(K_ir) * [best_pixel,1]';
            p = best_depth * p/p(3);

            p = pose_depth_to_rgb * [p; 1];
            p = p(1:3)/p(4);
        
        end
        correspondences(i, 11+(j-1)*3 : 13+(j-1)*3) = p;
            


        if debug_corresponding_depth
            if best_depth > -1
                % show undistorted pixel on undistorted image
                figure(5);
                imshow(depth_img_rectified);
                line(best_pixel(1),best_pixel(2),'Color','red','LineWidth',3,'Marker','o');
                waitforkey();
            end
        end

    end
end
clearvars i j p K_ir pose_depth_to_rgb pixel epi depth_img depth_img_rectified best_dist best_depth best_pixel;

% filter out correspondences without depth values
rows_to_delete = any(correspondences(:,11:19) == -1, 2);
correspondences(rows_to_delete,:) = [];
clearvars rows_to_delete;

%% RANSAC for initial pose estimation
max_iter = 500;
inlier_threshold = 100;
num_correspondences = size(correspondences, 1);
initial_poses = {};
inlier_sets = {};

for dev_idx = 2:3
    % calculate rigid pose from device j to device j-1
    best_inlier_set = [];

    for iter = 1:max_iter
        % randomly select 3 correspondence indices
        idx = [0, 0, 0];
        idx(1) = randi(num_correspondences);
        idx(2) = randi(num_correspondences);
        while idx(2) == idx(1)
            idx(2) = randi(num_correspondences);
        end
        idx(3) = randi(num_correspondences);
        while idx(3) == idx(1) || idx(3) == idx(2)
            idx(3) = randi(num_correspondences);
        end
        
        [R,t] = calculateRigidTransform(correspondences, idx, dev_idx);

        % calculate inlier set
        inlier_set = [];
        for i = 1:num_correspondences
            p_src = correspondences(i, ...
                11 + (dev_idx-2)*3: 13 + (dev_idx-2)*3);
            p_tgt = correspondences(i, ...
                11 + (dev_idx-1)*3: 13 + (dev_idx-1)*3);

            dist = R * p_src.' + t - p_tgt.';
            dist = dot(dist,dist);
            
            if dist < inlier_threshold
                inlier_set = [inlier_set, i];
            end

        end

        if size(inlier_set, 2) > size(best_inlier_set, 2)
            best_inlier_set = inlier_set;
        end

    end

    % calculate transform based on best inlier set
    [R,t] = calculateRigidTransform(correspondences, best_inlier_set, dev_idx);
    initial_poses{dev_idx - 1} = {R,t};
    inlier_sets{dev_idx - 1} = best_inlier_set;
end

clearvars dev_idx dist i idx img_idx inlier_set inlier_threshold ir_idx...
    iter max_iter p_src p_tgt R rgb_idx t best_inlier_set;

%% Bundle adjustment for finding the poses

% prepare initial poses table
ViewId = uint32([1; 2; 3]);
% AbsolutePose = [rigid3d(eye(3,3),[0,0,0]); ...
%     rigid3d(initial_poses{1}{1}',-(initial_poses{1}{1}' * initial_poses{1}{2}).'); ...
%     rigid3d(initial_poses{1}{1}'*initial_poses{2}{1}',...
%     (initial_poses{1}{1}' * ( -initial_poses{2}{1}'*initial_poses{2}{2} - initial_poses{1}{2})).')];

AbsolutePose = [rigid3d(eye(3,3),[0,0,0]); ...
    rigid3d(initial_poses{1}{1},initial_poses{1}{2}.'); ...
    rigid3d(initial_poses{2}{1}*initial_poses{1}{1},...
    (initial_poses{2}{1} * initial_poses{1}{2} + initial_poses{2}{2}).')];
for i = 1:3 %invert
    AbsolutePose(i) = rigid3d(AbsolutePose(i).Rotation',...
        - (AbsolutePose(i).Rotation' * AbsolutePose(i).Translation')');

end

camera_poses = table(ViewId, AbsolutePose);

% prepare 3D-to-2D correspondences
scene_points = [];

all_inliers = union(inlier_sets{1}, inlier_sets{2});
view_IDs = {};
for i = 1:size(all_inliers, 2)
    correspondence_idx = all_inliers(i);

    % check, for which cameras the current correspondence is considered an
    % inlier
    current_view_IDs = [];
    for dev_idx = 1:size(inlier_sets,2)
        if ismember(correspondence_idx, inlier_sets{dev_idx})
            current_view_IDs = union(current_view_IDs, ...
                [uint32(dev_idx), uint32(dev_idx+1)]);
        end
    end
    view_IDs{i} = current_view_IDs;

    % add scene point in world/first camera coordinates
    min_view = min(view_IDs{i});
    p = correspondences(all_inliers(i), 8 + min_view * 3:10 + min_view * 3);
    %scene_points = [scene_points; ...
    %    (AbsolutePose(min_view).Rotation' * (p' - AbsolutePose(min_view).Translation'))'];
    scene_points = [scene_points; ...
        (AbsolutePose(min_view).Rotation * p' + AbsolutePose(min_view).Translation')'];
end

point_tracks = [];
for i = 1:size(all_inliers,2)
    points_2D = [];
    for j = 1:size(view_IDs{i}, 1)
        dev_idx = view_IDs{i}(j);
        points_2D = [points_2D; correspondences(all_inliers(i), ...
            2 + (dev_idx-1) * 3 : 3 + (dev_idx-1) * 3)];
    end
    point_tracks = [point_tracks, pointTrack(view_IDs{i}, points_2D)];
end


% prepare camera intrinsics structs
camera_intrinsics = [];
for dev_idx = 1:3
    rgb_idx = rgb_camera_indices(dev_idx);
    K = dev_params{rgb_idx, 5};
    camera_intrinsics = [camera_intrinsics, ...
        cameraIntrinsics([K(1,1), K(2,2)], [K(1,3), K(2,3)],...
        [dev_params{rgb_idx, 3},dev_params{rgb_idx, 2}])];
end 

% start bundle adjustment
[scene_points_optimized, poses_optimized, errors] = bundleAdjustment( ...
    scene_points, point_tracks, camera_poses, camera_intrinsics, ...
    FixedViewIDs=1, PointsUndistorted=true, Verbose=true, ...
    RelativeTolerance=0, MaxIterations=1000);

