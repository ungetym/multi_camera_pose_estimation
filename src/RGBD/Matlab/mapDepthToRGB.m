% 
% Description:
%   The function mapDepthToRGB calculates 3D coordinates from a rectified
%   depth image and projects these into a different camera.
%
% Inputs:
%   depth_img   -   Depth image to detect the table tennis ball in
%   K_depth     -   K matrix of the depth camera
%   K_rgb       -   K matrix of the rgb camera
%   pose        -   Transform from depth to rgb camera
%
% Outputs:
%   mapped_coordinates  -   At position (x,y) this map contains the pixel
%                           coordinates of the corresponding 3D point
%                           projected to the rgb camera
%

function mapped_coordinates = mapDepthToRGB(depth_img, K_depth, K_rgb, pose)

    [target_res_x, target_res_y] = size(depth_img);
    [X,Y] = meshgrid(1:target_res_x, 1:target_res_y);
    
    % apply inverse of depth K matrix
    f_x = K_depth(1,1);
    f_y = K_depth(2,2);
    c_x = K_depth(1,3);
    c_y = K_depth(2,3);
    X = (X - c_x) ./ f_x;
    Y = (Y - c_y) ./ f_y;
    
    % map into 3D space based on depth values
    pcl_data = ones(size(depth_img,1), size(depth_img,2), 4);
    pcl_data(:,:,1) = X .* depth_img;
    pcl_data(:,:,2) = Y .* depth_img;
    pcl_data(:,:,3) = depth_img;

    % transform into camera coordinate system
    pcl_data = reshape(pcl_data, size(depth_img,1) * size(depth_img,2), 4)';
    pcl_data = pose * pcl_data;

    % apply K matrix of rgb camera
    pcl_data = K_rgb * (pcl_data(1:3,:)./pcl_data(4,:));
    pcl_data = pcl_data(1:2,:)./pcl_data(3,:);
    mapped_coordinates = reshape(pcl_data', size(depth_img,1), size(depth_img,2), 2);
    
end