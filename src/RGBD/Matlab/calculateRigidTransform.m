%
% Description:
%   The function calculateRigidTransform calculates the rotation and
%   translation between two sets of 3D points
%
% Inputs:
%   correspondences     -   Array containing the 3D points
%   indices             -   Index array for the subset of correspondences
%                           to use
%   dev_idx             -   Index of the target device (the source device
%                           is assumed to have index dev_idx-1)
%
% Outputs:
%   R   -   Rotation from source device to target device
%   t   -   Translation from source device to target device
%

function [R,t] = calculateRigidTransform(correspondences, indices, dev_idx)

    num_points = size(indices,2);
    % get the 3D points
    p_src = zeros(num_points,3);
    p_tgt = zeros(num_points,3);

    for i = 1:num_points
        p_src(i,:) = correspondences(indices(i), ...
            11 + (dev_idx-2)*3: 13 + (dev_idx-2)*3);
        p_tgt(i,:) = correspondences(indices(i), ...
            11 + (dev_idx-1)*3: 13 + (dev_idx-1)*3);
    end

    % calculate centroids
    c_src =  sum(p_src) / num_points;
    c_tgt =  sum(p_tgt) / num_points;
    % center points
    p_src = p_src - c_src;
    p_tgt = p_tgt - c_tgt;

    % calculate rotation via svd
    [U, ~ ,V] = svd( p_src.' * p_tgt );
    R = V * U.';
    % calculate translation
    t = c_tgt.' - R * c_src.';

end