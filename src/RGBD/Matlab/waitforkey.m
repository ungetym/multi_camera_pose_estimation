%
% Description:
%   The function waitforkey is used to stop the code execution at a certain
%   point and wait for the user to press a key to continue. This can be
%   helpful for debugging.
%

function waitforkey()  
    waitforbuttonpress;
    value = double(get(gcf,'CurrentCharacter'));
    while value ~= 29
        waitforbuttonpress;
        value = double(get(gcf,'CurrentCharacter'));
    end
    set(gcf,'currentch',char(1));
    drawnow;
end