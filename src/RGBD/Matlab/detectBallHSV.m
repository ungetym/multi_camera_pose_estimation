%
% Description:
%   The function detectBallHSV tries to detect an illuminated orange table
%   tennis ball in an RGB color image
%
% Inputs:
%   img     -   RGB image to detect the table tennis ball in
%
% Outputs:
%   x       -   Image column, the ball was detected in. 0 if not detected.
%   y       -   Image row, the ball was detected in. 0 if not detected.
%   r       -   Radius in px of the detected ball. 0 if not detected.
%

function [x,y,r] = detectBallHSV(img)
    
    % convert rgb image to hsv space
    img_hsv = rgb2hsv(img);

    % threshold the different channels (empirical values based on first data)
    %h = img_hsv(:,:,1) > 245/255;
    %s = (img_hsv(:,:,2) > 224/255) .* (img_hsv(:,:,2) < 248/255);
    %v = (img_hsv(:,:,3) > 61/255) .* (img_hsv(:,:,3) < 102/255);

    h = (img_hsv(:,:,1) > 340/360) + (img_hsv(:,:,1) < 20/360);
    s = (img_hsv(:,:,2) > 0.25) .* (img_hsv(:,:,2) < 0.5);
    v = (img_hsv(:,:,3) > 0.9);

    % calculate accepted pixels
    accepted = im2bw(h .* s .* v, 0.5);

    % apply filters
    SE = strel('square', 3);
    accepted = imdilate(accepted, SE);

    % check number of clusters by labeling them
    [label_image, num_clusters] = bwlabel(accepted);

    if num_clusters < 1
        x = 0;
        y = 0;
        r = 0;
        return;
    end

    % select cluster with best roundness value
    properties = regionprops(label_image, 'Centroid', ...
        'MinorAxisLength', 'MajorAxisLength', 'Area');

    % select cluster with best roundness value
    best_idx = -1;
    best_ratio = 10000;
    for i = 1:num_clusters
        ratio = properties(i).MajorAxisLength / properties(i).MinorAxisLength;
        if ratio < best_ratio && properties(i).Area > 25
            best_ratio = ratio;
            best_idx = i;
        end
    end

    % return if all clusters were rejected due to small size
    if best_idx == -1
        x = 0;
        y = 0;
        r = 0;
        return;
    end

    % return cluster center and radius
    x = properties(best_idx).Centroid(1);
    y = properties(best_idx).Centroid(2);
    r = properties(best_idx).MinorAxisLength/2;
    
    % reject small or unround detections
    if r < 3 || best_ratio > 2
        x = 0;
        y = 0;
        r = 0;
    end

end