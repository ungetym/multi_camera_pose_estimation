%
% Description:
%   The function undistortImage corrects image distortions based on
%   pre-calculated coordinate maps
%
% Inputs:
%   img             -   RGB image of size WxH to undistort
%   map_x           -   Undistorted x coordinates 
%   map_y           -   Undistorted y coordinates 
%   interpolation   -   'nearest', 'linear', or 'cubic' used for
%                       interpolation with imwarp
%
% Outputs:
%   rectified_img   -   Undistorted image 
%

function rectified_img = undistortImage(img, map_x, map_y, interpolation)
    % calculate displacement field
    [X,Y] = meshgrid(1:size(img,2), 1:size(img,1));
    displacement(:,:,1) = map_x - X;%max(1,min(size(img,2),map_x)) - X;
    displacement(:,:,2) = map_y - Y;%max(1,min(size(img,1),map_y)) - Y;

    if nargin == 4
        rectified_img = imwarp(img, displacement, interpolation);
    else
        rectified_img = imwarp(img, displacement);
    end
end