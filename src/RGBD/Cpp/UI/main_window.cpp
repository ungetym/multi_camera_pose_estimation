#include "main_window.h"
#include "./ui_main_window.h"

#include <QDesktopServices>
#include <QDir>
#include <QFileDialog>

Main_Window::Main_Window(std::shared_ptr<Data> data, QWidget *parent)
    : data_(data)
    , QMainWindow(parent)
    , ui_(new Ui::Main_Window)
{
    ui_->setupUi(this);

    // set initial visibility regarding the advanced options
    toggleAdvanced();

    // update GUI values
    setDataToGUI();

    // connect image dir buttons
    connect(ui_->button_select_image_dir, &QPushButton::clicked, this, &Main_Window::setImageDir);
    connect(ui_->button_open_image_dir, &QPushButton::clicked, this, &Main_Window::openImageDir);

    // connect intrinsics buttons
    connect(ui_->button_select_intrinsics_file, &QPushButton::clicked, this, &Main_Window::setIntrinsicsFile);

    // connect advanced options checkbox
    connect(ui_->check_box_advanced_options, &QCheckBox::toggled, this, &Main_Window::toggleAdvanced);

    // connect simplified process buttons
    connect(ui_->button_calc_poses, &QPushButton::clicked, this, &Main_Window::detectionStartRequested);

    // connect correspondence detection buttons
    connect(ui_->button_detect_correspondences, &QPushButton::clicked, this, &Main_Window::detectionStartRequested);

    // connect further correspondence detection related signals
    connect(this, &Main_Window::detectCorrespondences, data_->detector_.get(), &Detector::detectCorrespondences);
    connect(data_->detector_.get(), &Detector::progress, this, &Main_Window::progress);
    connect(data_->detector_.get(), &Detector::ballsDetected, this, &Main_Window::receiveDetections);

    // connect optimizer
    connect(ui_->button_calc_initial_poses, &QPushButton::clicked, this, &Main_Window::initialPoseEstimationRequested);
    connect(this, &Main_Window::initialPoseEstimation, data_->optimizer_.get(), &Optimizer::initialPoseEstimation);
    connect(data_->optimizer_.get(), &Optimizer::progress, this, &Main_Window::progress);
    connect(ui_->button_start_ba, &QPushButton::clicked, this, &Main_Window::bundleAdjustmentRequested);
    connect(this, &Main_Window::bundleAdjustment, data_->optimizer_.get(), &Optimizer::bundleAdjustment);

    // connect logger
    connect(this, &Main_Window::log, ui_->logger, &Logger::log);
    connect(data_.get(), &Data::log, ui_->logger, &Logger::log);
    connect(data_.get(), &Data::logMat, ui_->logger, &Logger::logMat);
    connect(data_->imagelist_gen_.get(), &Imagelist_Generator::log, ui_->logger, &Logger::log);
    connect(data_->detector_.get(), &Detector::log, ui_->logger, &Logger::log);
    connect(data_->optimizer_.get(), &Optimizer::log, ui_->logger, &Logger::log);
    connect(data_->optimizer_.get(), &Optimizer::logMat, ui_->logger, &Logger::logMat);

    // connect IO
    connect(ui_->button_save_poses, &QPushButton::clicked, this, &Main_Window::savePosesRequested);
    connect(ui_->button_save_poses_simple, &QPushButton::clicked, this, &Main_Window::savePosesRequested);

}

Main_Window::~Main_Window(){
    delete ui_;
}

void Main_Window::setDataToGUI(){
    ui_->lineedit_image_dir->setText(QString::fromStdString(data_->image_dir_));
    ui_->lineedit_rgb_scheme->setText(QString::fromStdString(data_->rgb_image_name_scheme_));
    ui_->lineedit_depth_scheme->setText(QString::fromStdString(data_->depth_image_name_scheme_));
    ui_->lineedit_rgb_cam_name->setText(QString::fromStdString(data_->rgb_camera_name_scheme_));
    ui_->lineedit_ir_cam_name->setText(QString::fromStdString(data_->ir_camera_name_scheme_));
    ui_->lineedit_intrinsics->setText(QString::fromStdString(data_->intrinsics_file_path_));
    data_->parseCalibration();
}

void Main_Window::progress(const float &value){
    ui_->progressbar->setValue(int(value*100.0f));
}

void Main_Window::setImageDir(){
    //open file dialog
    QString dir_name_q = QFileDialog::getExistingDirectory(this,"Choose a directory","~",QFileDialog::ShowDirsOnly);

    // check resulting string
    std::string dir_name = dir_name_q.toStdString();
    if(dir_name.size()==0){
        STATUS("No directory chosen.");
        return;
    }
    // if valid, save image dir path
    data_->image_dir_ = dir_name;
    ui_->lineedit_image_dir->setText(dir_name_q);
    STATUS("Image dir set to"+dir_name_q);
}

void Main_Window::openImageDir(){
    if(data_->image_dir_.size()==0){
        ERROR("No directory set.");
        return;
    }
    else{
        //check if dir exists
        QString dir_name_q = QString::fromStdString(data_->image_dir_);
        QDir dir(dir_name_q);
        if(!dir.exists()){
            ERROR("Directory does not exist.");
            return;
        }
        else{
            //open dir in default file explorer
            QDesktopServices::openUrl(QUrl::fromLocalFile(dir_name_q));
        }
    }
}

void Main_Window::setIntrinsicsFile(){
    //open file dialog
    QString file_name_q = QFileDialog::getOpenFileName(this,"Choose a calibration file","~");

    // check resulting string
    std::string file_name = file_name_q.toStdString();
    if(file_name.size()==0){
        STATUS("No file chosen.");
        return;
    }
    // if valid, save image dir path
    data_->intrinsics_file_path_ = file_name;
    data_->parseCalibration();
    ui_->lineedit_intrinsics->setText(file_name_q);
    STATUS("Calibration file set to" + file_name_q);
}

void Main_Window::toggleAdvanced(){
    // get current state of checkbox
    bool advanced = ui_->check_box_advanced_options->isChecked();
    // set visibility of simple..
    ui_->box_simple_options->setHidden(advanced);
    // ..and advanced options
    ui_->box_correspondece_detection->setHidden(!advanced);
    ui_->box_initial_poses->setHidden(!advanced);
    ui_->box_bundle_adjustment->setHidden(!advanced);

    // automatically start bundle adjustment after intial pose estimation if
    // simple mode is active
    if(!advanced){
        connect(data_->optimizer_.get(), &Optimizer::initialPosesCalculated,
                this, &Main_Window::bundleAdjustmentRequested);
    }
    else{
        disconnect(data_->optimizer_.get(), &Optimizer::initialPosesCalculated,
                   this, &Main_Window::bundleAdjustmentRequested);
    }
}

void Main_Window::detectionStartRequested(){
    // checks if all relevant data has been set
    if(data_->image_dir_.size() == 0){
        ERROR("Please set the image dir!");
        return;
    }
    if(data_->devices_.size() == 0){
        ERROR("Intrinsic parameters not loaded. Please set the camera parameter "
              "file!");
        return;
    }
    if(ui_->lineedit_rgb_scheme->text().size() == 0 ||
            ui_->lineedit_depth_scheme->text().size() == 0){
        ERROR("File name schemes not set!");
        return;
    }
    // emit signal to start detection with all relevant parameters
    emit detectCorrespondences(data_->image_dir_, data_->devices_,
                               ui_->lineedit_rgb_scheme->text(),
                               ui_->lineedit_depth_scheme->text(),
                               ui_->spinboxd_ball_radius->value(),
                               ui_->selector_visualization->currentIndex());
}

void Main_Window::initialPoseEstimationRequested(){
    if(!data_->detections_){
        ERROR("Detections not available");
        return;
    }
    emit initialPoseEstimation(data_->devices_, data_->detections_,
                               ui_->spinbox_ransac_max_iter->value(),
                               ui_->spinboxd_ransac_threshold->value());
}

void Main_Window::bundleAdjustmentRequested(){
    if(!data_->detections_){
        ERROR("Detections not available");
        return;
    }
    emit bundleAdjustment(data_->devices_, data_->detections_,
                          ui_->spinbox_ba_max_iter->value(),
                          ui_->spinboxd_ba_threshold->value());
}

void Main_Window::savePosesRequested(){
    //open file dialog
    QString file_name_q = QFileDialog::getSaveFileName(this, "Choose or create a calibration file to store the poses",
                                                       QString::fromStdString(data_->image_dir_));

    // check resulting string
    std::string file_name = file_name_q.toStdString();
    if(file_name.size()==0){
        STATUS("No file chosen.");
        return;
    }
    // if valid, save image dir path
    data_->saveCalibration(file_name);
}

void Main_Window::receiveDetections(const std::shared_ptr<Detections>& detections){
    data_->detections_ = detections;
    // in the simplified process, directly start the initial pose estimation
    if(! ui_->check_box_advanced_options->isChecked()){
        initialPoseEstimationRequested();
    }
}
