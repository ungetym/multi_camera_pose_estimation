////////////////////////////////////////////////////////////////////////////////
///     This header describes simple structs, constants, and type aliases    ///
///                      used throughout different classes                   ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <eigen3/Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

/// constants used to enable different debug visualizations
const int VIS_NONE = 0;
const int VIS_RGB_DETECTIONS = 1;
const int VIS_UNDISTORTED_DETECTIONS = 2;
const int VIS_DEPTH_PIXELS = 3;
const int VIS_ALL = 4;

///
/// \brief The Pose struct contains a transform between two 3D coordinate frames
///                        in different forms
///
struct Pose{

    ///
    /// \brief Pose is the default constructor
    ///
    Pose(){
        m_cv_ = cv::Mat::eye(4,4, CV_64F);
        R_cv_ = m_cv_(cv::Rect(0,0,3,3)).clone();
        t_cv_ = m_cv_(cv::Rect(3,0,1,3)).clone();
        cv::cv2eigen(m_cv_, m_);
        cv::cv2eigen(R_cv_, R_);
        cv::cv2eigen(t_cv_, t_);
    }

    ///
    /// \brief Pose is a constructor taking a 4x4 Eigen matrix
    /// \param P
    ///
    Pose(const Eigen::Matrix4d& P){
        m_ = P;
        R_ = m_.block(0,0,3,3);
        t_ = m_.block(0,3,3,1);
        cv::eigen2cv(m_,m_cv_);
        cv::eigen2cv(R_,R_cv_);
        cv::eigen2cv(t_,t_cv_);
    }

    ///
    /// \brief Pose is a constructor taking a 4x4 OpenCV matrix
    /// \param P
    ///
    Pose(const cv::Mat& P){
        m_cv_ = P.clone();
        R_cv_ = m_cv_(cv::Rect(0,0,3,3)).clone();
        t_cv_ = m_cv_(cv::Rect(3,0,1,3)).clone();
        cv::cv2eigen(m_cv_, m_);
        cv::cv2eigen(R_cv_, R_);
        cv::cv2eigen(t_cv_, t_);
    }

    ///
    /// \brief Pose is a constructor taking a 3x3 Eigen matrix and a 3d Eigen
    ///             vector
    /// \param R    rotation
    /// \param t    translation
    ///
    Pose(const Eigen::Matrix3d& R, const Eigen::Vector3d& t){
        m_ = Eigen::Matrix4d::Identity();
        m_.block(0,0,3,3) = R;
        m_.block(0,3,3,1) = t;
        R_ = R;
        t_ = t;
        cv::eigen2cv(m_,m_cv_);
        cv::eigen2cv(R_,R_cv_);
        cv::eigen2cv(t_,t_cv_);
    }

    ///
    /// \brief operator * multiplies this pose with a given other pose
    /// \param pose       the other pose
    /// \return           the product of these transformations
    ///
    Pose operator * (const Pose& pose) const{
        return Pose(R_ * pose.R_, R_ * pose.t_ + t_);
    }

    /// pose as 4x4 OpenCV mat
    cv::Mat m_cv_;
    /// pose as OpenCV mats in the form of a 3x3 rotation matrix...
    cv::Mat R_cv_;
    /// ...and a 1x3 translation vector
    cv::Mat t_cv_;

    /// pose as 4x4 Eigen matrix
    Eigen::Matrix4d m_;
    /// pose as 3x3 rotation Eigen matrix...
    Eigen::Matrix3d R_;
    /// ...and 3d translation Eigen vector
    Eigen::Vector3d t_;
};

///
/// \brief The Cam_Parameters struct contains the intrinsic parameters of a
///                                  single camera (i.e. the rgb or ir camera of
///                                  a device).
///
struct Cam_Parameters{
    /// camera name, e.g. 'irkinect0'
    std::string name_;

    /// resolution
    int res_x_;
    int res_y_;
    /// camera matrix
    cv::Mat K_;
    /// and its inverse
    cv::Mat K_inv_;
    /// distortion coefficients
    cv::Mat dist_;

    /// camera pose relative to world coordinates as 4x4 matrix, i.e. transform
    /// world->camera
    cv::Mat pose_;

    /// precalculated undistortion maps for image rectification
    cv::Mat map_x_;
    cv::Mat map_y_;
};

///
/// \brief The Device struct describes an RGBD camera consisting of a separate
///                          RGB and IR/depth camera
///
struct Device{

    ///
    /// \brief Device
    /// \param name
    ///
    Device(const std::string& name){
         device_name_ = name;
    }
    /// camera device name, e.g. 'kinect0'
    std::string device_name_;
    /// intrinsics
    Cam_Parameters rgb_;
    Cam_Parameters depth_;
    /// transformation from rgb to depth camera
    Pose pose_rgb_to_depth_;
    /// transformation from world coordinate frame to rgb camera
    Pose world_pose_;

    /// fundamental matrix (rgb to depth camera)
    cv::Mat F_;
};

///
/// \brief The Detection struct
///
struct Detection{

    /// associated image file paths
    std::string rgb_image_file_;
    std::string depth_image_file_;

    /// is set to false if ball or corresponding depth value can not be detected
    bool valid_;

    /// is set to true if detection is an inlier for any initial pose estimation
    bool inlier_ = false;

    /// rgb pixel coordinates
    double x_;
    double y_;
    /// size of the ball detected in the rgb image in pixels
    double r_;

    /// 3D scene point in rgb camera coordinates
    Eigen::Vector3d p_;
};

/// alias for readability - Detections[i][j] contains the detection from image i
/// of camera j
using Detections = std::vector<std::vector<Detection>>;

