#include "data.h"
#include <fstream>

Data::Data(){
    // create instances of the imagelist generator, detector and optimizer
    imagelist_gen_ = std::shared_ptr<Imagelist_Generator>(new Imagelist_Generator());
    detector_ = std::shared_ptr<Detector>(new Detector(imagelist_gen_));
    optimizer_ = std::shared_ptr<Optimizer>(new Optimizer());

    // load previous GUI settings if available
    loadData();
}

void Data::saveData(){
    // simply create a file in the binary dir...
    std::ofstream file("settings.txt");
    if (file.is_open()){
        // ...and store the GUI setup as simple strings
        file << "ImageDir\n" << image_dir_ << "\n";
        file << "RGBImageSheme\n" << rgb_image_name_scheme_ << "\n";
        file << "DepthImageSheme\n" << depth_image_name_scheme_ << "\n";
        file << "RGBCameraSheme\n" << rgb_camera_name_scheme_ << "\n";
        file << "IRCameraSheme\n" << ir_camera_name_scheme_ << "\n";
        file << "IntrinsicsFile\n" << intrinsics_file_path_ << "\n";
    }
}

void Data::loadData(){
    // try to load the setup file
    std::ifstream file("settings.txt");
    std::string label;
    if (file.is_open()){
        while(!file.eof()){
            // read 'line-by-line' where the first part is the descriptor and
            // the second part is the data value
            file >> label;
            if(label.compare("ImageDir") == 0){
                file >> image_dir_;
            }
            else if(label.compare("RGBImageSheme") == 0){
                file >> rgb_image_name_scheme_;
            }
            else if(label.compare("DepthImageSheme") == 0){
                file >> depth_image_name_scheme_;
            }
            else if(label.compare("RGBCameraSheme") == 0){
                file >> rgb_camera_name_scheme_;
            }
            else if(label.compare("IRCameraSheme") == 0){
                file >> ir_camera_name_scheme_;
            }
            else if(label.compare("IntrinsicsFile") == 0){
                file >> intrinsics_file_path_;
            }
        }
    }
}

bool Data::parseCameraNameScheme(const std::string &input_string, std::vector<Tag> *scheme){

    // search for tag $CAM and label everything else as string tag
    int last = -1;
    for(size_t i = 0; i< input_string.size(); i++){
        if(input_string[i] == '$'){
            if(i-last > 1){
                scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
            }

            if(input_string.substr(i,4).compare("$CAM") == 0){
                scheme->push_back(Tag(CAM,"$CAM"));
                i += 3;
                last = i;
            }
            else{
                ERROR("Unable to parse naming scheme: "
                      + QString::fromStdString(input_string)
                      + " Only the tag $CAM is allowed");
                scheme->clear();
                return false;
            }
        }
        else if(i == input_string.size() - 1){
            scheme->push_back(Tag(i-last-1,input_string.substr(last+1,i-last-1)));
        }
    }

    return true;
}

void Data::parseCalibration(){

    devices_.clear();

    // parse camera name schemes
    std::vector<Tag> rgb_cam_scheme, ir_cam_scheme;
    if(!parseCameraNameScheme(rgb_camera_name_scheme_, &rgb_cam_scheme)){
        return;
    }
    else{
        if(!parseCameraNameScheme(ir_camera_name_scheme_, &ir_cam_scheme)){
            return;
        }
    }

    // helper function to jump around ifstream
    auto goToLine = [](std::ifstream& file, const unsigned int& num){
        file.seekg(std::ios::beg);
        for(int i=0; i < num - 1; ++i){
            file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        }
        return !file.eof();
    };

    // open file
    std::ifstream file(intrinsics_file_path_.c_str());
    // check if file can be read
    if(!file.is_open ()){
        ERROR("Calibration file could not be opened");
        return;
    }

    // starting line after initial description in MCC_Cams file
    int current_line = 6;
    // save index for every camera - this is helpful if the ir and rgb cameras
    // are not stored in a known order
    std::map<std::string,int> name_index;

    // start reading
    std::vector<std::string> camera_names;
    std::string camera_name;
    while(goToLine(file, current_line)){

        // extract camera name
        file >> camera_name >> camera_name;

        // if eof, end reading
        if(file.eof()){
            break;
        }

        // check if one of the naming schemes matches
        bool rgb_scheme_matches = true;
        bool rgb_has_str_tag = false;
        std::string camera_name_rgb = camera_name;
        for(const Tag& tag : rgb_cam_scheme){
            if(tag.first > 0){
                size_t pos = camera_name.find(tag.second);
                rgb_scheme_matches = (pos != std::string::npos);
                rgb_has_str_tag = true;
                if (rgb_scheme_matches){
                    camera_name_rgb.erase(pos, tag.first);
                }
            }
        }
        bool ir_scheme_matches = true;
        bool ir_has_str_tag = false;
        std::string camera_name_ir = camera_name;
        for(const Tag& tag : ir_cam_scheme){
            if(tag.first > 0){
                size_t pos = camera_name.find(tag.second);
                ir_scheme_matches = (pos != std::string::npos);
                ir_has_str_tag = true;
                if (ir_scheme_matches){
                    camera_name_ir.erase(pos, tag.first);
                }
            }
        }

        bool is_rgb = rgb_scheme_matches && (!ir_scheme_matches ||
                                             (ir_scheme_matches && rgb_has_str_tag && !ir_has_str_tag));
        camera_name = is_rgb ? camera_name_rgb : camera_name_ir;

        // search for camera name in existing cameras
        Cam_Parameters* params;
        auto pos = std::find(camera_names.begin(), camera_names.end(),
                             camera_name);
        if(pos != camera_names.end()){
            params = is_rgb ? &(devices_[std::distance(
                                  camera_names.begin(), pos)].rgb_)
                    : &(devices_[std::distance(
                            camera_names.begin(), pos)].depth_);
        }
        else{
            camera_names.push_back(camera_name);
            devices_.push_back(Device(camera_name));
            params = is_rgb ? &(devices_.back().rgb_)
                            : &(devices_.back().depth_);
        }

        params->name_ = camera_name_rgb;

        if(is_rgb){
            STATUS("Reading RGB camera parameters of device "
                   + QString::fromStdString(camera_name));
        }
        else{
            STATUS("Reading IR camera parameters of device "
                   + QString::fromStdString(camera_name));
        }

        // read resolution
        file >> params->res_x_ >> params->res_y_;

        // read camera distortion parameters
        goToLine(file, current_line+1);
        params->dist_ = cv::Mat(8,1, CV_64F);
        for(int i = 0; i < 8; i++){
            file >> params->dist_.at<double>(0,i);
        }
        LOGMATRIX("Dist =",params->dist_);


        // read K matrix
        goToLine(file, current_line+2);
        params->K_ = cv::Mat(3,3,CV_64F);
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                file >> params->K_.at<double>(row,col);
            }
        }
        params->K_inv_ = params->K_.inv();
        LOGMATRIX("K =",params->K_);

        // read the pose
        goToLine(file, current_line+3);
        params->pose_ = cv::Mat(4, 4,CV_64F);
        for(int row = 0; row < 4; row++){
            for(int col = 0; col < 4; col++){
                file >> params->pose_.at<double>(row,col);
            }
        }
        LOGMATRIX("pose =",params->pose_);

        // go to next camera
        current_line += 4;
    }

    // set transforms between rgb and ir/depth device
    for(Device& dev : devices_){
        dev.pose_rgb_to_depth_ = Pose(dev.depth_.pose_ * dev.rgb_.pose_.inv());
    }
}

void Data::saveCalibration(const std::string& file_path){

    // open target file
    std::ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Output file "+ QString::fromStdString(file_path)+" could not be opened!");
        return;
    }

    // write header
    file << "$CamID $Width $Height $CamType\n"
            "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6\n"
            "$K matrix rowwise\n"
            "$Pose matrix rowwise\n";

    // write devices
    for(const Device& device: devices_){
        for(int i = 0; i < 2; i++){

            const Cam_Parameters& cam = (i==0) ? device.depth_ : device.rgb_;

            // write camera name and resolution
            file << "\nCam " << cam.name_ << " " << cam.res_x_
                 << " " << cam.res_y_ << " -1\n";
            // write ir intrinsics
            for(size_t j = 0; j < 7; j++){
                file << cam.dist_.at<double>(0,j) << " ";
            }
            file << cam.dist_.at<double>(0,7) << "\n";
            // write K matrix
            for(size_t row = 0; row < 3; row++){
                for(size_t col = 0; col < 3; col++){
                    file << cam.K_.at<double>(row,col) ;
                    if(col == 2 && row == 2){
                        file << "\n";
                    }
                    else{
                        file << " ";
                    }
                }
            }
            // write pose
            const Pose pose = (i==0) ? device.pose_rgb_to_depth_ * device.world_pose_ : device.world_pose_;
            for(size_t row = 0; row < 4; row++){
                for(size_t col = 0; col < 4; col++){
                    file << pose.m_cv_.at<double>(row,col);
                    if(col < 3 || row < 3){
                        file << " ";
                    }
                }
            }
        }
    }

    file.close();

    STATUS("Poses successfully saved.");
}
