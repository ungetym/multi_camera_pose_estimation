////////////////////////////////////////////////////////////////////////////////
///     The data class holds all relevant data and subprogram instances      ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Backend/detector.h>
#include <Backend/imagelist_generator.h>
#include <Backend/optimizer.h>
#include <UI/logger.h>
#include <QObject>

///
/// \brief The Data class holds all relevant data such as detections and device
///                       parameters. Furthermore, it contains pointers to the
///                       instances of the optimizer and detector and handles
///                       IO, i.e. the parsing and writing of camera parameter
///                       files.
///
class Data : public QObject{

    Q_OBJECT

public:
    ///
    /// \brief Data is the default constructor
    ///
    Data();

    /// destructor
    ~Data(){saveData();}

    ///
    /// \brief saveData is used to save the GUI settings to a txt file when the
    ///                 program is closed
    ///
    void saveData();

    ///
    /// \brief loadData is used to load the GUI data from a txt file at startup
    ///
    void loadData();

    ///
    /// \brief parseCameraNameScheme reads the user-specified cam naming scheme
    ///                              given as a string and divides it into a
    ///                              sequence of tag types which can later be
    ///                              used to associate file names with cameras.
    ///                              IMPORTANT: The tag $CAM must be used
    ///                              exactly once in every camera name scheme!
    /// \param input_string          the user-specified scheme as string
    /// \param scheme                the resulting list of tags
    /// \return                      true if valid scheme, false otherwise
    ///
    bool parseCameraNameScheme(const std::string& input_string,
                               std::vector<Tag>* scheme);

    ///
    /// \brief parseCalibration reads the device/camera parameters from an M
    ///                         CC_Cams file as produced by the Multi-Camera
    ///                         Calibration tool available here:
    ///                      https://gitlab.com/ungetym/Multi_Camera_Calibration
    ///
    void parseCalibration();

    ///
    /// \brief saveCalibration is the output pendant to parseCalibration
    /// \param file_path
    ///
    void saveCalibration(const std::string& file_path);

public:
    /// directory containing the RGB and depth images
    std::string image_dir_;
    std::string rgb_image_name_scheme_;
    std::string depth_image_name_scheme_;

    /// path to the file containing the intrinsic parameters for all devices
    std::string intrinsics_file_path_;

    /// naming schemes are also saved here for the purpose of saving/restoring
    /// the user settings
    std::string rgb_camera_name_scheme_;
    std::string ir_camera_name_scheme_;

    /// list of available RGBD devices
    std::vector<Device> devices_;

    /// pointer to an instance of the imagelist generator
    std::shared_ptr<Imagelist_Generator> imagelist_gen_;

    /// pointer to an instance of the rgb table tennis ball detector
    std::shared_ptr<Detector> detector_;

    /// pointer to the list of detections returned by the detector
    std::shared_ptr<Detections> detections_;

    /// pointer to the optimizer instance used for the initial pose estimation
    /// and the final bundle adjustment
    std::shared_ptr<Optimizer> optimizer_;

signals:
    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(const QString& msg, const cv::Mat& mat);
};
