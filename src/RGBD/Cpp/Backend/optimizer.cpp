#include "optimizer.h"

#include <random>

Optimizer::Optimizer(){}

void Optimizer::initialPoseEstimation(std::vector<Device>& devices,
                                      const std::shared_ptr<Detections>& detections,
                                      const int &max_iterations, const double &inlier_threshold){

    STATUS("Starting initial pose estimation...");

    // create a random number generator and seed it
    std::random_device rd;
    std::mt19937 gen(rd());

    double dist;
    std::vector<Pose> poses;
    std::vector<int> num_inliers;

    float num_iterations = max_iterations * (devices.size()-1);
    float current_iter = 0.0f;

    // determine pairwise poses
    for(size_t dev_idx = 0; dev_idx < devices.size() - 1; dev_idx++){

        // check list of correspondences for the ones between the devices with
        // index dev_idx and dev_idx+1
        std::vector<size_t> indices;
        for(size_t detection_idx = 0; detection_idx < detections->size(); detection_idx++){
            if((*detections)[detection_idx][dev_idx].valid_ && (*detections)[detection_idx][dev_idx+1].valid_){
                indices.push_back(detection_idx);
            }
        }

        if(indices.size() < 3){
            ERROR("Not enough correspondences available to estimate the initial pose of device "
                  + QString::fromStdString(devices[dev_idx].device_name_)
                  + " relative to device "
                  + QString::fromStdString(devices[dev_idx+1].device_name_));
            return;
        }

        // define randaom number generator range
        std::uniform_int_distribution<> sampler(0, indices.size()-1);

        std::vector<int> inlier_set, best_inlier_set;
        std::vector<Eigen::Vector3d> points_a, points_b;

        for(int i = 0; i < max_iterations; i++){

            // randomly sample 3 correspondences
            std::vector<int> idx;
            points_a.clear();
            points_b.clear();
            idx.push_back(sampler(gen));
            points_a.push_back((*detections)[indices[idx[0]]][dev_idx].p_);
            points_b.push_back((*detections)[indices[idx[0]]][dev_idx+1].p_);

            idx.push_back(sampler(gen));
            while(idx[0] == idx[1]){
                idx[1] = sampler(gen);
            }
            points_a.push_back((*detections)[indices[idx[1]]][dev_idx].p_);
            points_b.push_back((*detections)[indices[idx[1]]][dev_idx+1].p_);

            idx.push_back(sampler(gen));
            while(idx[0] == idx[2] || idx[1] == idx[2]){
                idx[2] = sampler(gen);
            }
            points_a.push_back((*detections)[indices[idx[2]]][dev_idx].p_);
            points_b.push_back((*detections)[indices[idx[2]]][dev_idx+1].p_);

            // calculate the pose based on these correspondences
            Pose pose = calculatePoseSVD(points_a, points_b);

            // calculate the inlier set
            inlier_set.clear();
            for(const size_t& j : indices){
                dist = (pose.R_ * (*detections)[j][dev_idx].p_ + pose.t_
                        - (*detections)[j][dev_idx+1].p_).norm();
                if(dist < inlier_threshold){
                    inlier_set.push_back(j);
                }
            }

            // check if best inlier set
            if(inlier_set.size() > best_inlier_set.size()){
                best_inlier_set = inlier_set;
            }

            current_iter++;
            emit progress(current_iter/num_iterations);
        }

        // calculate final pose with best inlier set
        points_a.clear();
        points_b.clear();
        for(const size_t& j : best_inlier_set){
            (*detections)[j][dev_idx].inlier_ = true;
            points_a.push_back((*detections)[j][dev_idx].p_);
            points_b.push_back((*detections)[j][dev_idx+1].p_);
        }
        poses.push_back(calculatePoseSVD(points_a, points_b));
        num_inliers.push_back(int(best_inlier_set.size()));
    }

    STATUS("...done. Initial poses are: ");
    // combine pairwise initial poses to get world poses
    STATUS(QString::fromStdString(devices[1].device_name_));
    STATUS("Inliers: " + QString::number(num_inliers[0]));
    LOGMATRIX("R", poses[0].R_cv_);
    LOGMATRIX("t", poses[0].t_cv_);
    devices[1].world_pose_ = poses[0];
    for(size_t pose_idx = 1; pose_idx < poses.size(); pose_idx++){
        poses[pose_idx] = poses[pose_idx] * poses[pose_idx-1];
        STATUS(QString::fromStdString(devices[pose_idx+1].device_name_));
        STATUS("Inliers: " + QString::number(num_inliers[pose_idx]));
        LOGMATRIX("R", poses[pose_idx].R_cv_);
        LOGMATRIX("t", poses[pose_idx].t_cv_);
        devices[pose_idx+1].world_pose_ = poses[pose_idx];
    }

    emit initialPosesCalculated();
}

void Optimizer::bundleAdjustment(std::vector<Device>& devices,
                                 const std::shared_ptr<Detections>& detections,
                                 const int &max_iterations,
                                 const double &change_threshold){
    STATUS("Starting bundle adjustment...");

    /// prepare the data for the ceres solver

    // create poses as double arrays
    std::vector<std::vector<double>> poses
            = std::vector<std::vector<double>>(devices.size(),
                                               std::vector<double>(6,0.0));

    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        Device& device = devices[dev_idx];
        ceres::RotationMatrixToAngleAxis<double>(
                    static_cast<double*> (device.world_pose_.R_.data()),
                    poses[dev_idx].data());
        poses[dev_idx][3] = device.world_pose_.t_[0];
        poses[dev_idx][4] = device.world_pose_.t_[1];
        poses[dev_idx][5] = device.world_pose_.t_[2];
    }

    // create 3D world points as double arrays
    std::vector<std::vector<double>> scene_points
            = std::vector<std::vector<double>>(detections->size(),
                                               std::vector<double>(3,0.0));

    for(size_t point_idx = 0; point_idx < detections->size(); point_idx++){
        Eigen::Vector3d world_point(0.0,0.0,0.0);
        double num_valid = 0.0;
        for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
            if( (*detections)[point_idx][dev_idx].inlier_ ){
                world_point += devices[dev_idx].world_pose_.R_.transpose() *
                        ((*detections)[point_idx][dev_idx].p_
                         - devices[dev_idx].world_pose_.t_);
                num_valid++;
            }
        }
        if(num_valid > 0.0){
            world_point /= num_valid;
        }

        scene_points[point_idx] = {world_point[0], world_point[1], world_point[2]};
    }

    /// create the problem :)

    ceres::Problem problem;
    // configure solver
    ceres::Solver::Options options;
    options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
    options.use_nonmonotonic_steps = false;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = 0;
    options.max_num_iterations = max_iterations;
    options.num_threads = 12;
    options.function_tolerance = change_threshold;
    options.gradient_tolerance = 0.0;

    // add residual blocks
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        for(size_t point_idx = 0; point_idx < detections->size(); point_idx++){
            if( (*detections)[point_idx][dev_idx].inlier_ ){
                ceres::CostFunction* cost_function;
                if(dev_idx > 0){
                    cost_function = new ceres::AutoDiffCostFunction<Reprojection_Error, 2, 6, 3>(
                                new Reprojection_Error(devices[dev_idx],
                                                       Eigen::Vector2d((*detections)[point_idx][dev_idx].x_,
                                                                       (*detections)[point_idx][dev_idx].y_)));
                    problem.AddResidualBlock(cost_function, new ceres::HuberLoss(1.0), poses[dev_idx].data(), scene_points[point_idx].data());

                }
                else{
                    cost_function = new ceres::AutoDiffCostFunction<Reprojection_Error_Fixed_Pose, 2, 3>(
                                new Reprojection_Error_Fixed_Pose(devices[dev_idx],
                                                                  Eigen::Vector2d((*detections)[point_idx][dev_idx].x_,
                                                                                  (*detections)[point_idx][dev_idx].y_)));
                    problem.AddResidualBlock(cost_function, new ceres::HuberLoss(1.0), scene_points[point_idx].data());
                }
            }
        }
    }

    /// optimization

    // run the solver
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    STATUS(QString::fromStdString(summary.FullReport()));

    // update poses
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        Device& device = devices[dev_idx];
        Eigen::Matrix3d rotation;
        ceres::AngleAxisToRotationMatrix<double>(poses[dev_idx].data(),
                                                 static_cast<double*>(rotation.data()));
        Eigen::Vector3d translation;
        translation[0] = poses[dev_idx][3];
        translation[1] = poses[dev_idx][4];
        translation[2] = poses[dev_idx][5];
        device.world_pose_ = Pose(rotation, translation);
    }

    // TODO?: update 3D points

    // tell the logger about the results
    STATUS("...done. The final poses are");
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        STATUS(QString::fromStdString(devices[dev_idx].device_name_));
        LOGMATRIX("R", devices[dev_idx].world_pose_.R_cv_);
        LOGMATRIX("t", devices[dev_idx].world_pose_.t_cv_);
    }

}


Pose Optimizer::calculatePoseSVD(const std::vector<Eigen::Vector3d>& points_a,
                                 const std::vector<Eigen::Vector3d>& points_b){
    // calculate centroids
    Eigen::Vector3d centroid_a, centroid_b;
    for(const Eigen::Vector3d& p : points_a){
        centroid_a += p;
    }
    centroid_a /= double(points_a.size());
    for(const Eigen::Vector3d& p : points_b){
        centroid_b += p;
    }
    centroid_b /= double(points_b.size());

    // center 3D point sets for each device
    int num_correspondeces = int(points_a.size());
    Eigen::Matrix<double, 3, Eigen::Dynamic> source_points, target_points;
    source_points.resize(3, num_correspondeces);
    target_points.resize(3, num_correspondeces);
    for(size_t col = 0; col < num_correspondeces; col++){
        source_points.block(0, col, 3, 1) = points_a[col] - centroid_a;
        target_points.block(0, col, 3, 1) = points_b[col] - centroid_b;
    }

    // calculate the SVD
    Eigen::Matrix3d point_mat = source_points * target_points.transpose();
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(point_mat, Eigen::ComputeFullU | Eigen::ComputeFullV);

    // get rotation R and translation t
    Eigen::Matrix3d R = svd.matrixV() * svd.matrixU().transpose();
    Eigen::Vector3d t = centroid_b - R * centroid_a;

    return Pose(R, t);
}
