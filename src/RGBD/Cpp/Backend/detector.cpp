#include "detector.h"
#include <math.h>

Detector::Detector(std::shared_ptr<Imagelist_Generator> imagelist_gen){
    imagelist_gen_ = imagelist_gen;
}

void Detector::detectCorrespondences(const std::string& image_dir,
                                     std::vector<Device>& devices,
                                     const QString& rgb_scheme,
                                     const QString& depth_scheme,
                                     const double& ball_radius,
                                     const int &visualize_detections){

    // create undistortion maps
    STATUS("Calculating undistortion maps...");
    for(Device& dev : devices){
        cv::initUndistortRectifyMap(dev.rgb_.K_, dev.rgb_.dist_, cv::noArray(),
                                    dev.rgb_.K_,
                                    cv::Size(dev.rgb_.res_x_,dev.rgb_.res_y_),
                                    CV_32FC1, dev.rgb_.map_x_, dev.rgb_.map_y_);
        cv::initUndistortRectifyMap(dev.depth_.K_, dev.depth_.dist_, cv::noArray(),
                                    dev.depth_.K_,
                                    cv::Size(dev.depth_.res_x_,dev.depth_.res_y_),
                                    CV_32FC1, dev.depth_.map_x_, dev.depth_.map_y_);
    }
    STATUS("...done.");

    // calculate F matrices for calculating epipolar lines in rectified depth
    // images
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        // get the translational part of the rgb to ir transformation
        cv::Mat t = devices[dev_idx].pose_rgb_to_depth_.t_cv_.clone();
        cv::Mat tx = (cv::Mat_<double>(3,3) <<
                      0.0, -t.at<double>(0,2), t.at<double>(0,1),
                      t.at<double>(0,2), 0.0, -t.at<double>(0,0),
                      -t.at<double>(0,1), t.at<double>(0,0), 0.0);
        // calculate essential matrix via R*t_x whereby R is the rotation and
        // t_x the cross product matrix based on the translation t
        cv::Mat E = devices[dev_idx].pose_rgb_to_depth_.R_cv_ * tx;
        // the F matrix is no given via F = K_ir^-t * E * K_rgb^-1
        devices[dev_idx].F_ = devices[dev_idx].depth_.K_.t().inv() * E
                * devices[dev_idx].rgb_.K_.inv();
    }

    // create a list of the available images
    Imagelist img_list = imagelist_gen_->createImagelist(image_dir,
                                                         rgb_scheme,
                                                         depth_scheme);

    // scan the file descriptor in the image list to create lists of available
    // cameras and image IDs
    std::vector<std::string> IDs;

    for(std::vector<RGBDImageDescription>& descriptors : img_list){
        IDs.push_back(descriptors[0].ID_);
        for(RGBDImageDescription& descriptor : descriptors){
            std::string camera = descriptor.camera_;

            // search for camera name in devices
            for(size_t cam_idx = 0; cam_idx < devices.size(); cam_idx++){
                if(camera.compare(devices[cam_idx].device_name_) == 0){
                    descriptor.cam_idx_ = cam_idx;
                }
            }
            if(descriptor.cam_idx_ == -1){
                ERROR("Camera name " + QString::fromStdString(camera)
                      + " has no match in device list from intrinsics file.");
                return;
            }
        }
    }
    float num_combinations = float(IDs.size() * devices.size());


    // detect table tennis ball in rgb images
    STATUS("Detecting ball in RGB images...");
    std::shared_ptr<Detections> detections = std::shared_ptr<Detections>(
                new Detections(IDs.size(),
                               std::vector<Detection>(devices.size(),
                                                      Detection())));

    float current_idx = 0;
    for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){
        for(const RGBDImageDescription& descriptor : img_list[ID_idx]){
            (*detections)[ID_idx][descriptor.cam_idx_].rgb_image_file_ =
                    descriptor.path_rgb_;
            (*detections)[ID_idx][descriptor.cam_idx_].depth_image_file_ =
                    descriptor.path_depth_;
            detectBallRGB(&((*detections)[ID_idx][descriptor.cam_idx_]),
                    (visualize_detections == VIS_ALL
                     || visualize_detections == VIS_RGB_DETECTIONS));
            current_idx++;
            emit progress(current_idx/num_combinations);
        }
    }
    emit progress(1.0);
    if(visualize_detections > 0){
        cv::destroyAllWindows();
    }
    STATUS("...done.");

    // undistort detections
    STATUS("Undistort detections...");
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        std::vector<cv::Point2f> distorted_points;
        for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){
            distorted_points.push_back(
                        cv::Point2f((*detections)[ID_idx][dev_idx].x_,
                                    (*detections)[ID_idx][dev_idx].y_));
        }
        std::vector<cv::Point2f> undistorted_points;
        cv::undistortPoints(distorted_points, undistorted_points,
                            devices[dev_idx].rgb_.K_, devices[dev_idx].rgb_.dist_,
                            cv::noArray(), devices[dev_idx].rgb_.K_);

        for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){
            if((*detections)[ID_idx][dev_idx].valid_){
                (*detections)[ID_idx][dev_idx].x_ = undistorted_points[ID_idx].x;
                (*detections)[ID_idx][dev_idx].y_ = undistorted_points[ID_idx].y;

                if(visualize_detections == VIS_ALL
                        || visualize_detections == VIS_UNDISTORTED_DETECTIONS){

                    cv::Mat image = cv::imread(
                                (*detections)[ID_idx][dev_idx].rgb_image_file_);
                    cv::remap(image, image, devices[dev_idx].rgb_.map_x_,
                              devices[dev_idx].rgb_.map_y_, cv::INTER_LINEAR);
                    cv::circle(image,
                               cv::Point(int((*detections)[ID_idx][dev_idx].x_),
                                         int((*detections)[ID_idx][dev_idx].y_)),
                               int((*detections)[ID_idx][dev_idx].r_),
                               cv::Scalar(0,255,0), 2);
                    cv::imshow("Undistorted detection", image);
                    cv::waitKey(0);
                }

            }
        }
    }
    if(visualize_detections > 0){
        cv::destroyAllWindows();
    }
    STATUS("...done.");

    // find corresponding depth value
    STATUS("Searching for corresponding depth values...");
    current_idx = 0;
    for(size_t dev_idx = 0; dev_idx < devices.size(); dev_idx++){
        for(size_t ID_idx = 0; ID_idx < IDs.size(); ID_idx++){

            Detection& detection = (*detections)[ID_idx][dev_idx];
            if(!detection.valid_){
                continue;
            }

            // undistort depth image
            cv::Mat depth_image = cv::imread(
                        detection.depth_image_file_,
                        cv::IMREAD_UNCHANGED);
            depth_image.convertTo(depth_image, CV_64F);
            cv::remap(depth_image, depth_image, devices[dev_idx].depth_.map_x_,
                      devices[dev_idx].depth_.map_y_, cv::INTER_NEAREST);

            // map depth to 3D to rgb for pixels along the epipolar line
            cv::Mat pixel_rgb = (cv::Mat_<double>(3,1) <<
                                 detection.x_,
                                 detection.y_,
                                 1.0);
            cv::Mat epi = devices[dev_idx].F_ * pixel_rgb;

            double dist, depth;
            double best_dist = 3.0;
            double best_depth = -1.0;
            cv::Point best_pixel;
            cv::Mat best_scene_point, scene_point, pixel_d;

            for(int col = 0; col < depth_image.cols; col++){
                int row = std::round(( - epi.at<double>(2,0)
                                       - epi.at<double>(0,0) * col) /
                                     epi.at<double>(1,0));

                if(row < 0 || row > depth_image.rows+1){
                    continue;
                }

                /// project pixel into 3D space and map it into rgb camera
                depth = depth_image.at<double>(row,col) + ball_radius;
                pixel_d = (cv::Mat_<double>(3,1) << double(col), double(row), 1.0);
                // transform pixel to camera coordinates
                pixel_d = devices[dev_idx].depth_.K_inv_ * pixel_d;
                // map to 3D using the depth value from the undistorted image
                pixel_d = depth * pixel_d / pixel_d.at<double>(2,0);
                // project 3d point into rgb camera
                scene_point = devices[dev_idx].pose_rgb_to_depth_.R_cv_.t() *
                        (pixel_d - devices[dev_idx].pose_rgb_to_depth_.t_cv_);

                pixel_d = devices[dev_idx].rgb_.K_ * scene_point;
                // get coordinates
                pixel_d /= pixel_d.at<double>(2,0);

                // calculate distance to rgb centroid
                dist = cv::norm(pixel_d - pixel_rgb);
                // and check if closer than previous best point
                if(dist < best_dist){
                    best_dist = dist;
                    best_depth = depth;
                    best_pixel = cv::Point(col,row);
                    best_scene_point = scene_point.clone();
                }
            }

            if(best_depth > 0.0){
                detection.p_ = Eigen::Vector3d(best_scene_point.at<double>(0,0),
                                               best_scene_point.at<double>(1,0),
                                               best_scene_point.at<double>(2,0));

                if(visualize_detections == VIS_ALL
                        || visualize_detections == VIS_DEPTH_PIXELS){
                    depth_image.convertTo(depth_image, CV_8UC3);
                    cv::cvtColor(depth_image, depth_image, cv::COLOR_GRAY2RGB);

                    cv::Point start(0, std::round((-epi.at<double>(2,0)
                                                   -epi.at<double>(0,0)) /
                                                  epi.at<double>(1,0)));
                    cv::Point end(depth_image.cols-1, std::round((-epi.at<double>(2,0)
                                                                  -epi.at<double>(0,0) * (depth_image.cols - 1)) /
                                                                 epi.at<double>(1,0)));
                    cv::line(depth_image, start, end, cv::Scalar(0,0,255), 2);
                    cv::circle(depth_image, best_pixel,
                               int(detection.r_), cv::Scalar(255,0,0), 2);
                    cv::imshow("Depth Image Correspondence", depth_image);
                    cv::waitKey(0);

                }
            }
            else{
                detection.valid_ = false;
                WARN("Depth correspondence in image "
                     + QString::fromStdString(detection.depth_image_file_)
                     + " could not be found.");
            }
            current_idx++;
            emit progress(current_idx/num_combinations);
        }
    }
    emit progress(1.0);
    if(visualize_detections > 0){
        cv::destroyAllWindows();
    }
    STATUS("...done.");

    emit ballsDetected(detections);
}

bool Detector::detectBallRGB(Detection* detection, const bool& visualize_detection){

    detection->valid_ = false;

    cv::Mat image = cv::imread(detection->rgb_image_file_);
    cv::Mat image_hsv;
    // convert to HSV color space
    cv::cvtColor(image, image_hsv, cv::COLOR_BGR2HSV);
    // separate channels
    cv::Mat channel_images[3];
    cv::split(image_hsv, channel_images);
    // threshold channels based on empirical values - TODO: User assisted
    // threshold selection
    cv::Mat masks[5];
    // threshold H
    cv::threshold(channel_images[0], masks[0], 170, 1.0, cv::THRESH_BINARY);
    cv::threshold(channel_images[0], masks[1], 10, 1.0, cv::THRESH_BINARY_INV);
    // threshold S
    cv::threshold(channel_images[1], masks[2], 64, 1.0, cv::THRESH_BINARY);
    cv::threshold(channel_images[1], masks[3], 128, 1.0, cv::THRESH_BINARY_INV);
    // threshold V
    cv::threshold(channel_images[2], masks[4], 230, 1.0, cv::THRESH_BINARY);
    // combine masks
    cv::Mat mask = (masks[0] | masks[1]) & (masks[2] & masks[3]) & masks[4];

    // apply dilation filter
    cv::Mat element = cv::getStructuringElement(cv::MORPH_DILATE, cv::Size(3,3),
                                                cv::Point(1,1));
    cv::erode(mask, mask, element);
    cv::dilate(mask, mask, element);

    // find connected components and their stats
    cv::Mat labels;
    cv::Mat stats;
    cv::Mat centroids;
    int num_components = cv::connectedComponentsWithStats(mask, labels, stats, centroids);

    // filter components according to radius and roundness
    float width, height, area, squareness, fill_factor, score;
    float target_fill_factor = M_PI/4.0f;
    float best_score = 0.3f;
    int best_component = -1;
    for(int comp_idx = 0; comp_idx < num_components; comp_idx++){

        // get component stats
        width = float(stats.at<uint32_t>(comp_idx, 2));
        height = float(stats.at<uint32_t>(comp_idx, 3));
        area = float(stats.at<uint32_t>(comp_idx, 4));

        // filter out components that are too small
        if((area < 25.0f) || (width > float(image.rows)/4.0f)
                || (height > float(image.rows)/4.0f)){
            continue;
        }

        // calculate properties and score
        squareness = std::min(width,height)/std::max(width,height);
        fill_factor = area / (width * height);
        score = squareness * (1.0f - 2.0f * std::abs(target_fill_factor - fill_factor));

        // check if best score
        if(score > best_score){
            best_component = comp_idx;
            best_score = score;
        }
    }

    // if a best component has been found, save its properties
    if(best_component != -1){
        detection->valid_ = true;
        detection->x_ = centroids.at<double>(best_component, 0);
        detection->y_ = centroids.at<double>(best_component, 1);
        detection->r_ = std::min(detection->x_ - double(stats.at<uint32_t>(best_component, 0)),
                                 detection->y_ - double(stats.at<uint32_t>(best_component, 1)));
        // show the detection
        if(visualize_detection){
            cv::circle(image, cv::Point(int(detection->x_), int(detection->y_)),
                       int(detection->r_), cv::Scalar(0,255,0), 2);
            cv::imshow("RGB Image Detection", image);
            cv::waitKey(0);
        }

        return true;
    }
    else{ // otherwise reject the image
        if(visualize_detection){
            // For additional debug output, uncomment the following line and
            // check the masks e.g. with GIMP
            //  cv::imwrite("Rejected.png", image);
            //  cv::imwrite("Mask.png", mask);
            //  cv::imwrite("MaskH.png", (masks[0] | masks[1]));
            //  cv::imwrite("MaskS.png", (masks[2] & masks[3]));
            //  cv::imwrite("MaskV.png", masks[4]);

            // show the rejected image
            cv::imshow("Rejected - No ball detected!", image);
            cv::waitKey(0);
        }
        // remove image dir path from image_file for clearer logging
        QString file_name = QString::fromStdString(detection->rgb_image_file_);
        file_name = file_name.mid(file_name.lastIndexOf('/')+1);
        WARN("Ball could not be detected in image "+file_name);
        return false;
    }
}

void Detector::getAssociatedDepthValues(){
    //TODO: outsource some code of detectCorrespondences to this function
}
