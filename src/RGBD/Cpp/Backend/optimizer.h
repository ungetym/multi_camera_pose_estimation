////////////////////////////////////////////////////////////////////////////////
///      Different optimization methods to calculate the RGB camera poses    ///
////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Backend/data_types.h"
#include "UI/logger.h"
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <QObject>

///
/// \brief The Reprojection_Error class represents the reprojection error of a
///                                     given pose and world point and is used
///                                     by the optimization to create the
///                                     residuals
///
class Reprojection_Error{

public:
    ///
    /// \brief Reprojection_Error is the default constructor
    /// \param device             for which the 3D-to-2D correspondence was
    ///                           measured
    /// \param pixel              the measured pixel position
    ///
    Reprojection_Error(const Device& device,
                       const Eigen::Vector2d& pixel) :
        device_{ device },
        pixel_{ pixel}
    { }

    ///
    /// \brief operator ()  calculates the reprojection error
    /// \param pose         transform from world to current RGB camera
    /// \param scene_point  3D world point to optimize
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const pose, const T* const scene_point,
                    T* residuals) const {

        const T* rotation = pose;
        const T* translation = pose + 3;

        // rotate world point into camera coordinates
        T p[3];
        ceres::AngleAxisRotatePoint(rotation, scene_point, p);

        // translate point
        p[0] += translation[0];
        p[1] += translation[1];
        p[2] += translation[2];

        // get the rgb camera's K matrix values
        const T fx = T(device_.rgb_.K_.at<double>(0,0));
        const T fy = T(device_.rgb_.K_.at<double>(1,1));
        const T cx = T(device_.rgb_.K_.at<double>(0,2));
        const T cy = T(device_.rgb_.K_.at<double>(1,2));

        // transfer from camera to pixel coordinates
        T x_projected = (fx * p[0] / p[2]) + cx;
        T y_projected = (fy * p[1] / p[2]) + cy;

        // calculate residuals, note: ceres squares these automatically
        residuals[0] = x_projected - T(pixel_[0]);
        residuals[1] = y_projected - T(pixel_[1]);

        return true;
    }

    ///
    /// \brief create   is used to create a ceres cost function
    /// \param device   current device
    /// \param pixel    detected pixel position
    /// \return
    ///
    static ceres::CostFunction* create(const Device& device,
                                       const Eigen::Vector2d& pixel) {
        return new ceres::AutoDiffCostFunction<Reprojection_Error, 2, 6, 3>(
                    new Reprojection_Error(device, pixel));
    }

protected:
    const Device device_;
    const Eigen::Vector2d pixel_;
};

///
/// \brief The Reprojection_Error_Fixed_Pose class is the pendant to the class
///                                                above, but for the first
///                                                camera which is assumed to be
///                                                located at the world
///                                                coordinate center with zero
///                                                rotation
///
class Reprojection_Error_Fixed_Pose{

public:
    ///
    /// \brief Reprojection_Error_Fixed_Pose is the default constructor
    /// \param device
    /// \param pixel
    ///
    Reprojection_Error_Fixed_Pose(const Device& device,
                                  const Eigen::Vector2d& pixel) :
        device_{ device },
        pixel_{ pixel}
    { }

    ///
    /// \brief operator () calculates the reprojection error
    /// \param p           3D world point to optimize
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const p, T* residuals) const {

        // get the rgb camera's K matrix values
        const T fx = T(device_.rgb_.K_.at<double>(0,0));
        const T fy = T(device_.rgb_.K_.at<double>(1,1));
        const T cx = T(device_.rgb_.K_.at<double>(0,2));
        const T cy = T(device_.rgb_.K_.at<double>(1,2));

        // transfer from camera to pixel coordinates
        T x_projected = (fx * p[0] / p[2]) + cx;
        T y_projected = (fy * p[1] / p[2]) + cy;

        // calculate residuals, note: ceres squares these automatically
        residuals[0] = x_projected - T(pixel_[0]);
        residuals[1] = y_projected - T(pixel_[1]);

        return true;
    }

    ///
    /// \brief create       is used to create a ceres cost function
    /// \param device       current camera device
    /// \param pixel        detected pixel position
    /// \return
    ///
    static ceres::CostFunction* create(const Device& device,
                                       const Eigen::Vector2d& pixel) {
        return new ceres::AutoDiffCostFunction<Reprojection_Error_Fixed_Pose, 2, 3>(
                    new Reprojection_Error_Fixed_Pose(device, pixel));
    }

protected:
    const Device device_;
    const Eigen::Vector2d pixel_;
};

///
/// \brief The Optimizer class contains different optimization methods for
///                            finding the RGB camera poses
///
class Optimizer : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Optimizer is the default constructor
    ///
    Optimizer();

public slots:

    ///
    /// \brief initialPoseEstimation via RANSAC and SVD
    /// \param devices          list of available RGB-D devices to which the
    ///                         poses will be saved
    /// \param detections       detected RGB-D and 3D positions of the table
    ///                         tennis ball
    /// \param max_iterations   for the RANSAC loop
    /// \param inlier_threshold in mm to accept a detection as RANSAC inlier
    ///
    void initialPoseEstimation(std::vector<Device>& devices,
                               const std::shared_ptr<Detections>& detections,
                               const int &max_iterations,
                               const double &inlier_threshold);

    ///
    /// \brief bundleAdjustment starts a non-linear optimization
    ///                         (Levenberg-Marquardt) with the results of the
    ///                         initial pose estimation
    /// \param devices          list of available RGB-D devices to which the
    ///                         poses will be saved
    /// \param detections       detected RGB-D and 3D positions of the table
    ///                         tennis ball
    /// \param max_iterations   for the LM optimization loop
    /// \param change_threshold represents the minimum value of error changes
    ///                         from one iteration to the next one, below which
    ///                         the loop will be stopped
    ///
    void bundleAdjustment(std::vector<Device>& devices,
                          const std::shared_ptr<Detections>& detections,
                          const int &max_iterations,
                          const double &change_threshold);

private:

    ///
    /// \brief Optimizer::calculatePoseSVD  calculates a rigid transformation
    ///                                     between two 3D point clouds
    /// \param points_a
    /// \param points_b
    /// \return
    ///
    Pose calculatePoseSVD(const std::vector<Eigen::Vector3d>& points_a,
                          const std::vector<Eigen::Vector3d>& points_b);

signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(const QString& msg, const cv::Mat& mat);

    ///
    /// \brief progress
    /// \param val
    ///
    void progress(const float& val);

    ///
    /// \brief initialPosesCalculated is emitted upon finishing the initial pose
    ///                               calculation to notify the main window
    ///                               class, that the poses are available and
    ///                               can be used for the next optimization step
    ///
    void initialPosesCalculated();
};
