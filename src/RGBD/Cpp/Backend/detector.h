////////////////////////////////////////////////////////////////////////////////
///            Detector for the table tennis ball in RGB-D images            ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Backend/data_types.h"
#include "Backend/imagelist_generator.h"
#include <QObject>

///
/// \brief The Detector class
///
class Detector : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Detector         is the default constructor
    /// \param imagelist_gen    pointer to an instance of the imagelist generator
    ///
    Detector(std::shared_ptr<Imagelist_Generator> imagelist_gen);

public slots:

    ///
    /// \brief detectCorrespondences    coordinates the whole correspondence
    ///                                 detection process, i.e. creation of
    ///                                 image file lists, RGB ball detection and
    ///                                 the detection of the associated depth
    ///                                 values
    /// \param image_dir                directory containing the RGB images
    /// \param devices                  list of devices
    /// \param rgb_scheme               rgb image naming scheme
    /// \param depth_scheme             depth image naming scheme
    /// \param ball_radius              in mm
    /// \param visualize_detections     one of the constants VIS_* specified in
    ///                                 data_types.h
    ///
    void detectCorrespondences(const std::string& image_dir,
                               std::vector<Device>& devices,
                               const QString& rgb_scheme,
                               const QString& depth_scheme,
                               const double& ball_radius,
                               const int& visualize_detections);

private:

    ///
    /// \brief detectBallRGB   tries to find the pixel position and radius of an
    ///                        illuminated table tennis ball in an RGB image
    /// \param detection       resulting pixel coordinates and radius
    /// \param visualize_detection set to true to enable debug visualization
    /// \return                true if detected, false otherwise
    ///
    bool detectBallRGB(Detection* detection, const bool &visualize_detection);

    ///
    /// \brief getAssociatedDepthValues uses epipolar geometry to find the
    ///                                 corresponding depth value of a detection
    ///                                 in an RGB image
    ///
    void getAssociatedDepthValues();

private:

    /// pointer to an instance of the imagelist generator
    std::shared_ptr<Imagelist_Generator> imagelist_gen_;

signals:

    ///
    /// \brief ballsDetected    is emitted after successfully finishing the ball
    ///                         detection
    /// \param detectedBalls    list of detections
    ///
    void ballsDetected(const std::shared_ptr<Detections>& detectedBalls);

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief progress
    /// \param val
    ///
    void progress(const float& val);
};
