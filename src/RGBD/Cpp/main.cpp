#include "UI/main_window.h"
#include <QApplication>

int main(int argc, char *argv[]){
    QApplication a(argc, argv);

    // create instance of data class, that will be used for all sub-processes
    std::shared_ptr<Data> data = std::shared_ptr<Data>(new Data());
    // create gui and start application
    Main_Window w(data);
    w.show();
    return a.exec();
}
